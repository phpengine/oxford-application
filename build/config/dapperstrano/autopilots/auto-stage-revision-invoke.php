<?php

/*************************************
*      Generated Autopilot file      *
*     ---------------------------    *
*Autopilot Generated By Dapperstrano *
*     ---------------------------    *
*************************************/

Namespace Core ;

class AutoPilotConfigured extends AutoPilot {

    public $steps ;

    public function __construct() {
	      $this->setSteps();
        $this->setSSHData();
    }

    /* Steps */
    private function setSteps() {

	    $this->steps =
	      array(
          array ( "InvokeSSH" => array(
                    "sshInvokeSSHDataExecute" => true,
                    "sshInvokeSSHDataData" => "",
                                "sshInvokeServers" => array(
                      array("target" => "178.63.72.156", "user" => "gcdeployman", "pword" => "turbulentDeploy1995",  ),
 ),
          ) , ) ,
	      );

	  }


//
// This function will set the sshInvokeSSHDataData variable with the data that
// you need in it. Call this in your constructor
//
  private function setSSHData() {
    $step = "0";
    $this->steps[$step]["InvokeSSH"]["sshInvokeSSHDataData"] = <<<"SSHDATA"
cd /tmp
git clone -b staging --no-checkout --depth 1 https://phpengine:codeblue041291@bitbucket.org/phpengine/oxford-application.git
cd oxford-application
git show HEAD:build/config/dapperstrano/autopilots/auto-stage-revision-install.php > /tmp/autopilot-oxap-stage-install.php
rm -rf /tmp/oxford-application
cd /tmp
sudo dapperstrano autopilot execute autopilot-oxap-stage-install.php
sudo jrush jfeature feature-install --config-file=/var/www/gcapplications/staging/oxford-application/oxford-application/current/src/configuration.php --feature-file=/var/www/gcapplications/staging/oxford-application/oxford-application/current/src/administrator/components/com_gcworkflowdeploy/feature_store/ZJHIR59P6OL196AF_1369950277.zip
sudo jrush jfeature feature-pull --config-file=/var/www/gcapplications/staging/oxford-application/oxford-application/current/src/configuration.php --pull-unique-time=ZJHIR59P6OL196AF_1369950277
sudo jrush cache site-clear --config-file=/var/www/gcapplications/staging/oxford-application/oxford-application/current/src/configuration.php
sudo jrush jfeature folder-defaults --config-file=/var/www/gcapplications/staging/oxford-application/oxford-application/current/src/configuration.php
sudo chown -R www-data /var/www/gcapplications/staging/oxford-application/oxford-application/current/src
sudo rm autopilot-oxap-stage-install.php
SSHDATA;
  }

}
