<?php
// No Direct Access
defined('_JEXEC') or die('No Direct Access');
?>

<?php
// $cck->addCSS('/templates/dj_page/js/jquery-1.9.1.min.js');
$cck->addScript('/templates/dj_page/js/jquery.jcarousel.min.js');
// $cck->addCSS('/templates/dj_page/js/start_carousel.js');
$cck->addStyleSheet('/templates/dj_page/css/skins/tango/skin.css')
?>

<script type="text/javascript">

  function mycarousel_initCallback(carousel)
  {
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
      carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
      carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
      carousel.stopAuto();
    }, function() {
      carousel.startAuto();
    });
  };

  jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
      auto: 1,
      scroll: 1,
      animation: 4000,
      wrap: 'last',
      initCallback: mycarousel_initCallback
    });
  });

</script>


<style>
  .cck-line-body{
     background: url("/<?php echo $cck->getValue('event_main_image_background_version') ; ?>") no-repeat left top;
  }
</style>

<h1 class="event_brand_title">
  <?php echo $cck->getValue('art_title'); ?>
</h1>

<div class="pagerow">

  <ul id="mycarousel" class="jcarousel-skin-tango">
    <?php

    $all_images_array = $cck->getValue('all_event_images') ;
    $no_of_items = count($all_images_array) ;

    foreach ($all_images_array as $single_item) {

    ?>

    <li>
        <img src="/<?php echo $single_item->thumb2; ?>" alt="" />
    </li>

    <?php
    }
    ?>

  </ul>

</div>


<div class="pagerow">

  <div class="leftEventPageColumn">

    <div class="pagerow event_page_row">
      <h2 class="event_page_subtitle">What is it?...</h2>
      <p>
        <?php echo $cck->getValue('event_type'); ?>
      </p>
    </div>

    <div class="pagerow event_page_row">
      <h2 class="event_page_subtitle">Ooh, Tell me more...</h2>
      <p>
        <?php echo $cck->getValue('event_description'); ?>
      </p>
    </div>

    <div class="pagerow event_page_row">
      <h2 class="event_page_subtitle">Where is it?...</h2>
      <p>
        <?php

        $venueId = $cck->getValue('venue_selector');
        $vRow = JCckContentArticle::getRow($venueId, "venue");
        echo $vRow->venue_name.' '.$vRow->venue_address.' '.$vRow->venue_postcode;

        ?>
      </p>
    </div>

    <div class="pagerow event_page_row">
      <h2 class="event_page_subtitle">When is it?...</h2>
      <p>
        <?php echo $cck->getValue('event_when_description'); ?>
      </p>
    </div>

  </div>

  <div class="rightEventPageColumn">

    <div class="buyTicketsBox">

      <h3 class="eventPageTicketBuying"> Get Tickets!! </h3>

      <?php

       // get list of all event instances for this brand
       $query = "SELECT * FROM jos_cck_store_form_event_instance WHERE event_brand_selector='"
         . $cck->getValue('art_id')."'" ;
       $associated_event_instances = JCckDatabase::loadObjectList($query);

       // get the current date
       $now_timestamp = time() ;

       // sort all event instances for this brand by date
       $parsed_associated_event_instances = array();
       foreach ($associated_event_instances as $associated_event_instance) {
         $associated_event_instance->event_date =
           substr_replace($associated_event_instance->event_date,
             $associated_event_instance->event_instance_start_time_hours,
             strlen($associated_event_instance->event_date)-5,
             2) ;
         $associated_event_instance->event_date =
           substr_replace($associated_event_instance->event_date,
             $associated_event_instance->event_instance_start_time_minutes,
             strlen($associated_event_instance->event_date)-2,
             2) ;
         $associated_event_instance_time = strtotime($associated_event_instance->event_date) ;
         // get the details of only the ones in future
         if ($associated_event_instance_time > $now_timestamp) {
           $eiRow = JCckContentArticle::getRow($associated_event_instance->id);
           // var_dump($eiRow);
           $associated_event_instance->event_instance_title = $eiRow->title ;
           $parsed_associated_event_instances[] = $associated_event_instance; } }


       // for the next three loop the following
       $i = 0;
       if (count($parsed_associated_event_instances)>0) {
         $parsed_associated_event_instances = array_reverse($parsed_associated_event_instances);
         foreach ($parsed_associated_event_instances as $parsed_associated_event_instance) {

           $query = 'SELECT ticketmaster_event_id FROM jos_seblod_ticketmaster_event_relations WHERE seblod_event_brand_id="'.$cck->getValue('art_id').'"';
           $ticketmaster_id = \JCckDatabase::loadResult($query);

           $link = '<a class="buyTicketLink" target="_blank" href="index.php?option=com_ticketmaster&view=event&id='.$ticketmaster_id.'&Itemid=166">'
             . '<span class="title">' . $parsed_associated_event_instance->event_instance_title .' </span><br /> '
             . $parsed_associated_event_instance->event_date.'</a>';
           echo $link ;

           $addButton  = '<a class="addToCartButton" target="_blank" href="index.php?option=com_ticketmaster&view=event&';
           $addButton .= 'id='.$ticketmaster_id.'&Itemid=166">';
           $addButton .= 'Buy These!';
           $addButton .= '</a>';
           echo $addButton;
           $i++;
           if ($i>3) { break; } } }

       else {
         echo '<span class="noResults">There are no events available</span>'; }

      ?>

    </div>

  </div>

</div>