<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.joomla
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Example Content Plugin
 *
 * @package     Joomla.Plugin
 * @subpackage  Content.joomla
 * @since       3.0
 */
class plgContentApplicationSaveChecks extends JPlugin {

  public function onContentAfterSave($context, $article, $isNew) {

    if (substr($_SERVER["REQUEST_URI"], 0, 14) != "/administrator") {

      $jArticleUser = \JUser::getInstance($article->created_by);

      $jCurrentUser = \JFactory::getUser();

      $cckArticle = \JCckContentArticle::getRow($article->id, "event_instance");

      if ($isNew == true) {
        $text = \CCK_Article::getText($article->id) ;
        $out = \CCK_Article::setValue($text , "application_status", "Pending" );
        error_log($out) ; }

      $current_is_admin = false;
      foreach (array("3", "4", "5", "6", "7", "8") as $admin_group_id) {
        if (in_array($admin_group_id, $jCurrentUser->getAuthorisedGroups())) {
          $current_is_admin = true; } }

      if ($current_is_admin != true) {
        if ( in_array("2", $jArticleUser->getAuthorisedGroups())) {
          $all_my_course_apps_query = 'SELECT id FROM jos_content WHERE created_by="'.$jArticleUser->id.'" AND catid="18" AND state="1"';
          $all_my_course_apps = \JCckDatabase::loadResultArray($all_my_course_apps_query);
          foreach ($all_my_course_apps as $one_course_app_id) {
            $one_course_app = \JCckContentArticle::getRow($one_course_app_id, "event_instance");
            if ($one_course_app->course_selector == $cckArticle->course_selector && $one_course_app->id != $cckArticle->id) { // an application for this course already exists
              JFactory::getApplication()->enqueueMessage(JText::_('You have already applied for this course.'), 'error');
              if ($one_course_app->application_status == "Under Review")  { // an application under review exists
                JFactory::getApplication()->enqueueMessage(JText::_('Your current application is under review.'), 'error'); }
              JFactory::getApplication()->enqueueMessage(JText::_('This application will be automatically deleted immediately'), 'error');
              $this->deleteArticle($article->id); } } } }

    }

  }

  private function deleteArticle($article_id) {
    $del_article_query = 'DELETE FROM jos_content WHERE id="'.$article_id.'" ';
    \JCckDatabase::doQuery($del_article_query);
  }

}
