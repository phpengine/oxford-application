<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_footer
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$jCurrentUser = JFactory::getUser();

$all_my_course_apps_query = 'SELECT id FROM jos_content WHERE created_by="'.$jCurrentUser->id.'" AND catid="18" AND state="1"';
$all_my_course_app_ids = \JCckDatabase::loadResultArray($all_my_course_apps_query);

$all_apps = array();
foreach ($all_my_course_app_ids as $one_course_app_id) {
  $one_course_app = $ticketRow = \JCckContentArticle::getRow($one_course_app_id, "");
  $all_apps[] = $one_course_app;
}

require JModuleHelper::getLayoutPath('mod_display_previous_applications', $params->get('layout', 'default'));
