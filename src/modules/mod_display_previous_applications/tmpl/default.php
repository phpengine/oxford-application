<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_footer
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div><?php

  $app_counter = 1;
  foreach($all_apps as $one_app) {

    $courseRow = \JCckContentArticle::getRow($one_app->course_selector, "");

    ?>

    <div class="one_app_in_list">
      <h2> Application <?php echo $app_counter; ?>: <?php echo $one_app->title; ?> </h2>
      <?php
      if (isset($courseRow)) {
      ?>
      <h4> For Course: <?php echo $courseRow->title; ?> </h4>
      <?php
      } else {
      ?>
        <h4> For Course: NONE SELECTED </h4>
      <?php
      }
      ?>
      <p> This application currently has the status: <?php echo $one_app->application_status ; ?> </p>

      <?php
      if (in_array($one_app->application_status, array("Pending")) ) {
        echo '<p> <a href="index.php?option=com_cck&view=form&layout=edit&type=event_instance&Itemid=108&id='.$one_app->id.'">Click here to edit this</a> </p>'; }
      if (in_array($one_app->application_status, array("Under Review")) ) {
        echo '<p> <a href="index.php?option=com_content&view=article&layout=edit&type=event_instance&id=59&Itemid=199&j_art_id='.$one_app->id.'">This application is Under Review, so cannot be edited, but Click here to view this</a> </p>'; }
      if (in_array($one_app->application_status, array("Accepted")) ) {
        echo '<p> <a href="index.php?option=com_content&view=article&layout=edit&type=event_instance&id=59&Itemid=199&j_art_id='.$one_app->id.'">This application has been Accepted, so cannot be edited, but Click here to view this</a> </p>'; }
      if (in_array($one_app->application_status, array("Rejected")) ) {
        echo '<p> <a href="index.php?option=com_content&view=article&layout=edit&type=event_instance&id=59&Itemid=199&j_art_id='.$one_app->id.'">This application has been Rejected, so cannot be edited, but Click here to view this</a> </p>'; }
      ?>

    </div>

    <?php
    $app_counter++ ;
  };




  ?></div>
