<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_footer
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div>
  <?php

    $courseRow = \JCckContentArticle::getRow($jCurrentArticle->course_selector, "");

    ?>

    <div class="one_application_view">
      <h2> Application for Course: <?php echo $courseRow->title; ?></h2>
      <p> This application currently has the status: <?php echo $jCurrentArticle->application_status ; ?> </p>

      <?php

      $meInAnArray = new \ArrayObject($jCurrentArticle);

      $property_keys_readable = array(
        "title" => "Title for the Application Submission",
        "created" => "Time application created",
        "dob" => "Your Date of Birth",
        "course_selector" => "Course applied for",
        "gender" => "Your Gender",
        "nationality" => "Your Nationality",
        "occupation" => "Your Occupation",
        "address" => "Your Address",
        "home_telephone_number" => "Your Home Telephone Number",
        "mobile_telephone_number" => "Your Mobile Telephone Number",
        "email_correspondence" => "Your Email address for Correspondence",
        "previous_qualifications" => "Your Previous Qualifications",
        "another_qualification_this_year" => "Are you studying for another Qualification this year?",
        "reasons_for_studying" => "Your Reasons for Applying for this Course?",
        "computing_experience" => "Your Computing Experience",
        "your_computer_system" => "Description of Your Computer System",
        "how_did_you_discover_the_course" => "How did you discover this Course?",
        "is_english_your_first_language" => "Is English Your First Language",
        "if_english_not_your_first_language_describe_quals" => "If English is not your first language, describe your Qualifications in English",
        "will_you_pay_your_own_fees" => "Will you pay your own fees?",
        "are_you_an_eu_student" => "Are you an EU student?",
        "application_status" => "Current Application Status",
        "notes_to_applicant" => "Notes from Reviewer to Applicant",
        "reviewer" => "Reviewer Name/Title",
        "application_nickname" => "Nickname for the Application Submission",
      );

      if (in_array($jCurrentArticle->application_status, array("Accepted")) ) {
        echo '<div class="resultDiv acceptedResultDiv"> ';
        echo '<p> Your application has been accepted.</p>';
        echo '</div>';
        $props_to_ignore_for_this_app_status = array("notes_internal"); }

      else if (in_array($jCurrentArticle->application_status, array("Rejected")) ) {
        echo '<div class="resultDiv rejectedResultDiv"> ';
        echo '<p> Your application has been rejected. </p>';
        echo '</div>';
        $props_to_ignore_for_this_app_status = array("notes_internal"); }

      else if (in_array($jCurrentArticle->application_status, array("Under Review")) ) {
        echo '<div class="resultDiv underReviewResultDiv"> ';
        echo '<p> Your application is still under review. </p>';
        echo '</div>';
        $props_to_ignore_for_this_app_status = array("notes_to_applicant", "reviewer", "notes_internal");   }

      else if (in_array($jCurrentArticle->application_status, array("Pending")) ) {
        echo '<div class="resultDiv pendingResultDiv"> ';
        echo '<p> Your application is still pending review. </p>';
        echo '</div>';
        $props_to_ignore_for_this_app_status = array("notes_to_applicant", "reviewer", "notes_internal"); }

      // for main application section, filled out by user
      $props_to_ignore_always = array("notes_internal", "id", "asset_id", "alias", "introtext", "fulltext", "state", "catid");
      $props_to_ignore_always = array_merge($props_to_ignore_always, array("created_by", "created_by_alias", "modified", "modified_by", "checked_out"));
      $props_to_ignore_always = array_merge($props_to_ignore_always, array("checked_out_time", "publish_up", "publish_down", "images", "urls"));
      $props_to_ignore_always = array_merge($props_to_ignore_always, array("attribs", "version", "ordering", "metakey", "metadesc"));
      $props_to_ignore_always = array_merge($props_to_ignore_always, array("access", "hits", "metadata", "featured", "language"));
      $props_to_ignore_always = array_merge($props_to_ignore_always, array("xreference"));

      foreach ($meInAnArray as $propertyKey => $propertyValue) {

        if (!in_array($propertyKey, $props_to_ignore_always)) {

          if (!in_array($propertyKey, $props_to_ignore_for_this_app_status)) {

            echo '<div class="app_view_section">';
            echo '<p class="app_view_prop_title">'.$property_keys_readable[$propertyKey].'</p>';
            echo '<p class="app_view_prop_value">'.$propertyValue.'</p>';
            echo '</div>'; } }

      }

      ?>

    </div>
</div>
