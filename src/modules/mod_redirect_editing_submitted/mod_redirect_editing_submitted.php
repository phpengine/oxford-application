<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_footer
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$jinput = JFactory::getApplication()->input;

$option = $jinput->get('option', '', 'STRING');
$layout = $jinput->get('layout', '', 'STRING');

if ($option = "cck" && $layout = "edit" ) {

  $id = $jinput->get('id', '', 'INTEGER');
  $one_course_app = $ticketRow = \JCckContentArticle::getRow($id, "");

  $isBlockedState = false;
  foreach (array("Under Review") as $blocked_status) {
    if ($blocked_status == $one_course_app->application_status) {
      $isBlockedState = true; } }

  if ($isBlockedState==true) {
    $allDone = JFactory::getApplication();
    $allDone->enqueueMessage(JText::_('This application cannot currently be edited.'), 'error');;
    $allDone->redirect('index.php?option=com_content&view=article&id=7&Itemid=111'); } }

require JModuleHelper::getLayoutPath('mod_redirect_editing_submitted', $params->get('layout', 'default'));
