<?php
/**
* @version 			SEBLOD 3.x Core
* @package			SEBLOD (App Builder & CCK) // SEBLOD nano (Form Builder)
* @url				http://www.seblod.com
* @editor			Octopoos - www.octopoos.com
* @copyright		Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
**/

defined( '_JEXEC' ) or die;

// Helper
class modCCKBreadCrumbsHelper
{
	// getList
	public static function getList( &$params )
	{
		$app			=	JFactory::getApplication();
		$pathway_mode	=	$params->get( 'pathway', 0 );
		$list			=	array();

		if ( $pathway_mode == 2 ) {
			$Itemid		=	$app->input->getInt( 'Itemid', 0 );
			$pathway	=	JTable::getInstance( 'menu' );
			$items		=	$pathway->getPath( $Itemid );
			$count 		=	count( $items );
			
			for ( $i = 1, $j = 0; $i < $count; $i++ ) {
				$list[$j]			=	new stdClass;
				$list[$j]->name		=	stripslashes( htmlspecialchars( $items[$i]->title, ENT_COMPAT, 'UTF-8' ) );
				if ( $items[$i]->type == 'alias' ) {
					$registry		=	new JRegistry;
					$registry->loadJSON( $items[$i]->params );
					$item_id		=	$registry->get( 'aliasoptions' );
				} else {
					$item_id		=	$items[$i]->id;
				}
				$list[$j++]->link	=	JRoute::_( 'index.php?Itemid='.$item_id );
			}
		} elseif ( $pathway_mode == 1 ) {
			$base		=	$app->getCfg( 'sef_rewrite' ) ? JURI::base( true ).'/' : JURI::base( true ).'/index.php/';
			$pathway	=	$app->getPathway();
			$items		=	$pathway->getPathWay();
			$count 		=	count( $items );
			
			for ( $i = 0; $i < $count; $i++ ) {
				$items[$i]->name	=	stripslashes( htmlspecialchars( $items[$i]->name, ENT_COMPAT, 'UTF-8' ) );
				$items[$i]->link	=	JRoute::_( $items[$i]->link );
				if ( ! ( strpos( $items[$i]->link, $base.'component/' ) !== false ) ) {
					$list[]	=	$items[$i];
				}
			}
		} else {
			$pathway	=	$app->getPathway();
			$items		=	$pathway->getPathWay();
			$count 		=	count( $items );

			for ( $i = 0; $i < $count; $i++ ) {
				$list[$i]		=	new stdClass;
				$list[$i]->name	=	stripslashes( htmlspecialchars( $items[$i]->name, ENT_COMPAT, 'UTF-8' ) );
				$list[$i]->link	=	JRoute::_( $items[$i]->link );
			}
		}

		return self::_getItems( $params, $list );
	}
	
	// getItems
	protected static function _getItems( &$params, $items )
	{
		$app	=	JFactory::getApplication();
		
		if ( $params->get( 'showHome', 1 ) ) {
			$item		=	new stdClass;
			$item->name	=	htmlspecialchars( $params->get( 'homeText', JText::_( 'MOD_CCK_BREADCRUMBS_HOME' ) ) );
			$item->link	=	JRoute::_( 'index.php?Itemid='.$app->getMenu()->getDefault()->id );
			array_unshift( $items, $item );
		}
		
		return $items;
	}

	// setSeparator
	public static function setSeparator( $custom = null )
	{
		$lang	=	JFactory::getLanguage();
		
		if ( $custom == null ) {
			if ( $lang->isRTL() ) {
				$_separator	=	JHtml::_( 'image', 'system/arrow_rtl.png', NULL, NULL, true );
			} else {
				$_separator	=	JHtml::_( 'image', 'system/arrow.png', NULL, NULL, true );
			}
		} else {
			$_separator	=	htmlspecialchars( $custom );
		}
		
		return $_separator;
	}
}
?>