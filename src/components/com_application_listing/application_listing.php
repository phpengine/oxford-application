<?php
/**
 * @version     1.0.0
 * @package     com_application_listing
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Big Boss <bigboss@golden-contact-computing.co.uk> - http://www.golden-contact-computing.co.uk
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

// Execute the task.
$controller	= JControllerLegacy::getInstance('Application_listing');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
