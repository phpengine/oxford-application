<?php

/****************************************************************
 * @version		1.0.0 ticketmaster $							*
 * @package		ticketmaster									*
 * @copyright	Copyright � 2009 - All rights reserved.			*
 * @license		GNU/GPL											*
 * @author		Robert Dam										*
 * @author mail	info@rd-media.org								*
 * @website		http://www.rd-media.org							*
 ***************************************************************/

function showprice($holder,$price,$currency) {
	
		if ($holder == 1) { $price = $currency.' '.number_format($price, 2, ',', '.'); }
		if ($holder == 2) { $price = number_format($price, 2, ',', '.').' '.$currency; }
		if ($holder == 3) { $price = $currency.' '.number_format($price, 2, ',', ''); }
		if ($holder == 4) { $price = number_format($price, 2, ',', '').' '.$currency; }
		if ($holder == 5) { $price = $currency.' '.number_format($price, 2, '.', ''); }
		if ($holder == 6) { $price = number_format($price, 2, '.', '').' '.$currency; }
		if ($holder == 7) { $price = number_format($price, '2' , '.', ',').' '.$currency; }
		if ($holder == 8) { $price = $currency.' '.number_format($price, '2' , '.', ','); }	
		if ($holder == 9)  { $price = number_format($price, '0' , '', ',').' '.$currency; }	
		if ($holder == 10) { $price = $currency.' '.number_format($price, '0' , '', ','); }	
		if ($holder == 11) { $price = number_format($price, '0' , '', '.').' '.$currency; }	
		if ($holder == 12) { $price = $currency.' '.number_format($price, '0' , '', '.'); }					
    
	return $price;  
}

function showmonth($holder) {
	
		if ($holder == 01) { $month = JText::_('COM_TICKETMASTER_JANUARI'); }
		if ($holder == 02) { $month = JText::_('COM_TICKETMASTER_FEBRUARI'); }
		if ($holder == 03) { $month = JText::_('COM_TICKETMASTER_MARCH'); }
		if ($holder == 04) { $month = JText::_('COM_TICKETMASTER_APRIL'); }
		if ($holder == 05) { $month = JText::_('COM_TICKETMASTER_MAY'); }
		if ($holder == 06) { $month = JText::_('COM_TICKETMASTER_JUNE'); }
		if ($holder == 07) { $month = JText::_('COM_TICKETMASTER_JULY'); }
		if ($holder == 08) { $month = JText::_('COM_TICKETMASTER_AUGUST'); }	
		if ($holder == 09) { $month = JText::_('COM_TICKETMASTER_SEPTEMBER'); }	
		if ($holder == 10) { $month = JText::_('COM_TICKETMASTER_OCTOBER'); }	
		if ($holder == 11) { $month = JText::_('COM_TICKETMASTER_NOVEMBER'); }	
		if ($holder == 12) { $month = JText::_('COM_TICKETMASTER_DECEMBER'); }					
    
	return $month;  
}

	function sendTransaction($eid) {
		
		global $mainframe;
		
		$db = JFactory::getDBO();

		## Making the query for getting the orders
		$sql='SELECT  a.*, c.name, c.emailaddress, e.eventname
			  FROM #__ticketmaster_orders AS a, #__ticketmaster_clients AS c, #__ticketmaster_events AS e
			  WHERE a.userid = c.userid
			  AND a.eventid = e.eventid
			  AND ordercode = '.(int)$eid.''; 
	 
		$db->setQuery($sql);
		$info = $db->loadObjectList();
		
		$k = 0;
		for ($i = 0, $n = count($info); $i < $n; $i++ ){
			
			$row  = &$info[$i]; 
			## Tickets are saved as: eTicket-1000
			## Create attachments the same as saved
			$attachment[] = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'tickets'.DS.'eTicket-'.$row->orderid.'.pdf';
			$name		= $row->name;
			$email		= $row->emailaddress;
			$event		= $row->eventname;
			$k=1 - $k;
			
		}	

		## Getting the desired info from the configuration table
		$sql = "SELECT * FROM #__ticketmaster_config
				WHERE configid = 1 ";
		 
         $db->setQuery($sql);
         $config = $db->loadObject();
		
		$countattachments = count($attachment);
		
		$message        = str_replace('%%NAME%%', $name, $config->sendpdfmail);
		$createsubject  = str_replace('%%EVENTNAME%%', $event, $config->subjectticketmail);
		
		if ($countattachments>0){
		
			$from 		 = $config->fromemail;
			$fromname 	 = $config->replytoname; 
			$recipient[] = $row->emailaddress;
			$cc[] 		 = $config->replytoname;
			$subject 	 = $createsubject;
			$body 	 	 = $message;
			$mode 		 = 1; 
			$bcc[] 		 = $config->bcc_email;
			$replyto 	 = $config->fromemail;
			$replytoname = $config->replytoname;	
				
			JUtility::sendMail($from, $fromname, $recipient, $subject, $body, $mode, $cc, $bcc, $attachment, $replyto, $replytoname);	
		}

		## Updating the order, PDF sent = 1
		$query = 'UPDATE #__ticketmaster_orders'
			. ' SET pdfsent = 1'
			. ' WHERE ordercode = '.$eid.'';
		
		## Do the query now	
		$db->setQuery( $query );
		
		## When query goes wrong.. Show message with error.
		if (!$db->query()) {
			$this->setError($db->getErrorMsg());
			return false;
		}

		## Updating the order, PDF sent = 1
		$query = 'UPDATE #__ticketmaster_tickets'
			. ' SET ticketssent = ticketssent+'.(int)$countattachments.''
			. ' WHERE ticketid = '.(int)$info->ticketid.'';
		
		## Do the query now	
		$db->setQuery( $query );
		
		## When query goes wrong.. Show message with error.
		if (!$db->query()) {
			$this->setError($db->getErrorMsg());
			return false;
		}

	}
?>