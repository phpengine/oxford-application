<?php

/**
 * @version		Ticketmaster Pro 1.0.2
 * @package		Ticketmaster
 * @copyright	Copyright � 2009 - All rights reserved.
 * @license		GNU/GPL
 * @author		Robert Dam
 * @author mail	info@rd-media.org
 * @website		http://www.rd-media.org
 *
 */

defined('_JEXEC') or die ('No Access to this file!');

jimport('joomla.application.component.controller');

class TicketmasterExtControllerOrder extends JControllerLegacy {

	function __construct() {
		parent::__construct();
		
		$this->id  = JRequest::getInt('id');
		## Getting the global DB session
		$session   = JFactory::getSession();
		## Gettig the orderid if there is one.
		$this->ordercode = $session->get('ordercode');
		
		## Check if the user is logged in.
		$user = JFactory::getUser();
		$this->userid = $user->id;		
		
	}
	
	function removeseat(){
			
		## Getting the id
		$id     = JRequest::getInt('id');
		## Get database information		
		$db  	= JFactory::getDBO();
		## Loading the application Joomla
		$app 	= JFactory::getApplication();	
		## Setting error to nothing!
		$error 	= '';	

		## Check if there are any orders, otherwise stop the script.
		$query = 'SELECT orderid, ticketid FROM #__ticketmaster_orders 
				  WHERE ordercode = '.(int)$this->ordercode.'
				  AND seat_sector = "'.(int)$id.'"'; 	 
		
		$db->setQuery($query);
		$item = $db->loadObject();
		
		$sql = 'SELECT * 
				FROM #__ticketmaster_tickets_ext 
				WHERE ticketid = '.(int)$item->ticketid.''; 	 
		
		$db->setQuery($sql);
		$coords = $db->loadObject();
					
		
		## Check if order is valid
		if($item->orderid == 0){		
			
			$error = '1';
			$msg = JText::_( 'COM_TICKETMASTEREXT_THIS_ORDER_IS_NOT_YOURS' );
		
			$arr = array('error' => $error, 'msg' => $msg, 'id' => $id);
			echo json_encode($arr);	
			exit();
		
		}else{

			## Update the tickets-totals that where removed.
			$query = 'UPDATE #__ticketmaster_orders SET seat_sector = 0 
					  WHERE orderid = '.$item->orderid.'';
			
			## Do the query now	
			$db->setQuery( $query );
			
			## When query goes wrong.. Show message with error.
			if (!$db->query()) {
				$this->setError($db->getErrorMsg());

				$error = '1';
				$msg = JText::_( 'COM_TICKETMASTEREXT_COULD_NOT_UPDATE_ORDER_TABLE' );
			
				$arr = array('error' => $error, 'msg' => $msg, 'id' => $id);
				echo json_encode($arr);	
				exit();
			}
			
			### NOW UPADTE THE COORDS TABLE
			$query = 'UPDATE #__ticketmaster_coords 
					  SET orderid = 0, booked = 0 
					  WHERE id = '.(int)$id.' ';
			
			## Do the query now	
			$db->setQuery( $query );
			
			## When query goes wrong.. Show message with error.
			if (!$db->query()) {
				$this->setError($db->getErrorMsg());

				$error = '1';
				$msg = JText::_( 'COM_TICKETMASTEREXT_COULD_NOT_UPDATE_COORDS_TABLE' );
			
				$arr = array('error' => $error, 'msg' => $msg, 'id' => $id);
				echo json_encode($arr);	
				exit();

			}	
			
			$error = '0';
			$msg = JText::_( 'COM_TICKETMASTEREXT_THIS_SEAT_IS_REMOVED' );
		
			$arr = array('error' => $error, 'msg' => $msg, 'id' => $id, 'background' => $coords->background_color, 'color' => $coords->border_color );
			echo json_encode($arr);	
			exit();
		
		}

	}
	
	function saveseat(){
		
		## Getting the id
		$id     = JRequest::getInt('id');
		## Get database information		
		$db  	= JFactory::getDBO();
		## Loading the application Joomla
		$app 	= JFactory::getApplication();	
		## Setting error to nothing!
		$error 	= '';	
		
		$sql = 'SELECT c.*, t.multi_seat, t.type, t.background_color, t.border_color
				FROM #__ticketmaster_coords AS c, #__ticketmaster_tickets_ext AS t
				WHERE c.ticketid = t.ticketid
				AND c.id = '.(int)$id.'';			

		$db->setQuery($sql);
		$item = $db->loadObject();
		
		## Check if the ticket is a multiseat ticket.		
		if ($item->multi_seat == 0){
			
			## Is it a chair or a seactor?
			if($item->type == 2){
				
				## It is a sector --> throw message.
				$msg = JText::_( 'COM_TICKETMASTEREXT_YOU_DONT_HAVE_TO_SELECT_A_SEAT' );
				$arr = array('error' => '1', 'msg' => $msg, 'id' => $this->id, 'border' => $item->border_color, 'background' => $item->background_color );
				echo json_encode($arr);	
				exit();				
				
			}
			
			## Get all orders for checking amount.
			$sql = 'SELECT *, count(orderid) AS total FROM #__ticketmaster_orders 
					WHERE ordercode = '.(int)$this->ordercode.'
					AND seat_sector = 0
					AND ticketid = '.$item->ticketid.'
					LIMIT 0,1'; 
						 
			$db->setQuery($sql);
			$order = $db->loadObject();				
			
			if($order->total > 0){
				
				### OK -- ORDER THE TICKET -- PERFORM SOME EXTRA CHECKS ###
				
				if ($item->booked == 1) {				

					$msg = JText::_( 'COM_TICKETMASTEREXT_THIS_SEAT_IS_TAKEN' );				
					$arr = array('error' => '1', 'msg' => $msg, 'id' => $id);
					echo json_encode($arr);	
					exit();		
				
				}				
						
				## OK NOW WE KNOW WHAT ORDER NEEDS TO BE UPDATED IN THE COORDS TABLE
				## AND WE NEED TO UPDATE THE ORDER TABLE
				
				$query = 'UPDATE #__ticketmaster_orders SET seat_sector = '.(int)$id.' WHERE orderid = '.(int)$order->orderid.' ';

				## Do the query now	
				$db->setQuery( $query );
				
				## When query goes wrong.. Show message with error.
				if (!$db->query()) {
					$msg = JText::_( 'COM_TICKETMASTEREXT_ERROR_UPDATE_QUERY_1' );
					$arr = array('error' => '1', 'msg' => $msg, 'id' => $this->id);
					echo json_encode($arr);	
					exit();	
				}
				
				### NOW UPADTE THE COORDS TABLE
				$query = 'UPDATE #__ticketmaster_coords 
						  SET orderid = '.(int) $order->orderid.', booked = 1 
						  WHERE id = '.$id.' ';
				
				## Do the query now	
				$db->setQuery( $query );
				
				## When query goes wrong.. Show message with error.
				if (!$db->query()) {
					$msg = JText::_( 'COM_TICKETMASTEREXT_ERROR_UPDATE_QUERY_2' );
					$arr = array('error' => '1', 'msg' => $msg, 'id' => $this->id);
					echo json_encode($arr);	
					exit();	
				}	
				
				
				$msg = JText::_( 'COM_TICKETMASTEREXT_SEAT_HAS_BEEN_ORDERED' );
				
				$arr = array('error' => '0', 
							 'msg' => $msg, 
							 'id' => $item->seatid,
							 'seat' => $item->id,
							 'border' => $item->border_color,
							 'background' => $item->background_color);
				
				echo json_encode($arr);	
				exit();			
				
			}else{
				
				if ($item->total == 0) {
					
						## Get all orders for checking amount.
						$sql = 'SELECT *, count(orderid) AS total 
								FROM #__ticketmaster_orders 
								WHERE ordercode = '.(int)$this->ordercode.'
						        AND ticketid = '.(int)$item->ticketid.''; 
									 
						$db->setQuery($sql);
						$ordereditems = $db->loadObject();						
						
						if( $ordereditems->total == 0) {		
					
							## Customer has not ordered a seat in this section. (Stop ordering this seat)
							$msg = JText::_( 'COM_TICKETMASTEREXT_NO_ORDER_FOR_THIS_SEAT' );
							$arr = array('error' => '1', 'msg' => $msg, 'id' => $this->id);
							echo json_encode($arr);	
							exit();							
						
						}else{
							
							## Customer has not ordered a seat in this section. (Stop ordering this seat)
							$msg = JText::_( 'COM_TICKETMASTEREXT_NOT_ORDERED_FOR_ROW' );
							$arr = array('error' => '1', 'msg' => $msg, 'id' => $this->id);
							echo json_encode($arr);	
							exit();								
						}
				}
			}

		}else{ 
			
			#### MULTI SEAT PART -- LESS CHECKS #####
			
			## Is it a chair or a seactor?
			if($item->type == 2){

				## It is a sector --> throw message.
				$msg = JText::_( 'COM_TICKETMASTEREXT_YOU_DONT_HAVE_TO_SELECT_A_SEAT' );
				$arr = array('error' => '1', 'msg' => $msg, 'id' => $this->id);
				echo json_encode($arr);	
				exit();				
				
			}

			## Check if seat is bookoed? Get lost and warn the user directly.
			if ($item->booked == 1) {				

				$msg = JText::_( 'COM_TICKETMASTEREXT_THIS_SEAT_IS_TAKEN' );				
				$arr = array('error' => '1', 'msg' => $msg, 'id' => $id);
				echo json_encode($arr);	
				exit();		
			
			}	
			
			## Double check if the clicked seat is a ordered seat:
			
			## Lets check if the clicked item has a prent or not.
			$sql = 'SELECT count(ticketid) AS total
					FROM #__ticketmaster_tickets
					WHERE parent = '.$item->ticketid.'';

			$db->setQuery($sql);
			$parent = $db->loadObject();			
			
			if($parent->total == 0){

				## Yes it is a parent so this query should get the total:
				$sql = 'SELECT count(orderid) AS total 
						FROM #__ticketmaster_orders 
						WHERE ordercode = '.(int)$this->ordercode.'
						AND seat_sector != 0
						AND ticketid = '.$item->ticketid.'
						LIMIT 0,1'; 
							 
				$db->setQuery($sql);
				$ordered = $db->loadObject();
				
				$sql = 'SELECT COUNT(orderid) AS total 
						FROM #__ticketmaster_orders
						WHERE ordercode = '.(int)$this->ordercode.'
						AND ticketid = '.$item->ticketid.'';	
						
				$db->setQuery($sql);
				$to_order = $db->loadObject();							
				
			}else{

				## No it is not a parent so this query should get the total:
				$query = 'SELECT count(o.orderid) AS total 
						  FROM #__ticketmaster_orders AS o, #__ticketmaster_tickets AS t 
						  WHERE o.ordercode = '.(int)$this->ordercode.'
						  AND o.seat_sector != 0
						  AND t.parent = '.$item->ticketid.'
						  AND o.ticketid = t.ticketid
						  LIMIT 0,1'; 					
							 
				$db->setQuery($query);
				$ordered = $db->loadObject();
				
				$sql = 'SELECT COUNT(a.orderid) AS total 
						FROM #__ticketmaster_orders AS a, #__ticketmaster_tickets AS t
						WHERE a.ordercode = '.(int)$this->ordercode.'
						AND a.ticketid = t.ticketid
						AND t.parent = '.$item->ticketid.'';	
						
				$db->setQuery($sql);
				$to_order = $db->loadObject();																	
				
			}						
			
			## OKAY, we do now know that the $ordered->total has got the amount of tickets for this order.
			## We also know how many items may be orderd: $n
			if ($ordered->total >= $to_order->total){
				
					$msg = JText::_( 'COM_TICKETMASTEREXT_YOU_HAVE_CHOSEN_ALL_SEATS_FOR_THIS_ORDER' );				
					$arr = array('error' => '1', 'msg' => $msg , 'id' => $id);
					echo json_encode($arr);	
					exit();
										
			}		

			
			## Get all orders for checking amount.
			$sql = 'SELECT *, count(orderid) AS total 
					FROM #__ticketmaster_orders 
					WHERE ordercode = '.(int)$this->ordercode.'
					AND seat_sector = 0
					LIMIT 0,1'; 					
						 
			$db->setQuery($sql);
			$order = $db->loadObject();				
			
			if($order->total > 0){	
			
				if ($item->booked == 1) {				

					$msg = JText::_( 'COM_TICKETMASTEREXT_THIS_SEAT_IS_TAKEN' );				
					$arr = array('error' => '1', 'msg' => $msg, 'id' => $id);
					echo json_encode($arr);	
					exit();		
				
				}	
				
				## OK NOW WE KNOW WHAT ORDER NEEDS TO BE UPDATED IN THE COORDS TABLE
				## AND WE NEED TO UPDATE THE ORDER TABLE
				
				$query = 'UPDATE #__ticketmaster_orders SET seat_sector = '.(int)$id.' WHERE orderid = '.(int)$order->orderid.' ';

				## Do the query now	
				$db->setQuery( $query );
				
				## When query goes wrong.. Show message with error.
				if (!$db->query()) {
					$msg = JText::_( 'COM_TICKETMASTEREXT_ERROR_UPDATE_QUERY_1' );
					$arr = array('error' => '1', 'msg' => $msg, 'id' => $this->id);
					echo json_encode($arr);	
					exit();	
				}
				
				### NOW UPADTE THE COORDS TABLE
				$query = 'UPDATE #__ticketmaster_coords 
						  SET orderid = '.(int) $order->orderid.', booked = 1 
						  WHERE id = '.$id.' ';
				
				## Do the query now	
				$db->setQuery( $query );
				
				## When query goes wrong.. Show message with error.
				if (!$db->query()) {
					$msg = JText::_( 'COM_TICKETMASTEREXT_ERROR_UPDATE_QUERY_2' );
					$arr = array('error' => '1', 'msg' => $msg, 'id' => $this->id);
					echo json_encode($arr);	
					exit();	
				}	
				
				
				$msg = JText::_( 'COM_TICKETMASTEREXT_SEAT_HAS_BEEN_ORDERED' );
				
				$arr = array('error' => '0', 
							 'msg' => $msg, 
							 'id' => $item->seatid,
							 'background' => $item->background_color);
				
				echo json_encode($arr);	
				exit();			
					
			
			}else{

				## No more seats to choose :) (Every order has a seat.
				$msg = JText::_( 'COM_TICKETMASTEREXT_REACHED_MAXIMUM_TICKETS_FOR_ORDER' );
				$error = '1';
				$arr = array('error' => $error, 'msg' => $msg, 'id' => (int)$id);
				echo json_encode($arr);	
				exit();					
			
			}					
		
		}

	}

}	
?>
