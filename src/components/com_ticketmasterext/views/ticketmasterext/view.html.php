<?php

/**
 * @version		Ticketmaster Pro 1.0.2
 * @package		ticketmaster
 * @copyright	Copyright � 2009 - All rights reserved.
 * @license		GNU/GPL
 * @author		Robert Dam
 * @author mail	info@rd-media.org
 * @website		http://www.rd-media.org
 */

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class TicketmasterExtViewTicketmasterext extends JViewLegacy {

	function display($tpl = null) {
		
		## Model is defined in the controller
		$model	= $this->getModel('eventdetails');
		
		## Getting the items into a variable
		$data	= $this->get('data');
		$config	= $this->get('config');
		$seats	= $this->get('cartitems');
		
		## Getting specific items, depending on ticket kind
		if($data->multi_seat != 1) {
			
			$items	= $this->get('seats');
			$order	= $this->get('cart');
			
		}else{
			
			## Check if an item has childs.
			$childs = $this->get('checkchilds');
			
			## if childs > 0 than run the queries below.
			if($childs->total != 0){ 
			
				$items	= $this->get('nochilds');
				$order	= $this->get('cartnochilds');
			
			## There are no childs, use this queries.
			}else{ 
			
				$items	= $this->get('nochilds');
				$order	= $this->get('cart');			
			
			}
			
		}		
		
		$this->assignRef('order', $order);
		$this->assignRef('seats', $seats);
		$this->assignRef('items', $items);
		$this->assignRef('data', $data);
		$this->assignRef('config', $config);

		#### THE JAVASCRIPT DEPENDS ON THE JOOMLA VERSION YOU ARE RUNNING ####
		#### SO WE ARE LOADING A TMPL THAT IS FOR THE CORRECT J-VERSION   ####
		$isJ30 = version_compare(JVERSION, '3.0.0', 'ge');
		
		if($isJ30){
			$tpl = 'bootstrap';
		}
		
		parent::display($tpl);		

	
	}

}
?>
