<?php 
/**
 * @version		1.0.2
 * @package		Ticketmaster Pro - Extending Ticketmaster!
 * @copyright	Copyright � 2009 - All rights reserved.
 * @license		GNU/GPL
 * @author		Robert Dam
 * @author mail	info@rd-media.org
 * @website		http://www.rd-media.org
 */

## Check if the file is included in the Joomla Framework
defined('_JEXEC') or die ('No Acces to this file!');

jimport( 'joomla.filter.output' );

$app 	  = JFactory::getApplication();
## Get document type and add it.
$document = JFactory::getDocument();
$document->addStyleSheet( 'components/com_ticketmasterext/assets/css/component.css' );

$document->addScript( JURI::root(true).'/components/com_ticketmasterext/assets/js/jquery1.4.2.js');
$document->addScript( JURI::root(true).'/components/com_ticketmasterext/assets/js/jquery-ui-1.8.2.custom.min.js');
$document->addScript( JURI::root(true).'/components/com_ticketmasterext/assets/js/jquery-ui-punch.js');

## Getting the global DB session
$session = JFactory::getSession();

## The image of the seat chart
$seatchart = JURI::root(true).'/administrator/components/com_ticketmasterext/assets/seatcharts/'.$this->data->seat_chart;
$image = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmasterext'.DS.'assets'.DS.'seatcharts'.DS.$this->data->seat_chart;
## Get the image size
list($width, $height, $type, $attr) = getimagesize($image);

?>

<style>

#glassbox {
	/* PLEASE DO NOT CHANGE */
	background:#FFF;
	height:<?php echo $height+20; ?>px;
	background-repeat:no-repeat;
	background-position: 0px 30px;
	position:relative;
	width:<?php echo $width; ?>px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
}

</style>

<?php if (count($this->order) == 0){ ?>
	<div class="warning"><?php echo JText::_( 'COM_TICKETMASTEREXT_NO_ORDERS' ); ?></div>
<?php }elseif ($this->data->multi_seat == 1) {  ?>
	<div class="info"><?php echo JText::_( 'COM_TICKETMASTEREXT_CHOICE_ALL' ); ?></div>
<?php } ?>

<div style="width:100%; height:25px; margin:8px auto 10px auto; color:#000; text-align:center; padding-bottom:2px;">
    
    <div id="ajaxLoader" style="display:none; text-align:center;">
      <img src="components/com_ticketmasterext/assets/images/loading-bar.gif" width="25" height="25" />
    </div>
    
    <div id="ajaxMessage" style="display:none; text-align:center; margin-bottom:2px;"></div>    
    
</div>

<div style="width:190px; position:absolute; padding-left:<?php echo $width+35; ?>px; margin-top: 4px;">

    <div style="font-size:115%; padding-bottom:5px; width:220px;">
		<?php echo JText::_( 'COM_TICKETMASTEREXT_YOU_MAY_CHOOSE' ); ?>
    </div>
    
    <?php if (count($this->order) > 0) { ?>
    
		<?php
        $k = 0;
        for ($i = 0, $n = count($this->order); $i < $n; $i++ ){
                
                ## Give give $row the this->item[$i]
                $row =  $this->order[$i];
                
                if ($this->data->multi_seat != 1){
                    $background_color = $row->background_color;
                }else{
                    $background_color = $this->data->background_color;
                }
                
                ?>
                <div style="heigth:35px; width:220px; margin: 4px 0px 4px 0px; border:1px solid #FFF;">
               
                    
                    <div style="width:180px; height:23px; line-height:23px; border:1px solid #CCC; position:absolute; margin: 0px 0px 0px 30px; 
                            -moz-border-radius: 2px; -webkit-border-radius: 2px; padding-left: 8px; overflow:hidden;">
                        <?php echo $row->ticketname; ?>
                    </div>
                    
                    <div id="seat-choice" class="seat-choice" 
                        style="background-color:#<?php echo $background_color; ?>; border-color:#<?php echo $row->border_color; ?>;">
                        <?php echo $row->total; ?>x
                    </div>                
                
                </div>
                <?php
                
           $k=1 - $k;
           }			
        ?>	
        
        <div style="font-size:115%; padding-bottom:5px; width:220px; clear:both; margin-top: 10px; margin-bottom:5px;">
            <?php echo JText::_( 'COM_TICKETMASTEREXT_CHOSEN_SEATS' ); ?>
        </div>  
       
        <div id="items" style="margin-bottom:10px; padding-bottom:20px; border:0px;">
            
            <?php $k = 0; 
			
				  for ($i = 0, $n = count($this->seats); $i < $n; $i++ ){ 
				  $row =  $this->seats[$i];
			?>
                
            <div id="<?php echo $row->seat_sector; ?>" class="item" style="margin:0px; padding:2px;">
                <div id="seat-choice" class="seat-choice" style="background-color:#<?php echo $background_color; ?>; 
                		float:left; border-color:#CCC; cursor:pointer; font-size:90%; margin:0px;">
                    <?php echo $row->seatid; ?>
                </div>                 
            </div>
			
			<?php $k=1 - $k; } ?>

        </div>
		
        <div style="padding-top: 15px;">
            <div id="trash" style="clear:both; margin-top: 25px;" class="out">
               <div id="trash-txt">
                 <?php echo JText::_( 'COM_TICKETMASTEREXT_DROPPABLE_REMOVE' ); ?>
               </div>
            </div>
        </div> 
        
        <div id="close" class="close">
            <div id="close-txt"><?php echo JText::_( 'COM_TICKETMASTEREXT_CLOSE_THIS_WINDOW' ); ?></div>
        </div>           
        
        <div style="font-size:115%; padding-bottom:5px; width:220px; clear:both; margin-top: 15px; margin-bottom:5px;">
            <?php echo JText::_( 'COM_TICKETMASTEREXT_INFORMATION' ); ?>
        </div>          
        
        <ul>
        	<li style="font-size:95%;"><?php echo JText::_( 'COM_TICKETMASTEREXT_DROPPABLE_ORDERED_INFO' ); ?></li>
        	<li style="font-size:95%;"><?php echo JText::_( 'COM_TICKETMASTEREXT_DROPPABLE_ORDERED_SEATS' ); ?></li>            
        	<li style="font-size:95%;"><?php echo JText::_( 'COM_TICKETMASTEREXT_DROPPABLE_REMOVE_INFO_1' ); ?></li>
            <li style="font-size:95%;"><?php echo JText::_( 'COM_TICKETMASTEREXT_DROPPABLE_REMOVE_INFO_2' ); ?></li>
        </ul>
    
    <?php }else{ ?>	
    
        <div style="font-size:95%; padding-bottom:5px; padding-top:8px; width:220px;">
            <?php echo JText::_( 'COM_TICKETMASTEREXT_YOUR_CART_IS_EMPTY' ); ?>
        </div>    
        
    <?php } ?>
    
</div>

<div style="border: solid 1px #CCCCCC; padding: 0px 0px 25px 9px; width:<?php echo $width+12; ?>px; float:left;	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;  margin-top: 4px;">

    <div id="glassbox" style="background-image: url(<?php echo $seatchart; ?>);">
    
    <?php 
    
           $k = 0;
           for ($i = 0, $n = count($this->items); $i < $n; $i++ ){
                
                ## Give give $row the this->item[$i]
                $row        = &$this->items[$i];
            
                $x 			 = $row->x_pos;
                $y 			 = $row->y_pos;
				$line_heigth = 'line-height:'. $row->heigth+2 .'px;';
                
                if ($row->booked > 0){
                
				    $style = 'color:#FFF; border-color:#000; background-color:#FF0000; pointer:none; '.$line_heigth;
                
				}else{
                    
					$style = 'color:#'.$row->font_color.'; border-color:#'.$row->border_color.'; '.$line_heigth;
                    
					if ($row->background_color != ''){
                        $background = $row->background_color;
                    }else{
                        $background = 'e1fdda';
                    }
					
                }
                
                ## This is a seat --> Load seat data.
                if ($row->type == 1){
                    echo '<div id="'.$row->id.'" class="seat-element" 
                            style="left:'.$x.'px; top:'.$y.'px; background-color:#'.$background.';
                                   width:'.$row->width.'px; height:'.$row->heigth.'px;
                                   position:absolute; '.$style.'">'.$row->seatid.'</div>';		
                }else{
					
                    echo '<div id="'.$row->id.'" class="seat-element" 
                            style="left:'.$x.'px; top:'.$y.'px;  background-color:#'.$background.'; 
                                   width:'.$row->width.'px; height:'.$row->heigth.'px;
                                   position:absolute; '.$style.'">
                                        <div style = "line-height:'.$row->heigth.'px;"><strong>'.$row->ticketname.'</strong></div>
                                   </div>';					
                }
                
           $k=1 - $k;
           }			
    ?>
    
    </div>

</div>

<p style="width: 800px; height:20px; margin:8px auto 0 auto; color:#000; text-align:center;">

</p>

<div id="respond"></div>



<script type="text/javascript">

var JQ = jQuery.noConflict();

JQ("#close").bind("click", function(e){
	window.parent.document.location.reload();
	parent.Mediabox.close();
});
		
JQ(".seat-element").bind("click", function(e){
	
	// Get the current clicked id :)
	var currentId = JQ(this).attr('id');
	
	//Give the chosen seat another color
	var color = JQ("#"+currentId+"").css("color");
	var background = JQ("#"+currentId+"").css("backgroundColor");		
	
	// Show Soading Bar
	JQ('#ajaxLoader').show();
	
	JQ.getJSON("index.php?option=com_ticketmasterext&controller=order&task=saveseat&id="+ currentId +"&format=raw",
		
	function(data){
		
		// Hide the Loading bar
		JQ('#ajaxLoader').hide();
		
		if (data.id == 0){
			
			JQ("#"+currentId+"").css('color', color);
            JQ("#ajaxMessage").html('<div id="failed"><?php echo JText::_( 'COM_TICKETMASTEREXT_UNEXPECTED_ERROR' ); ?></div>').hide().fadeIn(1000);
			setTimeout(function(){ JQ('#ajaxMessage').fadeOut(2000); }, 500);	
					
			return false;
		}
		
		if (data.error == 1){
			
			JQ("#"+currentId+"").css('color', color);
			JQ("#"+currentId+"").css('backgroundColor', background);
            JQ("#ajaxMessage").html('<div id="failed">'+ data.msg +'</div>').hide().fadeIn(500);
            setTimeout(function(){ JQ('#ajaxMessage').fadeOut(2000); }, 2000);			
			return false;
		}
		
		JQ("#"+currentId+"").css('color', '#FFF');		
		JQ("#"+currentId+"").css('backgroundColor', '#FF0000');		
		
		var elm = JQ('<div id="'+data.seat+'" class="item" style="padding:2px; border:0px;"><div id="seat-choice" class="seat-choice" style="background-color:#'+data.background+'; ?>; float:left; border-color:#<?php echo $row->border_color; ?>; cursor:pointer; font-size:90%;">'+data.id+'</div></div>').appendTo("#items");
		
		// Pass the element to makeDraggable
		makeDraggable(elm); 
				
		JQ("#ajaxMessage").html('<div id="success">'+ data.msg +'</div>').hide().fadeIn(1000);
		setTimeout(function(){ JQ('#ajaxMessage').fadeOut(2000); }, 2000);			

		
	});
	
});

JQ(document).ready(function () {
    makeDraggable(JQ(".item"));
});


function makeDraggable (jQueryElm) {
		
        jQueryElm.draggable({
                revert: false
        });
		
        JQ("#trash").droppable({
                tolerance: 'touch',
                over: function() {
					JQ(this).css('backgroundColor', '#F2F2F2');
                },
                out: function() {
                   	JQ(this).css('backgroundColor', '#FFF');
                },
                drop: function(e, source) {
					// Get the dragged element
					var $el = source.draggable;
					// The id of the draggable
					var currentId = $el.attr("id");
					var id = currentId;
					
					// Show Soading Bar
					JQ('#ajaxLoader').show();

					JQ.getJSON("index.php?option=com_ticketmasterext&controller=order&task=removeseat&id="+ id +"&format=raw",
						
					function(data){
						
						// Hide the Loading bar
						JQ('#ajaxLoader').hide();
						
						if(data.error == 1){
							
							JQ("#ajaxMessage").html('<div id="failed">'+ data.msg +'</div>').hide().fadeIn(1000);
							setTimeout(function(){ JQ('#ajaxMessage').fadeOut(2000); }, 2000);			
							return false;							
						
						}else{

							source.draggable.remove();
							
							JQ(".seat-element").attr("id", function() { 
								JQ("#"+currentId+"").css("backgroundColor", "#"+data.background);
								JQ("#"+currentId+"").css("border-color", "#"+data.color);
								JQ("#"+currentId+"").css('color', '#000');
							});
							
							JQ("#ajaxMessage").html('<div id="success">'+ data.msg +'</div>').hide().fadeIn(1000);
							setTimeout(function(){ JQ('#ajaxMessage').fadeOut(1000); }, 2000);								
						}
						
					});

					JQ(this).css('backgroundColor', '#FFF');
                }
        });
};

</script>