<?php
/**
 * @version		Ticketmaster Pro 1.0.2
 * @package		Ticketmaster
 * @copyright	Copyright © 2009 - All rights reserved.
 * @license		GNU/GPL
 * @author		Robert Dam
 * @author mail	info@rd-media.org
 * @website		http://www.rd-media.org
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.model' );

class TicketmasterExtModelTicketmasterExt extends JModelLegacy{

	function __construct(){
		parent::__construct();
		
		$array    = JRequest::getVar('cid', array(0), '', 'array');
		$this->id = (int)$array[0]; 
		
		## Getting the global DB session
		$session = JFactory::getSession();
		$this->ordercode = $session->get('ordercode');				
		
	}

   function getConfig() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## Making the query for showing all the clients in list function
			$query = 'SELECT payment_email_send, priceformat, valuta FROM #__ticketmaster_config WHERE configid = 1';
		 
		 	$db->setQuery($query);
		 	$this->data = $db->loadObject();
		}
		return $this->data;
	}


   function getSeats() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT c.*, t.ticketname, tt.background_color, tt.font_color, tt.border_color
					FROM #__ticketmaster_coords AS c,  #__ticketmaster_tickets AS t, #__ticketmaster_tickets_ext AS tt
					WHERE c.parent ='.(int)$this->id.'
					AND c.ticketid = t.ticketid
					AND c.ticketid = tt.ticketid'; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
	}
	
   function getNochilds() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT c.*, t.ticketname, tt.background_color
					FROM #__ticketmaster_coords AS c,  #__ticketmaster_tickets AS t, #__ticketmaster_tickets_ext AS tt
					WHERE c.ticketid = t.ticketid
					AND c.ticketid = tt.ticketid
					AND c.ticketid = '.(int)$this->id.''; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
	}	
	
   function getCheckChilds() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## This query checks if multi ticket has childs or not?
			$sql = 'SELECT COUNT(ticketid) AS total
					FROM #__ticketmaster_tickets
					WHERE parent = '.(int)$this->id.''; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObject();
		}
		return $this->data;
	}			

   function getData() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT * 
					FROM #__ticketmaster_tickets_ext
					WHERE ticketid ='.(int)$this->id.''; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObject();
		}
		return $this->data;
	}

	function _buildContentWhereTicket() {

		$db = JFactory::getDBO();
		
		$where = array();

		$where[] = 'a.eventid = e.eventid';
		$where[] = 'a.ticketid = t.ticketid';
		$where[] = 'a.ticketid = tt.ticketid';
		$where[] = 'a.ordercode = '.(int)$this->ordercode;


		$where 		= ( count( $where ) ? ' WHERE '. implode( ' AND ', $where ) : '' );

		return $where;
	}

   function getCart() {
   		
		if (empty($this->_data)) {

		 	$db    = JFactory::getDBO();
			$where = $this->_buildContentWhereTicket();
			
			## Making the query for showing all the clients in list function
			$sql='SELECT a.*, t.ticketname, t.ticketprice, t.ticketdate, t.starttime, e.eventname, tt.*, COUNT(a.orderid) AS total
			      FROM #__ticketmaster_orders AS a, #__ticketmaster_events AS e, #__ticketmaster_tickets AS t, #__ticketmaster_tickets_ext AS tt'
				  .$where.' GROUP BY a.ticketid' ; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
   }
   
   function getCartNoChilds() {
   		
		if (empty($this->_data)) {

		 	$db    = JFactory::getDBO();
			$where = $this->_buildContentWhereTicket();
			
			## Making the query for showing all the clients in list function
			$sql='SELECT a.*,COUNT(a.orderid) AS total, t.ticketname 
				  FROM #__ticketmaster_orders AS a, #__ticketmaster_tickets AS t
				  WHERE a.ordercode = '.(int)$this->ordercode.'
				  AND a.ticketid = t.ticketid
				  AND t.parent = '.(int)$this->id.'
				  GROUP BY ticketid'; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
   }   
   
   function getCartItems() {
   		
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
			
			## Making the query for showing all the clients in list function
			$sql='SELECT *
			      FROM #__ticketmaster_orders AS a, #__ticketmaster_coords AS c, #__ticketmaster_tickets_ext AS tt
				  WHERE a.orderid = c.orderid
				  AND c.ticketid = tt.ticketid
				  AND a.ordercode = '.(int)$this->ordercode; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
   }     

}
?>