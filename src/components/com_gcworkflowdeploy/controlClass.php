<?php
/**
 * GCFW Hello World
 * 
 * @package    GC Quickstart Deployer
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCQuickstartDeploye is copyrighted. 
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'modelClass.php';
require_once JPATH_COMPONENT.DS.'viewClass.php';

class GCWorkflowDeployerFEControlClass {
	
	private $configs;	
	private $view;
	private $model;
	private $errors;
	private $messages;
	
	private $loginstatus;
	private $session;

	public function __construct(){
		$this->configs = new GCWorkflowDeployerConfigClass();
		$this->view = new GCWorkflowDeployerFEViewClass();
		$this->model = new GCWorkflowDeployerModelClass();
		$this->messages = array();
		$this->checkLoginStatus();
		$this->session = JFactory::getSession();
	}

	public function pageNotLoggedIn() {
		include('controllers/notloggedin.php');
	}

	public function pageHome() {
		include('controllers/home.php');
	}

	public function pageDeleteInstall() {
		include('controllers/deleteinstall.php');
	}

	public function pagePullStart() {
		include('controllers/pullstart.php');
	}

	public function pagePushStart() {
		include('controllers/pushstart.php');
	}

	public function pagePushType() {
		include('controllers/pushtype.php');
	}

	public function pagePullHistory() {
		include('controllers/pullhistory.php');
	}

	public function pagePushHistory() {
		include('controllers/pushhistory.php');
	}
	
	public function pageSiteDetails() {
		include('controllers/pushdetails.php');
	}

	public function pageServerDetails() {
		include('controllers/servdetails.php');
	}

	public function pageVerifyPush() {
		include('controllers/verifypush.php');
	}

    public function pageProcessLocalPush() {
        include('controllers/processlocalpush.php');
    }

    public function pageProcessRemotePush() {
        include('controllers/processremotepush.php');
    }
	
	public function pageProcessPull() {
		include('controllers/processpull.php');
	}
	
	public function pagePushDetails() {
		include('controllers/pushdetails.php');
	}
	
	public function pageServicePull() {
		include('controllers/servicepull.php');
	}
	
	
	/* Helper Functions */
	
	/*
	 * Check the login status of the current user
	 * Forward user to homepage if not logged in
	 */
	private function checkLoginStatus() {
		$this->getLoginStatus();
		if (isset($_REQUEST["task"]) && $_REQUEST["task"]=="servicepush" ) {
		} else {
			if ( $this->loginstatus == 0 ) {
				$this->pageNotLoggedIn();
			}
		}
	}
	
	/*
	 * Returns the login status of the current user
	 */
	private function getLoginStatus() {
		$user = JFactory::getUser();
		if ( !$user->guest ) {
			$this->loginstatus = 1;
		} else {
			$this->loginstatus = 0;		
		}
	}

}
