<?php

		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_home").'</h2>';
		$htmlvar .= '<p>';
		$htmlvar .= $this->configs->give("home_intro_text");
		$htmlvar .= '</p>';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
    
		$htmlvar .= '<h3><a href="index.php?option=com_gcworkflowdeploy&task=pushtype">New Push</a></h3>';
		$htmlvar .= '<h3><a href="index.php?option=com_gcworkflowdeploy&task=pushhistory">Push History</a></h3>';
		$htmlvar .= '<h3><a href="index.php?option=com_gcworkflowdeploy&task=pullhistory">Pull History</a></h3>';
