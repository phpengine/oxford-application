<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_push_history").'</h2>';		
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
		
		$htmlvar .= '<div class="gcworkflow-pagination-block">';
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="New Push" class="gcworkflow-button" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="pushstart" />';
		$htmlvar .= '</form>';	
		$htmlvar .= '</div>';
	    
		$htmlvar .= '<div class="gcworkflow-pagination-block">';
	    for ( $i=1; $i<=$pageVars["pagecount"]; $i++ ) {
	    	if ( $pageVars["pagenum"] == $i) {
		    	$htmlvar .= '	<form action="index.php">';
		    	$htmlvar .= '		<input type="hidden" name="option" value="com_gcworkflowdeploy" />';
		    	$htmlvar .= '		<input type="hidden" name="task" value="pushhistory" />';
		    	$htmlvar .= '		<input type="hidden" name="pagenum" value="'.$i.'" />';
		    	$htmlvar .= '		<input type="submit" value="'.$i.'" class="gcworkflow-pagination-button-active" />';
		    	$htmlvar .= '	</form>';
	    	} else {
		    	$htmlvar .= '	<form action="index.php">';
		    	$htmlvar .= '		<input type="hidden" name="option" value="com_gcworkflowdeploy" />';
		    	$htmlvar .= '		<input type="hidden" name="task" value="pushhistory" />';
		    	$htmlvar .= '		<input type="hidden" name="pagenum" value="'.$i.'" />';
		    	$htmlvar .= '		<input type="submit" value="'.$i.'" class="gcworkflow-pagination-button" />';
		    	$htmlvar .= '	</form>';
	    	}
	    }	    
		$htmlvar .= '</div>';
		
		$htmlvar .= '<div class="gcworkflow-content-block">';
		if (count($pageVars["pushes"])>0 ) {
				$htmlvar .= '<table>';
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<th><h3>Time</h3></th>';
				$htmlvar .= '		<th><h3>Profile Title</h3></th>';
				$htmlvar .= '		<th><h3>Profile Description</h3></th>';
				$htmlvar .= '		<th><h3>Server Title</h3></th>';
				$htmlvar .= '		<th><h3>Server Description</h3></th>';
				$htmlvar .= '		<th><h3>Server Website URL</h3></th>';
				$htmlvar .= '		<th><h3>Push Status</h3></th>';
				$htmlvar .= '	</tr>';
			foreach ( $pageVars["pushes"] as $push ) {
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<td><p>'.date("H:i:s d/m/Y", $push["push_time"]).'</p></td>';
				$htmlvar .= '		<td><p>'.$push["push_profile_title"].'</p></td>';
				$htmlvar .= '		<td><p>'.$push["push_profile_description"].'</p></td>';
				$htmlvar .= '		<td><p>'.$push["push_serv_title"].'</p></td>';
				$htmlvar .= '		<td><p>'.$push["push_serv_desc"].'</p></td>';
				$htmlvar .= '		<td><p>'.$push["push_serv_website_url"].'</p></td>';
				$htmlvar .= '		<td><p>'.$push["push_status"].'</p></td>';
				$htmlvar .= '	</tr>';
			}
			$htmlvar .= '</table>';
		} else {
			$htmlvar .= '<h3>No Pushes performed yet.</h3>';		
		}
		$htmlvar .= '</div>';
	    
		$htmlvar .= '<div class="gcworkflow-pagination-block">';
	    for ( $i=1; $i<=$pageVars["pagecount"]; $i++ ) {
	    	if ( $pageVars["pagenum"] == $i) {
		    	$htmlvar .= '	<form action="index.php">';
		    	$htmlvar .= '		<input type="hidden" name="option" value="com_gcworkflowdeploy" />';
		    	$htmlvar .= '		<input type="hidden" name="task" value="pushhistory" />';
		    	$htmlvar .= '		<input type="hidden" name="pagenum" value="'.$i.'" />';
		    	$htmlvar .= '		<input type="submit" value="'.$i.'" class="gcworkflow-pagination-button-active" />';
		    	$htmlvar .= '	</form>';
	    	} else {
		    	$htmlvar .= '	<form action="index.php">';
		    	$htmlvar .= '		<input type="hidden" name="option" value="com_gcworkflowdeploy" />';
		    	$htmlvar .= '		<input type="hidden" name="task" value="pushhistory" />';
		    	$htmlvar .= '		<input type="hidden" name="pagenum" value="'.$i.'" />';
		    	$htmlvar .= '		<input type="submit" value="'.$i.'" class="gcworkflow-pagination-button" />';
		    	$htmlvar .= '	</form>';
	    	}
	    }	    
		$htmlvar .= '</div>';
		
		$htmlvar .= '<div class="gcworkflow-pagination-block">';
		$htmlvar .= '	<form action="index.php">';
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="New Push" class="gcworkflow-button" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="pushstart" />';
		$htmlvar .= '</form>';	
		$htmlvar .= '</div>';
		
		$htmlvar .= '</form>';	