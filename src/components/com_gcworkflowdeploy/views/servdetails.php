<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_server_details").'</h2>';		
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
		
		if (count($pageVars["servers"])>0 ) {
			$htmlvar .= '<table width="100%">';
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<th></th>';
				$htmlvar .= '		<th><h3>Title</h3></th>';
				$htmlvar .= '		<th><h3>Description</h3></th>';
				$htmlvar .= '		<th><h3>Webservice URL</h3></th>';
				$htmlvar .= '		<th><h3>Website URL</h3></th>';
				$htmlvar .= '	</tr>';
			foreach ( $pageVars["servers"] as $server ) {
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<td><input type="radio" name="servid" value="'.$server["id"].'"></input></td>';
				$htmlvar .= '		<td><p>'.$server["serv_title"].'</p></td>';
				$htmlvar .= '		<td><p>'.$server["serv_desc"].'</p></td>';
				$htmlvar .= '		<td><p>'.$server["serv_webservice_url"].'</p></td>';
				$htmlvar .= '		<td><p>'.$server["serv_website_url"].'</p></td>';
				$htmlvar .= '	</tr>';
			}
			$htmlvar .= '</table>';
		}
		
		
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Confirmation Page" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="serverdetails" />
		   </form>';