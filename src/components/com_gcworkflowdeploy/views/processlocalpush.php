<?php

		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_process_local_push").'</h2>';
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>'; } }
	    
		$htmlvar .= '<div class="processingWrap">';
		$htmlvar .= '	<div class="processingTop">';
		$htmlvar .= '		<div class="progressBarLargeWrap">';
		
		if ( $pageVars["result_spacecreation"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Space<br />Created</p>';
			$htmlvar .= '			</div>'; }
        else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Space<br />Creation<br />Broken</p>';
			$htmlvar .= '			</div>'; }

		if ( !is_array($pageVars["result_doLocalFileCopy"]) ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Files<br />Copied<br />Locally</p>';
			$htmlvar .= '			</div>'; }
        else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>File<br />Copy<br />Broken</p>';
			$htmlvar .= '			</div>'; }

		if ( $pageVars["result_doTableDump"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Tables<br />Dumped</p>';
			$htmlvar .= '			</div>'; }
        else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Table<br />Dump<br />Broken</p>';
			$htmlvar .= '			</div>'; }

		if ( $pageVars["result_createArchive"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Archive<br />Created</p>';
			$htmlvar .= '			</div>'; }
        else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Archive<br />Creation<br />Broken</p>';
			$htmlvar .= '			</div>'; }

        if ( !is_array($pageVars["result_doFileMove"]) ) {
            $htmlvar .= '			<div class="progressBarInnerOk" >';
            $htmlvar .= '			 <p>Files<br />Moving<br />OK</p>';
            $htmlvar .= '			</div>'; }
        else {
            $htmlvar .= '			<div class="progressBarInnerDead" >';
            $htmlvar .= '			 <p>File<br />Moving<br />Broken</p>';
            $htmlvar .= '			</div>'; }

		if (isset($pageVars["result_cleanupFiles"])) {
			if ( $pageVars["result_cleanupFiles"] == "OK" ) {
				$htmlvar .= '			<div class="progressBarInnerOk" >';
				$htmlvar .= '			 <p>Files<br />Cleaned</p>';
				$htmlvar .= '			</div>'; }
            else {
				$htmlvar .= '			<div class="progressBarInnerDead" >';
				$htmlvar .= '			 <p>File<br />Deletion<br />Broken</p>';
				$htmlvar .= '			</div>'; } }

		$htmlvar .= '		</div>';
		$htmlvar .= '	</div>';
		$htmlvar .= '	<div class="processingBottom">';
		$htmlvar .= '		<table>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<th><h3>Task</h3></th>';
		$htmlvar .= '				<th><h3>Progress</h3></th>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Creating Space:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_spacecreation"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Local File Copy:</p></td>';
		if (!is_array($pageVars["result_doLocalFileCopy"])) {
			$htmlvar .= '				<td><p>'.$pageVars["result_doLocalFileCopy"].'</p></td>';
		} else {
			$htmlvar .= '				<td>';
			foreach ($pageVars["result_doLocalFileCopy"] as $errorvar) {
				$htmlvar .= '				<p class="errorRow"> Error:'.$errorvar.'</p>';
			}
			$htmlvar .= '				</td>';
		}
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Dumping Tables:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_doTableDump"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Creating Archive:</p></td>';
		if (!is_array($pageVars["result_createArchive"])) {
			$htmlvar .= '				<td><p>'.$pageVars["result_createArchive"].'</p></td>';
		} else {
			foreach ($pageVars["result_createArchive"] as $errorvar) {
				$htmlvar .= '				<td><p class="errorRow"> Error:'.$errorvar.'</p></td>';
			}
		}
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Getting Archive Size:</p></td>';
		if ( is_numeric($pageVars["result_getArchiveSize"]) ) {
			$htmlvar .= '				<td><p>OK - '.$pageVars["result_getArchiveSize"].' Bytes</p></td>';
		} else {
			$htmlvar .= '				<td><p>FAILED - '.$pageVars["result_getArchiveSize"].'</p></td>';
		}
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Moving File:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_doFileMove"].'</p></td>';
		$htmlvar .= '			</tr>';
		
		if (isset($pageVars["result_cleanupFiles"])) {
		
			$htmlvar .= '			<tr>';
			$htmlvar .= '				<td><p>Cleaning Up Files:</p></td>';
			if (is_array($pageVars["result_cleanupFiles"])) {
				$htmlvar .= '				<td>';
				foreach ($pageVars["result_cleanupFiles"] as $error) {
					$htmlvar .= '				<p>'.$error.'</p>';
				}
				$htmlvar .= '				</td>';
			} else {
				$htmlvar .= '				<td><p>'.$pageVars["result_cleanupFiles"].'</p></td>';
			}
			$htmlvar .= '			</tr>';
		
		}

		$htmlvar .= '		</table>';
		$htmlvar .= '	</div>';
		$htmlvar .= '</div>';
		
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Confirm All" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="pushid" id="pushid" value="'.$pageVars["push-id"].'" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="processlocalpush" />
		   </form>';
		