<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_pushstart").'</h2>';		
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
		$htmlvar .= '<h1>Pick a Profile to Push:</h1>';
		
		if (count($pageVars["profiles"])>0 ) {
			$htmlvar .= '<table>';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<th></th>';
			$htmlvar .= '		<th><h3>Profile Title</h3></th>';
			$htmlvar .= '		<th><h3>Description</h3></th>';
			$htmlvar .= '		<th><h3>Unique ID</h3></th>';
			$htmlvar .= '	</tr>';
			foreach ( $pageVars["profiles"] as $profile ) {
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<td><input type="radio" name="profileuniqueid" value="'.$profile["profile_uniqueid"].'" /></td>';
				$htmlvar .= '		<td><p>'.$profile["profile_title"].'</p></td>';
				$htmlvar .= '		<td><p>'.$profile["profile_description"].'</p></td>';
				$htmlvar .= '		<td><p>'.$profile["profile_uniqueid"].'</p></td>';
				$htmlvar .= '	</tr>';
			}
			$htmlvar .= '</table>';		
			$htmlvar .= '
				<p style="text-align:center;">
				 <input type="submit" name="submit" value="Use this Profile" />
				</p>
				<input type="hidden" name="run" id="run" value="1" />
				<input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
				<input type="hidden" name="task" id="task" value="pushstart" />';
			$htmlvar .= '</form>';	
			
		} else {
			$htmlvar .= '<h3>No profiles configured</h3>';		
		}
