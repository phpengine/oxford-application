<?php

		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_verify_deployment_info").'</h2>';	
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>'; } }

        $htmlvar .= '<h3>';
        $htmlvar .= ' Push Details';
        $htmlvar .= '</h3>';
        $htmlvar .= '<table>';
        $htmlvar .= '	<tr>';
        $htmlvar .= '		<th><h3>Name</h3></th>';
        $htmlvar .= '		<th><h3>Value</h3></th>';
        $htmlvar .= '	</tr>';
        $htmlvar .= '	<tr>';
        $htmlvar .= '		<td><p>Push Title</p></td>';
        $htmlvar .= '		<td><p>';
        if (isset($pageVars["push-title"]) ) { $htmlvar .= $pageVars["push-title"];  }
        $htmlvar .= '</p></td>';
        $htmlvar .= '	</tr>';
        $htmlvar .= '	<tr>';
        $htmlvar .= '		<td><p>Push Description</p></td>';
        $htmlvar .= '		<td><p>';
        if (isset($pageVars["push-description"]) ) { $htmlvar .= $pageVars["push-description"]; }
        $htmlvar .= '</p></td>';
        $htmlvar .= '	</tr>';
        $htmlvar .= '</table>';

		$htmlvar .= '<h3>';
		$htmlvar .= ' Profile Details';
		$htmlvar .= '</h3>';
		$htmlvar .= '<table>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<th><h3>Name</h3></th>';
		$htmlvar .= '		<th><h3>Value</h3></th>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Title:</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["profile"]["profile_title"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Description:</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["profile"]["profile_description"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Unique ID:</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["profile"]["profile_uniqueid"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '</table>';

        if ($pageVars["push-type"]=="remote") {
            $htmlvar .= '<h3>';
            $htmlvar .= ' Target Server/Site Details';
            $htmlvar .= '</h3>';
            $htmlvar .= '<table>';
            $htmlvar .= '	<tr>';
            $htmlvar .= '		<th><h3>Name</h3></th>';
            $htmlvar .= '		<th><h3>Value</h3></th>';
            $htmlvar .= '	</tr>';
            $htmlvar .= '	<tr>';
            $htmlvar .= '		<td><p>Title</p></td>';
            $htmlvar .= '		<td><p>'.$pageVars["server"]["serv_title"].'</p></td>';
            $htmlvar .= '	</tr>';
            $htmlvar .= '	<tr>';
            $htmlvar .= '		<td><p>Descsription:</p></td>';
            $htmlvar .= '		<td><p>'.$pageVars["server"]["serv_desc"].'</p></td>';
            $htmlvar .= '	</tr>';
            $htmlvar .= '	<tr>';
            $htmlvar .= '		<td><p>Site URL:</p></td>';
            $htmlvar .= '		<td><p>'.$pageVars["server"]["serv_website_url"].'</p></td>';
            $htmlvar .= '	</tr>';
            $htmlvar .= '	<tr>';
            $htmlvar .= '		<td><p>Webservice URL:</p></td>';
            $htmlvar .= '		<td><p>'.$pageVars["server"]["serv_webservice_url"].'</p></td>';
            $htmlvar .= '	</tr>';
            $htmlvar .= '</table>'; }

        if ($pageVars["push-type"]=="local") {
            $htmlvar .= '<h3>';
            $htmlvar .= ' This will be stored locally with admin configured location settings';
            $htmlvar .= '</h3>'; }

		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Process Push" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="verifypush" />
		   </form>';