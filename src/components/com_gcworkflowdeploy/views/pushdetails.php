<?php

		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_pushdetails").'</h2>';
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>'; } }

        $htmlvar .= '<h3>';
        $htmlvar .= ' Push Details';
        $htmlvar .= '</h3>';
		$htmlvar .= '<table>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<th><h3>Name</h3></th>';
		$htmlvar .= '		<th><h3>Description</h3></th>';
		$htmlvar .= '		<th><h3>Value</h3></th>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Push Title</p></td>';
        $htmlvar .= '		<td><p>The Title of this push instance, an example could be "Editorial Advert Content from';
        $htmlvar .= 'Saturday 4th of April"</p></td>';
		$htmlvar .= '		<td><input type="text" name="push-title" value="';
		if (isset($pageVars["push-title"]) ) { $htmlvar .= $pageVars["push-title"];  }
		$htmlvar .= '" /></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Push Description</p></td>';
        $htmlvar .= '		<td><p>A more detailed description of the exact state you\'re saving. An example could';
        $htmlvar .= 'be something like "Updated Terms and conditions by Legal on 2nd April, and Updated CSS by';
        $htmlvar .= 'Designers on the 1st of April"</p></td>';
		$htmlvar .= '		<td><textarea type="text" name="push-description">';
		if (isset($pageVars["push-description"]) ) { $htmlvar .= $pageVars["push-description"]; }
		$htmlvar .= '</textarea></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '</table>';
		
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Submit" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="pushdetails" />
		   </form>';