<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_delete_install").'</h2>';		
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
	    if ($pageVars["curstage"]==1) {
			$htmlvar .= '<h2>Are you sure you want to delete this?</h2>';
	    } else if ($pageVars["curstage"]==2) {
			$htmlvar .= '<h2>Are you ABSOLUTELY sure you want to delete this?</h2>';
	    } else if ($pageVars["curstage"]==3) {
			$htmlvar .= '<h2>You have just deleted:</h2>';
	    }
		
		$htmlvar .= '<table>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>'.$pageVars["install"]["id"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["install"]["qs_site_name"].'</p></td>';
		$htmlvar .= '		<td><p><a target="blank" href="http://'.$pageVars["install"]["qs_site_url"].'">'.$pageVars["install"]["qs_site_url"].'</a></p></td>';
		$htmlvar .= '		<td><p>'.date("H:i:s d/M/Y", $pageVars["install"]["qs_install_time"]).'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '</table>';
		
		if ($pageVars["curstage"]==3) {
		
			$htmlvar .= '<table>';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<td><p>File Deletion:</p></td>';
			$htmlvar .= '		<td><p>'.$pageVars["result_doFileDelete"].'</p></td>';
			$htmlvar .= '	</tr>';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<td><p>VHost Deletion:</p></td>';
			$htmlvar .= '		<td><p>'.$pageVars["result_doVhostDelete"].'</p></td>';
			$htmlvar .= '	</tr>';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<td><p>Database Deletion:</p></td>';
			$htmlvar .= '		<td><p>'.$pageVars["result_doDataDelete"].'</p></td>';
			$htmlvar .= '	</tr>';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<td><p>Instance Data Deletion:</p></td>';
			$htmlvar .= '		<td><p>'.$pageVars["result_doInstanceDataDelete"].'</p></td>';
			$htmlvar .= '	</tr>';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<td><p>Apache Restart:</p></td>';
			$htmlvar .= '		<td><p>USE LINK BELOW</p></td>';
			$htmlvar .= '	</tr>';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<td colspan="2"><p><a target="_blank" href="/index.php?option='.$this->configs->give("com_name").'&task=apacherestart">First, Click here to restart Server (Or website cant be seen)</a> When you click here, you should get an error message in a new window that says the Connection to the webserver was reset while the page was loading. This is correct.</p></td>';
			$htmlvar .= '	</tr>';
			$htmlvar .= '</table>';
		}
		
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Delete Install" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="instanceid" id="instanceid" value="'.$pageVars["install"]["id"].'" />
		    <input type="hidden" name="stage" id="stage" value="'.$pageVars["newstage"].'" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="deleteinstall" />
		   </form>';