<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_pushtype").'</h2>';		
		$htmlvar .= ' <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
		$htmlvar .= '<h1>Pick a type of Push:</h1>';
		$htmlvar .= '<table>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<th></th>';
		$htmlvar .= '		<th><h3>Push Type</h3></th>';
		$htmlvar .= '		<th><h3>Description</h3></th>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><input type="radio" name="push-type" value="remote" /></td>';
		$htmlvar .= '		<td><p>Remote:</p></td>';
		$htmlvar .= '		<td><p>Perform a feature recording and send it directly to a remote server.</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><input type="radio" name="push-type" value="local" /></td>';
		$htmlvar .= '		<td><p>Local:</p></td>';
		$htmlvar .= '		<td><p>Perform a feature recording and save it on this server.</p></td>';
		$htmlvar .= '	</tr>';			
		$htmlvar .= '</table>';
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Use this Profile" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="pushtype" />';
		$htmlvar .= '</form>';	
