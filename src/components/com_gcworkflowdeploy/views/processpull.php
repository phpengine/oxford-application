<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_process_pull").'</h2>';	
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }

		$htmlvar .= '<div class="processingWrap">';
		$htmlvar .= '	<div class="processingTop">';
		$htmlvar .= '		<div class="progressBarLargeWrap">';
			
		$errors = 0;	
	
		if ( $pageVars["result_fileexists"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>File<br />Exists</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>File<br />Doesn\'t<br />Exist</p>';
			$htmlvar .= '			</div>';		
			$errors = 1;	
		}
	
		if ($errors == 1) {
		} else if ( $errors != 1 && $pageVars["result_doLocalFileCopy"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>File<br />Copied</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>File<br />Copy<br />Broken</p>';
			$htmlvar .= '			</div>';			
			$errors = 1;			
		}
	
		if ($errors == 1) {
		} else if ( $errors != 1 && $pageVars["result_doDbTableInstall"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Tables<br />Installed</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Table<br />Install<br />Broken</p>';
			$htmlvar .= '			</div>';	
			$errors = 1;			
		}
	
		if ($errors == 1) {
		} else if ( $errors != 1 && $pageVars["result_doDbTableRewrite"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Tables<br />Rewritten</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Table<br />Rewrite<br />Broken</p>';
			$htmlvar .= '			</div>';	
			$errors = 1;						
		}
	
		if ($errors == 1) {
		} else if ( $errors != 1 && $pageVars["result_doDbTableDrop"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Temp<br />Table/s<br />Dropped</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Temp<br />Table<br />Drop<br />Broken</p>';
			$htmlvar .= '			</div>';	
			$errors = 1;						
		}
	
		if ($errors == 1) {
		} else if ( $errors != 1 && $pageVars["result_cleanupFiles"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Files<br />Cleaned</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>File<br />Deletion<br />Broken</p>';
			$htmlvar .= '			</div>';	
			$errors = 1;						
		}
		
		$htmlvar .= '		</div>';
		$htmlvar .= '	</div>';
		$htmlvar .= '	<div class="processingBottom">';
		$htmlvar .= '		<table>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<th><h3>Task</h3></th>';
		$htmlvar .= '				<th><h3>Progress</h3></th>';
		$htmlvar .= '			</tr>';
		
		if ($pageVars["backup_first"] == "1") {
			$htmlvar .= '			<tr>';
			$htmlvar .= '				<td><p>Backing Up Profile Files:</p></td>';
			$htmlvar .= '				<td><p>'.$pageVars["result_backup_files"].'</p></td>';
			$htmlvar .= '			</tr>';
			$htmlvar .= '			<tr>';
			$htmlvar .= '				<td><p>Backing Up Affected Tables:</p></td>';
			$htmlvar .= '				<td><p>'.$pageVars["result_backup_tables"].'</p></td>';
			$htmlvar .= '			</tr>';
		}
		
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>File Exists:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_fileexists"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>File Copy:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_doLocalFileCopy"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Creating Temp Tables:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_doDbTableInstall"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Table Rewrite:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_doDbTableRewrite"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Dropping Temp Tables:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_doDbTableDrop"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Cleaning Up Files:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_cleanupFiles"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '		</table>';
		$htmlvar .= '	</div>';
		$htmlvar .= '</div>';
		
		$htmlvar .= '
		   <form action="index.php">
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Return Home" class="gcworkflow-button" />
		    </p>
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="home" />
		   </form>
		   
		   <form action="index.php">
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Pull History" class="gcworkflow-button" />
		    </p>
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="pullhistory" />
		   </form>';
		
		$htmlvar .= '
		   </form>';
		
