<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_pull_history").'</h2>';		
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
		
		$htmlvar .= '<div class="gcworkflow-pagination-block">';
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="New Pull" class="gcworkflow-button" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="pullstart" />';
		$htmlvar .= '</form>';	
		$htmlvar .= '</div>';
	    
		$htmlvar .= '<div class="gcworkflow-pagination-block">';
	    for ( $i=1; $i<=$pageVars["pagecount"]; $i++ ) {
	    	if ( $pageVars["pagenum"] == $i) {
		    	$htmlvar .= '	<form action="index.php">';
		    	$htmlvar .= '		<input type="hidden" name="option" value="com_gcworkflowdeploy" />';
		    	$htmlvar .= '		<input type="hidden" name="task" value="pullhistory" />';
		    	$htmlvar .= '		<input type="hidden" name="pagenum" value="'.$i.'" />';
		    	$htmlvar .= '		<input type="submit" value="'.$i.'" class="gcworkflow-pagination-button-active" />';
		    	$htmlvar .= '	</form>';
	    	} else {
		    	$htmlvar .= '	<form action="index.php">';
		    	$htmlvar .= '		<input type="hidden" name="option" value="com_gcworkflowdeploy" />';
		    	$htmlvar .= '		<input type="hidden" name="task" value="pullhistory" />';
		    	$htmlvar .= '		<input type="hidden" name="pagenum" value="'.$i.'" />';
		    	$htmlvar .= '		<input type="submit" value="'.$i.'" class="gcworkflow-pagination-button" />';
		    	$htmlvar .= '	</form>';
	    	}
	    }	    
		$htmlvar .= '</div>';
		
		$htmlvar .= '<div class="gcworkflow-content-block">';
			if (count($pageVars["pulls"])>0 ) {
			$htmlvar .= '<table>';
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<th><h3>Pull Time</h3></th>';
				$htmlvar .= '		<th><h3>Profile Title</h3></th>';
				$htmlvar .= '		<th><h3>Profile Description</h3></th>';
				$htmlvar .= '		<th><h3>Profile Unique ID</h3></th>';
				$htmlvar .= '		<th><h3>Push Key</h3></th>';
				$htmlvar .= '		<th><h3>Push Time</h3></th>';
				$htmlvar .= '		<th><h3>Action</h3></th>';
				$htmlvar .= '	</tr>';
			foreach ( $pageVars["pulls"] as $pull ) {
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<td><p>'.date('H:i:s d/m/Y',$pull["pull_time"]).'</p></td>';
				$htmlvar .= '		<td><p>'.$pull["pull_profile_title"].'</p></td>';
				$htmlvar .= '		<td><p>'.$pull["pull_profile_description"].'</p></td>';
				$htmlvar .= '		<td><p>'.$pull["pull_profile_uniqueid"].'</p></td>';
				$htmlvar .= '		<td><p>'.$pull["push_key"].'</p></td>';
				$htmlvar .= '		<td><p>'.$pull["push_key_desc"].'</p></td>';
				
				$htmlvar .= '		<td>';
				
				if ($pull["pinstalled"] == false) {
					$htmlvar .= '		<p><a href="index.php?option=com_gcworkflowdeploy&task=profileinstall&pullid='.$pull["id"].'">Install Profile</a></p>';				
				} 
				
				if ($pull["pull_status"] == "WAITING") {
					$htmlvar .= '		<p><a href="index.php?option=com_gcworkflowdeploy&task=processpull&pullid='.$pull["id"].'">Perfom Pull</a></p>';				
				} else if ($pull["pull_status"] == "COMPLETE") {
					$htmlvar .= '		<p>Pull is Completed</p>';				
				} else {
					$htmlvar .= '		<p>Status Error</p>';				
				}
				
				$htmlvar .= '		</td>';
				
				$htmlvar .= '	</tr>';
			}
			$htmlvar .= '</table>';
		} else {
			$htmlvar .= '<h3>No Pushes have been made to this server.</h3>';		
		}
		$htmlvar .= '</div>';
	    
		$htmlvar .= '<div class="gcworkflow-pagination-block">';
	    for ( $i=1; $i<=$pageVars["pagecount"]; $i++ ) {
	    	if ( $pageVars["pagenum"] == $i) {
		    	$htmlvar .= '	<form action="index.php">';
		    	$htmlvar .= '		<input type="hidden" name="option" value="com_gcworkflowdeploy" />';
		    	$htmlvar .= '		<input type="hidden" name="task" value="pushhistory" />';
		    	$htmlvar .= '		<input type="hidden" name="pagenum" value="'.$i.'" />';
		    	$htmlvar .= '		<input type="submit" value="'.$i.'" class="gcworkflow-pagination-button-active" />';
		    	$htmlvar .= '	</form>';
	    	} else {
		    	$htmlvar .= '	<form action="index.php">';
		    	$htmlvar .= '		<input type="hidden" name="option" value="com_gcworkflowdeploy" />';
		    	$htmlvar .= '		<input type="hidden" name="task" value="pushhistory" />';
		    	$htmlvar .= '		<input type="hidden" name="pagenum" value="'.$i.'" />';
		    	$htmlvar .= '		<input type="submit" value="'.$i.'" class="gcworkflow-pagination-button" />';
		    	$htmlvar .= '	</form>';
	    	}
	    }	    
		$htmlvar .= '</div>';
		
		$htmlvar .= '<div class="gcworkflow-pagination-block">';
		$htmlvar .= '	<form action="index.php">';
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="New Pull" class="gcworkflow-button" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="pullstart" />';
		$htmlvar .= '</form>';	
		$htmlvar .= '</div>';

		$htmlvar .= '</form>';	