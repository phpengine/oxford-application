<?php

		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_process_remote_push").'</h2>';
		$htmlvar .= '
		   <form action="index.php" method="GET">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
		$htmlvar .= '<div class="processingWrap">';
		$htmlvar .= '	<div class="processingTop">';
		$htmlvar .= '		<div class="progressBarLargeWrap">';
		if ( substr(strval($pageVars["result_authenticate1"]), 0, 2) == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Authentication<br />1 OK</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Authentication<br />1 Broken</p>';
			$htmlvar .= '			</div>';
		}
	
		
		if ( $pageVars["result_spacecreation"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Space<br />Created</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Space<br />Creation<br />Broken</p>';
			$htmlvar .= '			</div>';
		}
	
		
		if ( !is_array($pageVars["result_doLocalFileCopy"]) ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Files<br />Copied<br />Locally</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>File<br />Copy<br />Broken</p>';
			$htmlvar .= '			</div>';
		}
	
		
		if ( $pageVars["result_doTableDump"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Tables<br />Dumped</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Table<br />Dump<br />Broken</p>';
			$htmlvar .= '			</div>';	
		}
	
		
		if ( $pageVars["result_createArchive"] == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Archive<br />Created</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Archive<br />Creation<br />Broken</p>';
			$htmlvar .= '			</div>';	
		}	
		
		if ( substr(strval($pageVars["result_authenticate2"]), 0, 2) == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Archive<br />Size<br />OK</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Archive<br />Size<br />Broken</p>';
			$htmlvar .= '			</div>';	
		}
	
		
		if ( substr(strval($pageVars["result_authenticate2"]), 0, 2) == "OK" ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Authenticaion<br />2 OK</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Authentication<br />2 Broken</p>';
			$htmlvar .= '			</div>';
		}
	
		
		if ( !is_array($pageVars["result_doFileSend"]) ) {
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Files<br />Sending<br />OK</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>File<br />Sending<br />Broken</p>';
			$htmlvar .= '			</div>';
		}
		
		
		if ( substr(strval($pageVars["result_getRemoteArchiveSize"]), 0, 2) == "OK" ) {		
			$htmlvar .= '			<div class="progressBarInnerOk" >';
			$htmlvar .= '			 <p>Remote<br />Archive<br />Size OK</p>';
			$htmlvar .= '			</div>';
		} else {
			$htmlvar .= '			<div class="progressBarInnerDead" >';
			$htmlvar .= '			 <p>Remote<br />Archive<br />Size<br />Broken</p>';
			$htmlvar .= '			</div>';
		}
		
		
		if (isset($pageVars["result_cleanupFiles"])) {	
		
			if ( $pageVars["result_cleanupFiles"] == "OK" ) {
				$htmlvar .= '			<div class="progressBarInnerOk" >';
				$htmlvar .= '			 <p>Files<br />Cleaned</p>';
				$htmlvar .= '			</div>';
			} else {
				$htmlvar .= '			<div class="progressBarInnerDead" >';
				$htmlvar .= '			 <p>File<br />Deletion<br />Broken</p>';
				$htmlvar .= '			</div>';	
			}
		
		}

		$htmlvar .= '		</div>';
		$htmlvar .= '	</div>';
		$htmlvar .= '	<div class="processingBottom">';
		$htmlvar .= '		<table>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<th><h3>Task</h3></th>';
		$htmlvar .= '				<th><h3>Progress</h3></th>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>First Authentication:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_authenticate1"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Creating Space:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_spacecreation"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Local File Copy:</p></td>';
		if (!is_array($pageVars["result_doLocalFileCopy"])) {
			$htmlvar .= '				<td><p>'.$pageVars["result_doLocalFileCopy"].'</p></td>';
		} else {
			$htmlvar .= '				<td>';
			foreach ($pageVars["result_doLocalFileCopy"] as $errorvar) {
				$htmlvar .= '				<p class="errorRow"> Error:'.$errorvar.'</p>';
			}
			$htmlvar .= '				</td>';
		}
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Dumping Tables:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_doTableDump"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Creating Archive:</p></td>';
		if (!is_array($pageVars["result_createArchive"])) {
			$htmlvar .= '				<td><p>'.$pageVars["result_createArchive"].'</p></td>';
		} else {
			foreach ($pageVars["result_createArchive"] as $errorvar) {
				$htmlvar .= '				<td><p class="errorRow"> Error:'.$errorvar.'</p></td>';
			}
		}
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Getting Archive Size:</p></td>';
		if ( is_numeric($pageVars["result_getArchiveSize"]) ) {
			$htmlvar .= '				<td><p>OK - '.$pageVars["result_getArchiveSize"].' Bytes</p></td>';
		} else {
			$htmlvar .= '				<td><p>FAILED - '.$pageVars["result_getArchiveSize"].'</p></td>';
		}
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Second Authentication:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_authenticate2"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Sending File:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_doFileSend"].'</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td><p>Geting Remote Archive Size:</p></td>';
		$htmlvar .= '				<td><p>'.$pageVars["result_getRemoteArchiveSize"].'</p></td>';
		$htmlvar .= '			</tr>';
		
		if (isset($pageVars["result_cleanupFiles"])) {
		
			$htmlvar .= '			<tr>';
			$htmlvar .= '				<td><p>Cleaning Up Files:</p></td>';
			if (is_array($pageVars["result_cleanupFiles"])) {
				$htmlvar .= '				<td>';
				foreach ($pageVars["result_cleanupFiles"] as $error) {
					$htmlvar .= '				<p>'.$error.'</p>';
				}
				$htmlvar .= '				</td>';
			} else {
				$htmlvar .= '				<td><p>'.$pageVars["result_cleanupFiles"].'</p></td>';
			}
			$htmlvar .= '			</tr>';
		
		}
		
		$htmlvar .= '			<tr>';
		$htmlvar .= '				<td colspan="2"><p><a target="_blank" href="'.$pageVars["target_url"].'">';
		$htmlvar .= '				If all ok, Click here to See New Site.</a> Your changes will only be immediately visible if ';
		$htmlvar .= '				the target site is set to passively allow changes, or this key has passive push privileges.';
		$htmlvar .= '				</p></td>';
		$htmlvar .= '			</tr>';
		$htmlvar .= '		</table>';
		$htmlvar .= '	</div>';
		$htmlvar .= '</div>';
		
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Confirm All" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="pushid" id="pushid" value="'.$pageVars["push-id"].'" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="processremotepush" />
		   </form>';
		