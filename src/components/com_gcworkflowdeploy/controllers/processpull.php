<?php
		if ($this->loginstatus == 1) {
		    //initialize page
			if (isset($_REQUEST["run"]) ) {
					unset($_REQUEST["run"]);
				// If its set to run do the validation				
				// NO VALIDATION HERE
				if (count($this->messages)>0 ){
					$this->pageProcessPull();
				} else {
					$this->pageConfirmDeployment();
				}
			} else {
			    //initialize page
			    $content;
			    $content = array();
			    if ( count($this->messages)>0 ) {
			    	$content["messages"] = $this->messages ;
			    }
				require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."pullClass.php");
				$content["pullid"] = $_REQUEST['pullid'] ;
				$pulldetails = $this->model->getSinglePullDetails($content["pullid"]);
				$pull = new GCWorkflowDeployerPullClass( $content["pullid"] ) ;
				
				$content["result_fileexists"] = $pull->checkFileExists();
				$pull->statusUpdate("FILEEXIST");
				$content["result_doLocalFileCopy"] = $pull->doLocalFileCopy();
				$pull->statusUpdate("LOCALCOPY");
				$content["result_doDbTableInstall"] = $pull->doTempDbTableInstall();
				$pull->statusUpdate("DBTBLINSTALL");
				$content["result_doDbTableRewrite"] = $pull->doNewDbTableRewrite();
				$pull->statusUpdate("DBTBLREWRITE");
				$content["result_doDbTableDrop"] = $pull->doTempDbTableDrop();
				$pull->statusUpdate("DBTBLDROP");
			
				if ( $this->configs->give("pull_backup_first")=="1" ) {
					$content["backup_first"] = 1;
				} else {
					$content["backup_first"] = 0;				
				}
				
				if ( $this->configs->give("auto_pull_file_cleanup")=="1" ) {
					$content["result_cleanupFiles"] = $pull->cleanupFiles(); //done
					$pull->statusUpdate("CLEANUPFILES");
				}
				$pull->statusUpdate("COMPLETE");
			    $this->view->pageProcessPull($content);
			}
		}
