<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

$pushservice = new GCWorkflowDeployerServicePushClass();
$pushservice->executeme();

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'modelClass.php';

class GCWorkflowDeployerServicePullClass {
	
	private static $config ;
	private $model ;
	private $basedir ;
	private $uploaddir ;
	private $fname ;
	private $key ;

	public function __construct(){
		$this->key = $_REQUEST["key"];
		$this->actionreq = $_REQUEST["action"];
		$this->configs = new GCWorkflowDeployerConfigClass();
		$this->model = new GCWorkflowDeployerModelClass();
		if ($this->actionreq == "send") {
			$this->uploaddir = $this->configs->give("temp_pull_folder");
			if (substr($this->uploaddir, "-1", "1") != "/") {
				$this->uploaddir .= DS;
			}
		}
		if ($this->actionreq == "fsize") {
			$this->uploaddir = $this->configs->give("temp_pull_folder");
			if (substr($this->uploaddir, "-1", "1") != "/") {
				$this->uploaddir .= DS;
			}
			$this->fname = $_REQUEST["fname"];
		}
	}
	
	public function executeme() {
		if ($this->actionreq == "auth") {
			$authres = $this->authorise();
			$this->display($authres);			
		} else if ($this->actionreq == "send") {
			$fileres = $this->receive();
			$this->display($fileres);		
		} else if ($this->actionreq == "fsize") {
			$filesize = $this->filesize();
			$this->display($filesize);		
		}
	}
	
	/*
	 * Here is the Authoriser
	 */
	private function authorise(){
		if ($this->model->isKey($this->key) ) {
			return "OK";
		} else {
			echo $this->key;
			return false;
		}
	}
	
	/*
	 * Here is the Receiver
	 */
	private function receive(){
		// do a key auth check when running any function
		if ($this->model->isKey($this->key) ) {
			$uploadfolder = $this->uploaddir . substr($_FILES['arch']['name'], 0, (strlen($_FILES['arch']['name'])-4) ).DS;
			mkdir( $uploadfolder );
			$uniqueid = substr($_FILES['arch']['name'], 0, 16);
			$uploadfile = $uploadfolder . $_FILES["arch"]["name"];
			$pushtime = intval( substr($_FILES['arch']['name'], 17, (strlen($_FILES['arch']['name'])-4) ) );
			if (move_uploaded_file($_FILES['arch']['tmp_name'], $uploadfile)) {
				require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'pullClass.php';
				$pullid = GCWorkflowDeployerPullClass::createNewPull();
				$pull = new GCWorkflowDeployerPullClass($pullid); 
				$keyDetails = $this->model->getSingleKeyDetailsByKey($this->key);
				$this->model->setUpdatePull($pullid, "push_key", $this->key);
				$this->model->setUpdatePull($pullid, "push_key_desc", $keyDetails["key_desc"]);
				$this->model->setUpdatePull($pullid, "pull_profile_uniqueid", $uniqueid );
				$this->model->setUpdatePull($pullid, "pull_time", time() );
				$this->model->setUpdatePull($pullid, "push_time", $pushtime );
				$this->model->setUpdatePull($pullid, "pull_status", "WAITING" );
				$pull->extractZip( $uploadfile, $uploadfolder );
				$pullprofiledetails = $pull->getProfileDetails($uploadfolder);
				var_dump($pullprofiledetails);
				$this->model->setUpdatePull($pullid, "pull_profile_title", $pullprofiledetails["push_profile_title"] );
				$this->model->setUpdatePull($pullid, "pull_profile_description", $pullprofiledetails["push_profile_description"] );
				return $_FILES["arch"]["name"];
			} else {
			    return "NOT OK";
			}
		} else {
			return false;
		}
	}
	
	/*
	 * Here is the File Size Getter
	 */
	private function filesize(){
		// do a key auth check when running any function
		if ($this->model->isKey($this->key) ) {
			// for some reason its getting </result in it so remove that
			if (strstr($_REQUEST["fname"], '</result')) {
				$_REQUEST["fname"] = substr($_REQUEST["fname"], 0, (strlen($_REQUEST["fname"])-8) ); 
			}
			$fname = $_REQUEST["fname"];
			if ($filesize = filesize($this->uploaddir.$_REQUEST["fname"])) {
				//echo $filesize;
				return $filesize;
			} else {
				return "NOTOK";
			}
		} else {
			return false;
		}
	}

	/*
	 * Display Function
	 */
	public function display($content) {
		$htmlvar  = '<result>';
		$htmlvar .= $content;
		$htmlvar .= '</result>';
		echo $htmlvar;
	}
	
}
