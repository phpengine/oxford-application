<?php
	if ($this->loginstatus == 1) {
		if (isset($_REQUEST["run"]) ) {
			unset($_REQUEST["run"]);
			// If its set to run do the validation				
			if ( !isset($_REQUEST["deploy-profileid"]) ) {
			    $content["profiles"] = $this->model->getAllProfiles();
		        $this->view->pagePushStart($content);
			} else {
				$this->session->set( "deploy-type", "push" );
				$this->session->set( "deploy-profileid", $_REQUEST["deploy-profileid"] );
				$this->pageServerDetails();
			}
		} else {
		    //initialize page
		    $content;
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ;
		    }
		    if ( isset($_REQUEST["pagenum"]) && is_numeric($_REQUEST["pagenum"]) && $_REQUEST["pagenum"]>1 ) {
		    	$content["pagenum"] = $_REQUEST["pagenum"];
		    } else {
		    	$content["pagenum"] = 1;			    
		    }
		    $content["pushes"] = $this->model->getPushHistoryPage( $content["pagenum"] );
		    $content["pagecount"] = ceil( count($this->model->getAllPushHistory() ) / $this->configs->give("items_per_page") );		    
	        $this->view->pagePushHistory($content);
		}
	}