<?php
		if ($this->loginstatus == 1) {
		    //initialize page
			if (isset($_REQUEST["run"]) ) {
					unset($_REQUEST["run"]);
				// If its set to run do the validation				
				// NO VALIDATION HERE
				if (count($this->messages)>0 ){
					$this->pageProcessLocalPush();
				} else {
					$this->messages[] = "Push complete";
					$this->pageHome();
				}
			} else {
			    //initialize page
			    $content;
			    $content = array();
			    if ( count($this->messages)>0 ) {
			    	$content["messages"] = $this->messages ;  }
			    
				require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."pushClass.php");
				$content["push-id"] = $this->session->get('push-id') ;
				
				$pushdetails = $this->model->getSinglePushDetails($content["push-id"]);
				// $this->session->set('push-profileuniqueid', $pushdetails[])
				$content["target_url"] = $pushdetails["push_serv_website_url"];
				
				$push = new GCWorkflowDeployerPushClass( $content["push-id"] ) ;
				// echo "pushid".$this->session->get('push-id');
				$content["result_spacecreation"] = $push->doSpaceCreate();
				$push->statusUpdate("SPACECREATE");
				$content["result_doLocalFileCopy"] = $push->doLocalFileCopy();
				$push->statusUpdate("LOCALCOPY");
				$content["result_doTableDump"] = $push->doTableDump();
				$push->statusUpdate("TABLEDUMP");
				$content["result_createArchive"] = $push->createArchive();
				$push->statusUpdate("ZIPPED");
				$content["result_getArchiveSize"] = $push->getArchiveSize();
				$push->statusUpdate("LOCALZIPSIZE");
				$content["result_doFileMove"] = $push->doFileMove();
				$push->statusUpdate("FILEMOVED");
				
				$content["result_getRemoteArchiveSize"] = $push->getRemoteArchiveSize($content["result_doFileSend"]);
				// fsize should return the id and filesize of the last file pushed by this key
				if ( is_int($content["result_getRemoteArchiveSize"]) ) {
					$content["result_getRemoteArchiveSize"] = '	OK - '.$content["result_getRemoteArchiveSize"].' Bytes';
				} else {
					$content["result_getRemoteArchiveSize"] = '	FAILED - '.$content["result_getRemoteArchiveSize"];
				}
				
				$push->statusUpdate("REMOTEFILESIZE");
				if ( $this->configs->give("auto_push_file_cleanup")=="1" ) {
					$content["result_cleanupFiles"] = $push->cleanupFiles(); //done
					$push->statusUpdate("CLEANUPFILES");
				}
				$push->statusUpdate("COMPLETE");
			    $this->view->pageProcessLocalPush($content);
			}
		}