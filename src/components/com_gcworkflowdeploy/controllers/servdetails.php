<?php
		if ($this->loginstatus == 1) {
		    //initialize page
			if (isset($_REQUEST["run"]) ) {
				unset($_REQUEST["run"]);
				// If its set to run do the validation				
				if ( !isset($_REQUEST["servid"]) ) {
					$this->messages[] = "You need to Choose a Server"; }
				if (count($this->messages)>0 ){
					$this->pageServerDetails(); }
                else {
					$this->session->set( 'push-servid', $_REQUEST["servid"] );
					$this->pagePushDetails(); } }
            else {
			    //initialize page
			    $content = array();
			    if ( count($this->messages)>0 ) {
			    	$content["messages"] = $this->messages ; }
			    $content["servers"] = $this->model->getAllServersDetails();
		        $this->view->pageServerDetails($content);
			}
		}