<?php
		if ($this->loginstatus == 1) {
			if (isset($_REQUEST["run"]) ) {
				unset($_REQUEST["run"]);
				if ( !isset($_REQUEST["deploy-profileid"]) ) {
          $content["profiles"] = $this->model->getAllProfiles();
          $content["groups"] = $this->model->getAllGroups();
			    $this->view->pagePullStart($content); }
        else {
					$this->session->set( "deploy-type", "pull" );
					$this->session->set( "deploy-profileid", $_REQUEST["deploy-profileid"] );
					$this->pageServerDetails(); }
			} else {
			    $content = array();
			    if ( count($this->messages)>0 ) {
			    	$content["messages"] = $this->messages ; }
			      $content["profiles"] = $this->model->getAllProfiles();
            $content["groups"] = $this->model->getAllGroups();
		        $this->view->pagePullStart($content);
			}
		} else {
    }