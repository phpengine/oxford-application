<?php
	if ($this->loginstatus == 1) {
		if (isset($_REQUEST["run"]) ) {
			unset($_REQUEST["run"]);
			if ( !isset($_REQUEST["profileuniqueid"]) ) {
		        $this->messages[] = "No Profile Selected";
		        $this->pagePushStart(); }
            else {
				$this->session->set( "push-profileuniqueid", $_REQUEST["profileuniqueid"] );
                if ($this->session->get("push-type")=="remote" ) {
                    $this->pageServerDetails(); }
                else if ($this->session->get("push-type")=="local" )  {
                    $this->pagePushDetails(); }
                else {
                    $this->messages[] = "No Push Type Selected";
                    $this->pagePushType();} } }
        else {
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ; }
		    $content["profiles"] = $this->model->getAllProfiles();
	        $this->view->pagePushStart($content); } }