<?php
	if ($this->loginstatus == 1) {
		if (isset($_REQUEST["run"]) ) {
			unset($_REQUEST["run"]);
			// If its set to run do the validation				
			if ( !isset($_REQUEST["push-type"]) ) {
		        $this->messages[] = "No Push Type Selected";
		        $this->pagePushType(); }
            else {
				$this->session->set( "push-type", $_REQUEST["push-type"] );
				if ($_REQUEST["push-type"]=="remote") {
					$this->pageServerDetails(); }
				else if ($_REQUEST["push-type"]=="local") {
					$this->pagePushStart(); } } }
        else {
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ; }
	        $this->view->pagePushType($content);
		}
	}
