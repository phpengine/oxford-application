<?php
    if ($this->loginstatus == 1) {
        if (isset($_REQUEST["run"]) ) {
            unset($_REQUEST["run"]);
            if ( !isset($_REQUEST["push-title"]) || strlen($_REQUEST["push-title"])==0 ) {
                $this->messages[] = "You need to enter a Push Title"; }
            if ( !isset($_REQUEST["push-description"]) || strlen($_REQUEST["push-description"])==0 ) {
                $this->messages[] = "You need to enter a Push Description"; }
            if (count($this->messages)>0 ) {
                $this->pagePushDetails(); }
            else {
                $this->session->set('push-title', $_REQUEST["push-title"]);
                $this->session->set('push-description', $_REQUEST["push-description"]);
                $this->pageVerifyPush(); } }
        else {
            $content = array();
            if ( count($this->messages)>0 ) {
                $content["messages"] = $this->messages ; }
            $content["push-title"] = $this->session->get('push-title');
            $content["push-description"] = $this->session->get("push-description");
            $this->view->pagePushDetails($content); }
    }