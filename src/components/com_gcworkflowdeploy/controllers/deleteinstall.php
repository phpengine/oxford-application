<?php
		if ($this->loginstatus == 1) {
			if (isset($_REQUEST["run"]) ) {
				unset($_REQUEST["run"]);
				// If its set to run do the validation
				// Check the stage
				if($_REQUEST["stage"]==1) {		    
				    $content;
				    $content = array();
				    if ( count($this->messages)>0 ) {
				    	$content["messages"] = $this->messages ;
				    }
				    $content["qsinstance"] = $_REQUEST["instanceid"];
				    $content["curstage"] = $_REQUEST["stage"];
				    $content["newstage"] = $_REQUEST["stage"] + 1;
				    $content["install"] = $this->model->getSingleInstallInstance($content["qsinstance"]);
		        	$this->view->pageDeleteInstall($content);
				} elseif($_REQUEST["stage"]==2) {		    
				    $content;
				    $content = array();
				    if ( count($this->messages)>0 ) {
				    	$content["messages"] = $this->messages ;
				    }
				    $content["qsinstance"] = $_REQUEST["instanceid"];
				    $content["curstage"] = $_REQUEST["stage"];
				    $content["newstage"] = $_REQUEST["stage"] + 1;
				    $content["install"] = $this->model->getSingleInstallInstance($content["qsinstance"]);
		        	$this->view->pageDeleteInstall($content);
				}  elseif($_REQUEST["stage"]==3) {		    
				    $content;
				    $content = array();
					require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."unDeployClass.php");
				    $content["qsinstance"] = $_REQUEST["instanceid"];
				    $content["install"] = $this->model->getSingleInstallInstance($content["qsinstance"]);
				    $content["curstage"] = $_REQUEST["stage"];
				    $content["newstage"] = $_REQUEST["stage"] + 1;
					$unDeploy = new GCQuickstartDeployerUnDeployClass() ;
				    $content["result_doFileDelete"] = $unDeploy->doFileDelete($content["qsinstance"]);
				    $content["result_doVhostDelete"] = $unDeploy->doVhostDelete($content["qsinstance"]);
				    $content["result_doDataDelete"] = $unDeploy->doDataDelete($content["qsinstance"]);
				    $content["result_doInstanceDataDelete"] = $unDeploy->doInstanceDataDelete($content["qsinstance"]);
				    if ( count($this->messages)>0 ) {
				    	$content["messages"] = $this->messages ;
				    }
		        	$this->view->pageDeleteInstall($content);
				}  elseif($_REQUEST["stage"]==4) {	
					$this->pageHome();
				} else { // no stage page accessed in error
					$this->messages[] = "Delete Page accessed in error";
					$this->pageHome();
				}
			} else {
			    //initialize page
			    $content;
			    $content = array();
			    if ( count($this->messages)>0 ) {
			    	$content["messages"] = $this->messages ;
			    }
			    $content["qsinstance"] = $_REQUEST["instanceid"];
			    $content["install"] = $this->model->getSingleInstallInstance($content["qsinstance"]);
		        $this->view->pageDeleteInstall($content);
			}
		}