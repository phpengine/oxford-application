<?php
		if ($this->loginstatus == 1) {
			if (isset($_REQUEST["run"]) ) {
				unset($_REQUEST["run"]);
				// If its set to run do the validation				
				if ( !isset($_REQUEST["deploy-profileid"]) ) {
				    $content["profiles"] = $this->model->getAllProfiles();
			        $this->view->pagePullStart($content); }
                else {
					$this->session->set( "deploy-type", "pull" );
					$this->session->set( "deploy-profileid", $_REQUEST["deploy-profileid"] );
					$this->pageServerDetails(); } }
            else {
			    $content = array();
			    if ( count($this->messages)>0 ) {
			    	$content["messages"] = $this->messages ; }
			    if ( isset($_REQUEST["pagenum"]) && is_numeric($_REQUEST["pagenum"]) && $_REQUEST["pagenum"]>1 ) {
			    	$content["pagenum"] = $_REQUEST["pagenum"]; }
                else {
			    	$content["pagenum"] = 1;	 }
			    $content["pulls"] = $this->model->getPullHistoryPage( $content["pagenum"] );
			    $content["pagecount"] =  ceil(count($this->model->getAllPullHistory() )  / $this->configs->give("items_per_page") );
			    $ipull = 0;
			    foreach ($content["pulls"] as $pull) {
			    	$content["pulls"][$ipull]["pinstalled"] = $this->model->checkProfileExists($content["pulls"][$ipull]["pull_profile_uniqueid"]);
			    	$ipull++; }
		        $this->view->pagePullHistory($content); } }