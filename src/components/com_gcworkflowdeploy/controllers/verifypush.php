<?php
	if ($this->loginstatus == 1) {
		if (isset($_REQUEST["run"]) ) {
			unset($_REQUEST["run"]);
			if (count($this->messages)>0 ) {
				unset($_REQUEST["run"]);
				$this->pageVerfiyPush(); }
            else {
				unset($_REQUEST["run"]);
				if ( !$this->session->get('push-profileuniqueid') ) {
					$this->messages[] = "Session Expired";
					$this->pagePushType(); }
                else {
                    $pushType = $this->session->get("push-type");
                    if ($pushType=="remote") {
                        $this->session->set('push-id', $this->model->setNewPushID() ) ;
                        $profile = $this->model->getSingleProfileDetailsFromUnique( $this->session->get('push-profileuniqueid') );
                        $server = $this->model->getSingleServerDetails( $this->session->get('push-servid') );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_profile_title", $profile["profile_title"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_profile_uniqueid", $profile["profile_uniqueid"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_profile_description", $profile["profile_description"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_time", time() );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_serv_id", $server["id"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_serv_title", $server["serv_title"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_serv_desc", $server["serv_desc"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_serv_website_url", $server["serv_website_url"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_serv_webservice_url", $server["serv_webservice_url"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_serv_upload_type", $server["serv_upload_type"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_status", "START" );
                        $this->pageProcessRemotePush(); }
                    else if ($pushType=="local") {
                        $this->session->set('push-id', $this->model->setNewPushID() ) ;
                        $profile = $this->model->getSingleProfileDetailsFromUnique( $this->session->get('push-profileuniqueid') );
                        $server = $this->model->getSingleServerDetails( $this->session->get('push-servid') );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_profile_title", $profile["profile_title"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_profile_uniqueid", $profile["profile_uniqueid"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_profile_description", $profile["profile_description"] );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_time", time() );
                        $this->model->setUpdatePush($this->session->get('push-id'), "push_status", "START" );
                        $this->pageProcessLocalPush(); }
                    else {
                        $this->messages[] = "No Push Type Set";
                        $this->pagePushType(); } } } }
        else {
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ; }
		    $content["profile"] = $this->model->getSingleProfileDetailsFromUnique( $this->session->get('push-profileuniqueid') );
            $content["push-type"] = $this->session->get("push-type");
            if ($content["push-type"]=="remote") {
                $content["server"] = $this->model->getSingleServerDetails( $this->session->get('push-servid') ); }
            $content["push-title"] = $this->session->get('push-title');
            $content["push-description"] = $this->session->get("push-description");
		    $this->view->pageVerifyPush($content); } }