<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';

class GCWorkflowDeployerFEViewClass {
	
	private static $config;

	public function __construct(){
		$this->configs = new GCWorkflowDeployerConfigClass();
		$this->openPage();
	}
	
	public function pageNotLoggedIn($pageVars) {
		include('views/notloggedin.php');
		echo $htmlvar;
	}

	public function pageHome($pageVars) {
		include('views/home.php');
		echo $htmlvar;	
	}

	public function pagePullHistory($pageVars) {
		include('views/pullhistory.php');
		echo $htmlvar;	
	}

	public function pagePushHistory($pageVars) {
		include('views/pushhistory.php');
		echo $htmlvar;	
	}

	public function pagePullStart($pageVars) {
		include('views/pullstart.php');
		echo $htmlvar;	
	}

	public function pagePushStart($pageVars) {
		include('views/pushstart.php');
		echo $htmlvar;	
	}

	public function pagePushType($pageVars) {
		include('views/pushtype.php');
		echo $htmlvar;	
	}

	public function pageSiteDetails($pageVars) {
		include('views/pushdetails.php');
		echo $htmlvar;	
	}
	
	public function pageServerDetails($pageVars) {
		include('views/servdetails.php');
		echo $htmlvar;	
	}
	
	public function pageVerifyPush($pageVars) {
		include('views/verifypush.php');
		echo $htmlvar;	
	}

    public function pageProcessLocalPush($pageVars) {
        include('views/processlocalpush.php');
        echo $htmlvar;
    }

    public function pageProcessRemotePush($pageVars) {
        include('views/processremotepush.php');
        echo $htmlvar;
    }

    public function pageProcessPull($pageVars) {
		include('views/processpull.php');
		echo $htmlvar;	
	}
	
	public function pagePushDetails($pageVars) {
		include('views/pushdetails.php');
		echo $htmlvar;	
	}
	
	/* HELPERS */

	private function openPage(){
		JHTML::stylesheet('component.css', "components/com_gcworkflowdeploy/css/");
	}

}
