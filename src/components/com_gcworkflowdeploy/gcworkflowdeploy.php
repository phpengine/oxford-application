<?php
/**
 * GCFW Hello World
 * 
 * @package    GC Quickstart Deployer
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer is copyrighted.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );
if(!defined('DS')) { define('DS',DIRECTORY_SEPARATOR); }

// CLASS REQUIRED
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT.DS.'controlClass.php';

ini_set("max_execution_time", "120");

$configs = new GCWorkflowDeployerConfigClass();
$control = new GCWorkflowDeployerFEControlClass();

if ($configs->give("hide_php_errors")=="1") {
	ini_set("display_errors", "0"); }

$task = (isset($_REQUEST["task"])) ? $_REQUEST["task"] : "home" ;
switch ($task) {
  case 'notloggedin': 
    $control->pageNotLoggedIn();
    break;
  case 'home': 
    $control->pageHome();
    break;
  case 'pushstart': 
    $control->pagePushStart();
    break;
  case 'pushtype': 
    $control->pagePushType();
    break;
  case 'pullstart': 
    $control->pagePullStart();
    break;
  case 'pushhistory': 
    $control->pagePushHistory();
    break;
  case 'pullhistory': 
    $control->pagePullHistory();
    break;
  case 'pushdetails':
    $control->pagePushDetails();
    break;
  case 'serverdetails': 
    $control->pageServerDetails();
    break;
  case 'verifypush': 
    $control->pageVerifyPush();
    break;
  case 'processremotepush':
    $control->pageProcessRemotePush();
    break;
  case 'processlocalpush':
    $control->pageProcessLocalPush();
    break;
  case 'processpull': 
    $control->pageProcessPull();
    break;
  case 'servicepull':
    $control->pageServicePull();
    break;
  default:
    $control->pageHome();
    break;
}
