<?php

/************************************************************
 * @version			ticketmaster 2.5.5
 * @package			com_ticketmaster
 * @copyright		Copyright � 2009 - All rights reserved.
 * @license			GNU/GPL
 * @author			Robert Dam
 * @author mail		info@rd-media.org
 * @website			http://www.rd-media.org
 *************************************************************/

defined('_JEXEC') or die ('No Access to this file!');

jimport('joomla.application.component.controller');

class TicketmasterControllerValidate extends JControllerLegacy {

	function __construct() {
		parent::__construct();
		
		$this->ordercode 	= JRequest::getInt('oc', 0);
		$this->id 			= JRequest::getInt('cid', 0);

	}
	
	function validate(){
	
		$mainframe = JFactory::getApplication();
		
		## Getting the POST variables.
		$post = JRequest::get('post');
		
		if ($this->ordercode == 0) { 	
			$msg = JText::_( 'COM_TICKETMASTER_NO_VALID_ID' );
			$mainframe->redirect('index.php?option=com_ticketmaster', $msg );	
		}				

		if ($this->id == 0) { 	
			$msg = JText::_( 'COM_TICKETMASTER_NO_VALID_ID' );
			$mainframe->redirect('index.php?option=com_ticketmaster', $msg );	
		}	
		
		## GETTING THE MODEL TO SAVE
		$model	=& $this->getModel('validate');
		
		## OK, all tickets have been added to the database session.<br />
		## We need to update the table for total-tickets.

		$path_include = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'classes'.DS.'confirmation.php';
		include_once( $path_include );
		
		if(isset($this->ordercode)) {  
		
			$sendconfirmation = new confirmation( (int)$this->ordercode );  
			$sendconfirmation->doConfirm();
			$sendconfirmation->doSend();
		
		}  		
		

		if(!$model->update($this->ordercode, $this->id)) {
			$msg = JText::_( 'COM_TICKETMASTER_VALIDATION_FAILED' );
			$mainframe->redirect('index.php?option=com_ticketmaster', $msg );
		}
		
		$msg = JText::_( 'COM_TICKETMASTER_VALIDATED' );
		$mainframe->redirect('index.php?option=com_ticketmaster', $msg );		
	
	}

}	
?>
