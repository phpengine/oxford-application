<?php
/****************************************************************
 * @version				2.5.5 ticketmaster 						
 * @package				ticketmaster								
 * @copyright           Copyright © 2009 - All rights reserved.			
 * @license				GNU/GPL										
 * @author				Robert Dam									
 * @author mail         info@rd-media.org							
 * @website				http://www.rd-media.org						
 ***************************************************************/

defined('_JEXEC') or die ('No Access to this file!');

jimport('joomla.application.component.controller');

## This Class contains all data for the car manager
class TicketmasterControllerJQuery extends JControllerLegacy {

	function __construct(){
		
		parent::__construct();
		
		$this->id    = JRequest::getInt('id', 0); 		
	}

	## This function will display if there is no choice.
	function getdata() {
		
		$id  = JRequest::getInt('id', 0); 
		
		$db    = JFactory::getDBO();	
		## Making the query for getting the config
		$sql='SELECT  * FROM #__ticketmaster_tickets WHERE ticketid = '.(int)$id.''; 
	 
		$db->setQuery($sql);
		$t = $db->loadObject();
	
		$arr = array('eventcode' => $t->eventcode, 
					 'ticketname' => $t->ticketname, 
					 'location' => $t->location, 
					 'locationinfo' => $t->locationinfo,
					 'starttime' => $t->starttime,
					 'ticketprice' => $t->ticketprice,
					 'totaltickets' => $t->totaltickets,
					 'ticket_fontcolor_r' => $t->ticket_fontcolor_r,
					 'ticket_fontcolor_g' => $t->ticket_fontcolor_g,
					 'ticket_fontcolor_b' => $t->ticket_fontcolor_b,
					 'ticket_fontsize' => $t->ticket_fontsize,
					 'ticketnr_fontcolor_r' => $t->ticketnr_fontcolor_r,
					 'ticketnr_fontcolor_g' => $t->ticketnr_fontcolor_g,
					 'ticketnr_fontcolor_b' => $t->ticketnr_fontcolor_b,
					 'ticketnr_fontsize' => $t->ticketnr_fontsize,
					 'ticketid_nr_fontcolor_r' => $t->ticketid_nr_fontcolor_r,
					 'ticketid_nr_fontcolor_g' => $t->ticketid_nr_fontcolor_g,
					 'ticketid_nr_fontcolor_b' => $t->ticketid_nr_fontcolor_b,
					 'ticketid_nr_fontsize' => $t->ticketid_nr_fontsize,
					 'eventname_position' => $t->eventname_position,
					 'date_position' => $t->date_position,
					 'location_position' => $t->location_position,
					 'orderid_position' => $t->orderid_position,
					 'ordernumber_position' => $t->ordernumber_position,
					 'price_position' => $t->price_position,
					 'bar_position' => $t->bar_position, 
					 'name_position' => $t->name_position, 
					 'free_text_2' => $t->free_text_2, 
					 'end_date' => $t->end_date, 
					 'free_text_1' => $t->free_text_1, 
					 'min_ordering' => $t->min_ordering, 
					 'max_ordering' => $t->max_ordering, 
					 'ticketdate' => $t->ticketdate, 
					 'sale_stop' => $t->sale_stop, 
					 'orderdate_position' => $t->orderdate_position);
		
		echo json_encode($arr);
	
	}
 
	## This function will display if there is no choice.
	function getSampleData() {
			
		$arr = array('eventname_position' => '6-33.3',
					 'date_position' => '6-60',
					 'location_position' => '6-116',
					 'orderid_position' => '6-10',
					 'ordernumber_position' => '6-83.7',
					 'price_position' => '6-67.8',
					 'bar_position' => '155-99', 
					 'name_position' => '6-15', 
					 'free_text2_position' => '8-15',
					 'free_text1_position' => '10-15', 
					 'position_seatnumber' => '12-15', 
					 'orderdate_position' => '6-75.3');
		
		echo json_encode($arr);
	
	} 

	function orderTickets(){
		
		echo 'ja';
		
	}	
	
	function ordering(){
		
		//$db = JFactory::getDBO();
	
		//$data = json_decode($_POST["data"]);
		
		//foreach($data->coords as $item) {
		
			//$path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'assets'.DS.'test.txt';			
			// Open the file to get existing content
			//$current = file_get_contents($path);
			// Append a new person to the file
			//$current .= "test";
			// Write the contents back to the file
			//file_put_contents($path, $current);			
			
			//$db  = JFactory::getDBO();
			//$sql = 'UPDATE #__ticketmaster_tickets SET ordering = '.$item->ordering.' WHERE ticketid = '.$item->ticketid.'';
			//$db->setQuery($sql);
			
			//if (!$db->query() ){
			//	echo "<script>alert('".$row->getError()."');	 
			//}	
			
		//}
		
		echo 'ja';
		
	}
   
}	
?>
