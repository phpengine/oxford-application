<?php
/****************************************************************
 * @version				2.5.5 ticketmaster 						
 * @package				ticketmaster								
 * @copyright           Copyright � 2009 - All rights reserved.			
 * @license				GNU/GPL										
 * @author				Robert Dam									
 * @author mail         info@rd-media.org							
 * @website				http://www.rd-media.org						
 ***************************************************************/

defined('_JEXEC') or die ('No Access to this file!');

jimport('joomla.application.component.controller');

## This Class contains all data for the car manager
class ticketmasterControllerVenues extends JControllerLegacy {

	function __construct() {
		parent::__construct();
		
			## Register Extra tasks
			$this->registerTask( 'add' , 'edit' );
			$this->registerTask('unpublish','publish');
			$this->registerTask('apply','save' );	
	}

	## This function will display if there is no choice.
	function display() {
	
		JRequest::setVar( 'layout', 'default');
		JRequest::setVar( 'view', 'venues');
		parent::display();
	}
   
	function edit() {
	
		JRequest::setVar( 'layout', 'form');
		JRequest::setVar( 'view', 'venues');		
		JRequest::setVar( 'hidemainmenu', 1 );
		parent::display();

	}
	
	function save() {

		$post	            	= JRequest::get('post');
		$post['locdescription'] = JRequest::getVar('locdescription', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		$post['website'] 		= JRequest::getVar('website', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		
		## Getting the street and city for Google maps
		$street = str_replace(" ", "+", JRequest::getVar('street'));
		$city = JRequest::getVar('city');
		
		## Connecting DB
		$db = JFactory::getDBO();	
		## Making the query for getting the config
		$sql='SELECT google_maps_key FROM #__ticketmaster_config WHERE configid = 1'; 
	 	## Setting the Query
		$db->setQuery($sql);
		## Assign the object
		$config = $db->loadObject();
		
		if ($post['own_ll'] == 0) {

			## To check !! Does this work properly enough?
			$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address=573/1,+Jangli+Maharaj+Road,+Deccan+Gymkhana,+Pune,+Maharashtra,+India&sensor=false');
			
			$output= json_decode($geocode);
			
			$lat = $output->results[0]->geometry->location->lat;
			$long = $output->results[0]->geometry->location->lng;		
			
			
			
			## Google_maps_key
			## Three parts to the querystring: q is address, output is the format (
			$key = stripslashes($config->google_maps_key);
			$address = urlencode($street.'+'.$city); 
			$googleurl = "http://maps.google.com/maps/geo?q=".$address."&output=json&key=".$key."&sensor=true";
			
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, $googleurl);
			curl_setopt($ch, CURLOPT_HEADER,0);
			curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			$data = curl_exec($ch);
			curl_close($ch);
			
			$geo_json = json_decode($data, true);
	
			if ($geo_json['Status']['code'] == '200') {
				
				$precision = $geo_json['Placemark'][0]['AddressDetails']['Accuracy'];
				$latitude = $geo_json['Placemark'][0]['Point']['coordinates'][0];
				$longitude = $geo_json['Placemark'][0]['Point']['coordinates'][1];
				
				$post['googlemaps_latitude'] = $latitude;
				$post['googlemaps_longitude'] = $longitude;
			
			} else {
			
				$msg = "Error in geocoding! Http error ".substr($data,0,3);
				$link = 'index.php?option=com_ticketmaster&controller=venues';
				$this->setRedirect($link, $msg);			
			}
		
		}
		
		$model	= $this->getModel('venues');

		if ($model->store($post)) {
			$msg = JText::_( 'COM_TICKETMASTER_VENUE_SAVED' );
		} else {
			$msg = JText::_( 'COM_TICKETMASTER_VENUE_NOT_SAVED' );
		}
		
		$link = 'index.php?option=com_ticketmaster&controller=venues';
		$this->setRedirect($link, $msg);
	}


	function publish()
	{

		$cid = JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		
		## Getting the task (publish/upnpublish)
		if ($this->getTask() == 'publish') {
			$publish = 1;
		} else {
			$publish = 0;
		}		

		if (count( $cid ) < 1) {
			$link = 'index.php?option=com_ticketmaster&controller=venues';
			$this->setRedirect($link, JText::_( 'COM_TICKETMASTER_VENUE_SELECT'));
		}

		$model = $this->getModel('venues');
		if(!$model->publish($cid, $publish)) {
			$link = 'index.php?option=com_ticketmaster&controller=events';
			$this->setRedirect($link, JText::_( 'COM_TICKETMASTER_VENUE_ERROR_CONTROLLER'));
		}
		$link = 'index.php?option=com_ticketmaster&controller=venues';
		$this->setRedirect($link);
	}

	function remove() {
	
		global $option, $mainframe;
		
		$cid = JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$model = $this->getModel('venues');
		
		if(!$model->remove($cid)) {
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}

		$this->setRedirect('index.php?option=com_ticketmaster&controller=venues', JText::_( 'COM_TICKETMASTER_VENUE_DELETED'));
		
	}

   
}	
?>
