<?php 

/****************************************************************
 * @version			2.5.5											
 * @package			ticketmaster									
 * @copyright		Copyright © 2009 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

## Direct access is not allowed.
defined('_JEXEC') or die();

class remover{					

	function cleanup(){
		
		$this->setError = '';
		
		$db = JFactory::getDBO();
		## Making the query for getting the config
		$sql='SELECT  remove_unfinished, removal_hours FROM #__ticketmaster_config WHERE configid = 1'; 
	 
		$db->setQuery($sql);
		$config = $db->loadObject();
		
		if ($config->remove_unfinished != 1) {
			return false;
		}

		## Create a new date NOW()-1h. (database session is not longer than 2 hours in global config.
		$cleanup = date('Y-m-d H:i:s', mktime(date('H')-$config->removal_hours, date('i'), date('s'), date('m'), date('d'), date('Y')));

		## Pickup the items that will be deleted. We need to update the ticketcounter.
		## First getting the selected item in an object.
		## make sure we are also picking up the parent if needed.
		$update = 'SELECT o.ticketid , t.parent AS parentticket, o.orderid, o.seat_sector
				   FROM #__ticketmaster_orders  AS o, #__ticketmaster_tickets AS t
				   WHERE o.orderdate < "'.$cleanup.'"  
				   AND userid = 0
				   AND o.ticketid = t.ticketid';

		$db->setQuery($update);
		$this->data = $db->loadObjectList();
		
		$this->counter = count($this->data);
		
		if ($this->counter < 1){
			$this->setError = 'No unfnished orders in the database.';			
		}		
		
		## Now go on with the delete functions. All expired orders have been saved.
		$query = 'DELETE FROM #__ticketmaster_orders WHERE orderdate < "'.$cleanup.'"  AND userid = 0';
		
		## Do the query now	and delete all selected invoices.
		$db->setQuery( $query );
		
		## When query goes wrong.. Show message with error.
		if (!$db->query()) {
			$this->setError = $db->getErrorMsg();
			return false;
		}	

		## Tickets have been removed successfull
		## Now we need to update the totals from the Object earlier this script.
		$k = 0;
		for ($i = 0, $n = count($this->data); $i < $n; $i++ ){
		
			$row = $this->data[$i];
			
			$query = 'UPDATE #__ticketmaster_tickets'
				  . ' SET totaltickets = totaltickets+1'
				  . ' WHERE ticketid = '.$row->ticketid.' ';
			
			## Do the query now	
			$db->setQuery( $query );
			
			## When query goes wrong.. Show message with error.
			if (!$db->query()) {
				$this->setError = $db->getErrorMsg();
				return false;
			}
			
			## This is for the parent ticket. (there is a parent available)
			## If not, then the query won't have to run as there is no parent.
			if ($row->parentticket != 0){
				## Update the tickets-totals that where removed.
				$query = 'UPDATE #__ticketmaster_tickets'
					. ' SET totaltickets = totaltickets+1'
					. ' WHERE ticketid = '.$row->parentticket.' ';

				## Do the query now	
				$db->setQuery( $query );
				
				## When query goes wrong.. Show message with error.
				if (!$db->query()) {
					$this->setError = $db->getErrorMsg();
					return false;
				}						
			}
			
			if ($row->seat_sector != 0){
			
				$query = 'UPDATE #__ticketmaster_coords 
						  SET booked = 0, orderid = 0 
						  WHERE orderid = '.$row->orderid.' ';

				## Do the query now	
				$db->setQuery( $query );
				
				## When query goes wrong.. Show message with error.
				if (!$db->query()) {
					$this->setError = $db->getErrorMsg();
					return false;
				}						
				
								
			}
				
		$k=1 - $k;
		}
		
		return true;			
	
	}
	
	function counter(){
		return $this->counter;
	}
	
	function error(){
		return $this->setError;
	}	

}