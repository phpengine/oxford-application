<?php
/****************************************************************
 * @version			Ticketmaster Pro 1.0.1							
 * @package			Ticketmaster									
 * @copyright		Copyright © 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

## no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.model' );

class TicketmasterExtModeltickets extends JmodelLegacy
{
	function __construct(){
		parent::__construct();

		$mainframe =& JFactory::getApplication();
		
		$config = JFactory::getConfig();
		
		// Get the pagination request variables
		$limit        = $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		//$limitstart    = $mainframe->getUserStateFromRequest( 'limitstart', 'limitstart', 0, 'int' );
		$limitstart    = JRequest::getInt('limitstart', 0);
		
		// In case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
		
		$array    = JRequest::getVar('cid', array(0), '', 'array');
		$this->id = (int)$array[0]; 		
	}
	
	function getPagination() {
		
		if (empty($this->_pagination)) {
		
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}
	
		return $this->_pagination;
	}
    
    function getTotal() {
	
        if (empty($this->_total)) {

			$where		= $this->_buildContentWhere();
		
			## Making the query for showing all the clients in list function
			$query = 'SELECT a.*, b.eventname, b.groupname
					  FROM #__ticketmaster_tickets AS a, #__ticketmaster_events AS b, #__ticketmaster_venues AS v'
					.$where; 
            $this->_total = $this->_getListCount($query, $this->getState('limitstart'), $this->getState('limit'));
        }

        return $this->_total;
    }  

	function _buildContentWhere() {
	
		$mainframe = JFactory::getApplication();
		
		$db = JFactory::getDBO();
		
		$filter_order = $mainframe->getUserStateFromRequest( 'filter_ordering_t','filter_ordering_t','a.eventid','cmd' );

		$where = array();

		$where[] = 'a.eventid = b.eventid';
		$where[] = 'a.venue = v.id';
		$where[] = 'a.parent = 0';
		
		
		if($filter_order == 0) {
			$where[] = 'a.eventid > 0';
		}else{
			$where[] = 'a.eventid = '.$filter_order;
		}
		
		
		$where 		= ( count( $where ) ? ' WHERE '. implode( ' AND ', $where ) : '' );

		return $where;
	}

   function getList() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
			
			$where = $this->_buildContentWhere();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT a.*, b.eventname, b.groupname, v.venue, v.city, v.id AS venueid
					FROM #__ticketmaster_tickets AS a, #__ticketmaster_events AS b, #__ticketmaster_venues AS v'
					.$where.' ORDER BY b.eventid'; 
		 
		 	$db->setQuery($sql, $this->getState('limitstart'), $this->getState('limit' ));
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
	}

   function getSettings() {
   
		if (empty($this->_data)) {

		 	$db           = JFactory::getDBO();
			$mainframe    = JFactory::getApplication();
			$filter_order = $mainframe->getUserStateFromRequest( 'filter_ordering_t','filter_ordering_t','a.eventid','cmd' );

			if($filter_order == 0) {
				$where = 'AND a.eventid > 0';
			}else{
				$where = 'AND a.eventid = '.$filter_order;
			}
			
			## Making the query for showing all the clients in list function
			$sql = 'SELECT COUNT(id) AS total 
					FROM #__ticketmaster_tickets_ext AS ext, #__ticketmaster_tickets AS a
					WHERE ext.ticketid = a.ticketid '. $where; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObject();
		}
		return $this->data;
	}
	
   function getChilds() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
			
			$where = $this->_buildChildsWhere();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT a.*, b.eventname, b.groupname
					FROM #__ticketmaster_tickets AS a, #__ticketmaster_events AS b'
					.$where.' ORDER BY a.ticketprice ASC'; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
	}	

	function _buildChildsWhere() {
	
		$mainframe = JFactory::getApplication();
		
		$db = JFactory::getDBO();
		
		$filter_order     = $mainframe->getUserStateFromRequest( 'filter_ordering_t','filter_ordering_t','a.eventid','cmd' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( 'filter_order_Dir', 'filter_order_Dir', 'asc', 'word' );

		$where = array();

		$where[] = 'a.eventid = b.eventid';
		$where[] = 'a.parent != 0';
		
		if($filter_order == 0) {
			$where[] = 'a.eventid > 0';
		}else{
			$where[] = 'a.eventid = '.$filter_order;
		}
		
		
		$where 		= ( count( $where ) ? ' WHERE '. implode( ' AND ', $where ) : '' );

		return $where;
	}

   function getPreferences() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
			
			## Making the query for showing all the clients in list function
			$sql = 'SELECT *	
					FROM #__ticketmaster_tickets_ext
					WHERE ticketid ='.(int)$this->id.''; 

		 	$db->setQuery($sql);
		 	$item = $db->loadObject();

			if(!$item->ticketid) {

				$query = "INSERT INTO #__ticketmaster_tickets_ext (id, ticketid, seat_width, seat_heigth) 
							VALUES ( '', ".$this->id.", 14, 14 )";	

				$db->setQuery( $query );
				$db->query();
				
			}

			## Making the query for showing all the clients in list function
			$sql = 'SELECT ext.*, t.ticketname, t.parent	
					FROM #__ticketmaster_tickets_ext AS ext, #__ticketmaster_tickets AS t
					WHERE ext.ticketid ='.(int)$this->id.'
					AND ext.ticketid = t.ticketid';  
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObject();
		}
		return $this->data;
	}

   function getSeats() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT c.*, t.ticketname, tt.*
					FROM #__ticketmaster_coords AS c,  #__ticketmaster_tickets AS t, #__ticketmaster_tickets_ext AS tt
					WHERE c.parent ='.(int)$this->id.'
					AND c.ticketid = t.ticketid
					AND c.ticketid = tt.ticketid'; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
	}
	
	
   function getNochilds() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT c.*, t.ticketname, tt.background_color
					FROM #__ticketmaster_coords AS c,  #__ticketmaster_tickets AS t, #__ticketmaster_tickets_ext AS tt
					WHERE c.ticketid = t.ticketid
					AND c.ticketid = tt.ticketid
					AND c.ticketid = '.(int)$this->id.''; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
	}	
	
   function getTotals() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT c . * , COUNT( c.ticketid ) AS total, COUNT(booked) as totalbooked
					FROM #__ticketmaster_coords AS c, #__ticketmaster_tickets AS t
					WHERE c.ticketid = t.ticketid
					GROUP BY c.ticketid'; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
	}
	
   function getTotalbooked() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT ticketid, COUNT(booked) as totalbooked
					FROM #__ticketmaster_coords
					WHERE booked = 1
					GROUP BY ticketid'; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
	}				
	
   function getData() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT * 
					FROM #__ticketmaster_tickets_ext
					WHERE ticketid ='.(int)$this->id.''; 
		 
		 	$db->setQuery($sql);
		 	$this->data = $db->loadObject();
		}
		return $this->data;
	}		

	function store($data) {
		
		$row =& $this->getTable();

		## Bind the form fields to the web link table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		## Make sure the web link table is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		} 

		## Store the web link table to the database
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}	
				
	return true;
	}

}
?>