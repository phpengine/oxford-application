<?php
/****************************************************************
 * @version			Ticketmaster Pro 1.0.1							
 * @package			Ticketmaster									
 * @copyright		Copyright © 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

## no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.model' );

class TicketmasterExtModelImport extends JmodelLegacy
{
	function __construct(){
		parent::__construct();

		$mainframe =& JFactory::getApplication();
		
		$config = JFactory::getConfig();
		
		## Get the pagination request variables
		$limit        = $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		//$limitstart    = $mainframe->getUserStateFromRequest( 'limitstart', 'limitstart', 0, 'int' );
		$limitstart    = JRequest::getInt('limitstart', 0);
		
		## In case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
		
		$array    = JRequest::getVar('cid', array(0), '', 'array');
		$this->id = (int)$array[0]; 		
	}
	
	function getPagination() {
		
		if (empty($this->_pagination)) {
		
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}
	
		return $this->_pagination;
	}
    
    function getTotal() {
	
        if (empty($this->_total)) {

			$where		= $this->_buildContentWhere();
		
			## Making the query for showing all the clients in list function
			$query = 'SELECT a.*, b.eventname, b.groupname
					  FROM #__ticketmaster_tickets AS a, #__ticketmaster_events AS b, #__ticketmaster_venues AS v'
					.$where; 
            $this->_total = $this->_getListCount($query, $this->getState('limitstart'), $this->getState('limit'));
        }

        return $this->_total;
    }  

	function _buildContentWhere() {
	
		$mainframe =& JFactory::getApplication();
		
		$db =& JFactory::getDBO();
		
		$filter_order     = $mainframe->getUserStateFromRequest( 'filter_ordering_t','filter_ordering_t','a.eventid','cmd' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( 'filter_order_Dir', 'filter_order_Dir', 'asc', 'word' );

		$where = array();

		$where[] = 'a.eventid = b.eventid';
		$where[] = 'a.venue = v.id';
		$where[] = 'a.parent = 0';
		
		
		if($filter_order == 0) {
			$where[] = 'a.eventid > 0';
		}else{
			$where[] = 'a.eventid = '.$filter_order;
		}
		
		
		$where 		= ( count( $where ) ? ' WHERE '. implode( ' AND ', $where ) : '' );

		return $where;
	}

   function getList() {
   
		if (empty($this->_data)) {

		 	$db = JFactory::getDBO();
			
			$where = $this->_buildContentWhere();
		
			## Making the query for showing all the clients in list function
			$sql = 'SELECT a.*, b.eventname, b.groupname, v.venue, v.city, v.id AS venueid
					FROM #__ticketmaster_tickets AS a, #__ticketmaster_events AS b, #__ticketmaster_venues AS v'
					.$where.' ORDER BY b.eventid'; 
		 
		 	$db->setQuery($sql, $this->getState('limitstart'), $this->getState('limit' ));
		 	$this->data = $db->loadObjectList();
		}
		return $this->data;
	}
	
	function store($data, $dest_ticketid, $dest_ticket, $original) {
		
		$db = JFactory::getDBO();
		
		## Get the parent ticketid from the ticket table.
		$sql = 'SELECT parent AS parentticket FROM #__ticketmaster_tickets 
				WHERE ticketid = '.(int)$dest_ticketid.'';
				
		$db->setQuery($sql);
		$data = $db->loadObject();
		
		## Select data from original ticket
		$sql = 'SELECT * FROM #__ticketmaster_coords 
				WHERE ticketid = '.(int)$original.'';
				
		$db->setQuery($sql);
		$coords = $db->loadObjectList();
		
	    for ($i = 0, $n = count($coords); $i < $n; $i++ ){
		
			$row = $coords[$i];
			
			$insert['id'] = NULL;
			$insert['orderid'] = NULL;
			$insert['x_pos'] = $row->x_pos;
			$insert['y_pos'] = $row->y_pos;
			$insert['ticketid'] = $dest_ticketid;
			$insert['seatid'] = $row->seatid;
			$insert['booked'] = NULL;
			$insert['parent'] = $data->parentticket;
			$insert['type'] = $row->type;
			$insert['width'] = $row->width;
			$insert['heigth'] = $row->heigth;
			
			## Save data to DB :)
			self::data($insert);
			## Clear $insert values
			unset($insert);

	    }		
		
	    return true;	
		

	}
	
	function storemulti($eventid, $to_copy) {
		
		$db = JFactory::getDBO();
		
		## Select data from original ticket
		$sql = 'SELECT * FROM #__ticketmaster_coords 
				WHERE ticketid = '.(int)$eventid.'';
				
		$db->setQuery($sql);
		$coords = $db->loadObjectList();
		
	    for ($i = 0, $n = count($coords); $i < $n; $i++ ){
		
			$row = $coords[$i];
			
			$insert['id'] = NULL;
			$insert['orderid'] = NULL;
			$insert['x_pos'] = $row->x_pos;
			$insert['y_pos'] = $row->y_pos;
			$insert['ticketid'] = $to_copy;
			$insert['seatid'] = $row->seatid;
			$insert['booked'] = NULL;
			$insert['parent'] = NULL;
			$insert['type'] = $row->type;
			$insert['width'] = $row->width;
			$insert['heigth'] = $row->heigth;
			
			## Save data to DB :)
			self::data($insert);
			## Clear $insert values
			unset($insert);

	    }		
		
	    return true;	
		

	}
	
	
	function data($data) {

		$row =& $this->getTable();

		## Bind the form fields to the web link table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		## Make sure the web link table is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		} 

		## Store the web link table to the database
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}	
				
	return true;
		
	}

}
?>