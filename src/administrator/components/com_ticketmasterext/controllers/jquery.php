<?php

/****************************************************************
 * @version			Ticketmaster Pro 1.0.2							
 * @package			Ticketmaster									
 * @copyright		Copyright © 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

defined('_JEXEC') or die ('No Access to this file!');

jimport('joomla.application.component.controller');

## This Class contains all data for the car manager
class TicketmasterExtControllerJquery extends JControllerLegacy {

	function __construct() {
		parent::__construct();
		
		## Register Extra tasks
		$this->registerTask( 'add' , 'edit' );
		$this->registerTask('unpublish','publish');
		$this->registerTask('apply','save' );	
	}

	function checkTicket(){
		echo '1';
	}
   
}	
?>
