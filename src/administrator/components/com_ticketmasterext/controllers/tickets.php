<?php

/****************************************************************
 * @version			Ticketmaster Pro 1.0.1							
 * @package			Ticketmaster									
 * @copyright		Copyright © 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

defined('_JEXEC') or die ('No Access to this file!');

jimport('joomla.application.component.controller');

## This Class contains all data for the car manager
class TicketmasterExtControllerTickets extends JControllerLegacy {

	function __construct() {
		parent::__construct();
		
		## Register Extra tasks
		$this->registerTask( 'add' , 'edit' );
		$this->registerTask('unpublish','publish');
		$this->registerTask('apply','save' );	
	}

	## This function will display if there is no choice.
	function display() {
	
		JRequest::setVar( 'layout', 'default');
		JRequest::setVar( 'view', 'tickets');
		parent::display();
	}
   
	function edit() {
	
		JRequest::setVar( 'layout', 'form');
		JRequest::setVar( 'view', 'tickets');		
		JRequest::setVar( 'hidemainmenu', 1 );
		parent::display();

	}
	
	function preferences() {
	
		JRequest::setVar( 'layout', 'preferences');
		JRequest::setVar( 'view', 'tickets');		
		JRequest::setVar( 'hidemainmenu', 1 );
		parent::display();

	}	

	function save() {

		$post = JRequest::get('post');

		$model	=& $this->getModel('tickets');
		
		$chart = JRequest::getVar( 'seatchart', '', 'files', 'array' );

		if ($chart['name']){
	
			jimport('joomla.filesystem.file');
			
			$chart['name'] = JFile::makeSafe($chart['name']);
			$filename = JFile::makeSafe($chart['name']);
			
			## The link to the previous saved data.
			$link = 'index.php?option=com_ticktmasterext&controller=tickets';
			
			$allowed = array('image/pjpeg','image/gif','image/png','image/jpeg','image/JPG','image/jpg');
			
			## Check if the image is in the of the supported extentions
			if (!in_array($chart['type'], $allowed)){
				JError::raiseWarning(100, ''.$chart['name'].' '.JText::_( 'COM_TICKETMASTEREXT_ONLY_JPG_PNG' ).'');
				$this->setRedirect($link);
			}

			$path 	= JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_ticketmasterext'.DS.'assets'.DS.'seatcharts'.DS;
			$ext =  JFile::getExt($filename);		
			
			chmod ( $chart['tmp_name'], 0755);
			
			## Moving the file now to the destination folder (images), offcourse with the new name.
			if (!JFile::upload($chart['tmp_name'], $path.$post['id'].'.'.$ext)) {
				JError::raiseWarning(100, ''.$chart['name'].' '.JText::_( 'COM_TICKETMASTER_COULD_NOT_MOVE_FILE').'');
				$this->setRedirect($link);
			}
			
			$post['seat_chart'] = $post['id'].'.'.$ext;
			
		}			

		if ($model->store($post)) {
			$msg = JText::_( 'COM_TICKETMASTEREXT_TICKETPREFS_SAVED' );
		} else {
			$msg = JText::_( 'COM_TICKETMASTEREXT_TICKETPREFS_SAVED_FAILED' );
		}
		
		$link = 'index.php?option=com_ticketmasterext&controller=tickets';
		$this->setRedirect($link, $msg);
	}
	
	function getRecord() {

		$ticketid = JRequest::getInt('ticketid', 0);
		
		$db = JFactory::getDBO();

		## Load the dimensions of this seat
		$sql = 'SELECT * FROM #__ticketmaster_tickets_ext 
				WHERE ticketid = '.(int)$ticketid.''; 
	 
		$db->setQuery($sql);
		$seat = $db->loadObject();	
	
		## Loading the latest coord.
		$sql = 'SELECT * FROM #__ticketmaster_coords 
				WHERE ticketid = '.(int)$ticketid.' 
				ORDER BY seatid DESC LIMIT 0,1'; 
	 
		$db->setQuery($sql);
		$item = $db->loadObject();	
		
		## selecting the parent.
		$sql = 'SELECT parent FROM #__ticketmaster_tickets 
				WHERE ticketid = '.(int)$ticketid.''; 
	 
		$db->setQuery($sql);
		$result = $db->loadObject();				
		
		if(count($item) == 0){
			
			$query = "INSERT INTO #__ticketmaster_coords (orderid, x_pos, y_pos, ticketid, seatid, booked, type, parent, width, heigth) 
						VALUES (0, 10, 10, ".$ticketid.", 1, 0, ".$seat->type.", ".$result->parent.", ".$seat->seat_width.", ".$seat->seat_heigth." )";	
			
			$db->setQuery( $query );
			$db->query();
			
			$dataid = $db->insertid();	
			$seatid = 1;
						 			
		
		}else{
			
			$seatid = $item->seatid+1;
			
			$query = "INSERT INTO #__ticketmaster_coords (orderid, x_pos, y_pos, ticketid, seatid, booked, type, parent, width, heigth) 
						VALUES (0, 10, 10, ".$ticketid.", ".(int)$seatid.", 0, ".$seat->type.", ".$result->parent.", ".$seat->seat_width.", ".$seat->seat_heigth.")";	
			
			$db->setQuery( $query );
			$db->query();
			
			$dataid = $db->insertid();				
			
		}		
		
		$arr = array('seatid' => $seatid, 'id' => $dataid, 'seat_width' => $seat->seat_width, 'seat_heigth' => $seat->seat_heigth);
		
		echo json_encode($arr);		
		
	}

	function saveRecord() {

		//decode JSON data received from AJAX POST request
		$data = json_decode($_POST["data"]);
		
		foreach($data->coords as $item) {
			
			##Extract X number for panel
			$x_coord = preg_replace('/[^\d\s]/', '', $item->coordTop);
			##Extract Y number for panel
			$y_coord = preg_replace('/[^\d\s]/', '', $item->coordLeft);
			
			## Insert PDF into DB
			$db =& JFactory::getDBO();
			$sql = 'UPDATE #__ticketmaster_coords SET x_pos = "'.$x_coord.'", y_pos = "'.$y_coord.'" WHERE id = "'.$item->coordId.'" ';
			$db->setQuery($sql);
			
			if (!$db->query() ){
				echo "<script>alert('".$row->getError()."');
				window.history.go(-1);</script>\n";		 
			}	
			
		}

		echo "success";
		
	}

	function removerecord() {

		$id = JRequest::getInt('id', 0);
		
		$db  = JFactory::getDBO();
		
		## Make the query to delete one of the categories.
		$query = 'DELETE FROM #__ticketmaster_coords WHERE id ='.(int)$id.'';
		$db->setQuery($query);
		
		if (!$db->query() ){
			$arr = array('result' => '0', 'id' => $id);	 
		}else{
			$arr = array('result' => '1', 'id' => $id);	
		}
			
		echo json_encode($arr);		
				
	}

   
}	
?>
