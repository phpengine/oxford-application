<?php

/****************************************************************
 * @version			Ticketmaster Pro 1.0.1							
 * @package			Ticketmaster									
 * @copyright		Copyright © 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

defined('_JEXEC') or die ('No Access to this file!');

jimport('joomla.application.component.controller');

## This Class contains all data for the car manager
class TicketmasterExtControllerImport extends JControllerLegacy {

	function __construct() {
		parent::__construct();
		
		## Register Extra tasks
		$this->registerTask( 'add' , 'edit' );
		$this->registerTask('unpublish','publish');
		$this->registerTask('apply','save' );	
	}

	## This function will display if there is no choice.
	function display() {
	
		JRequest::setVar( 'layout', 'default');
		JRequest::setVar( 'view', 'import');
		parent::display();
	}
   
	function edit() {
	
		JRequest::setVar( 'layout', 'form');
		JRequest::setVar( 'view', 'import');		
		JRequest::setVar( 'hidemainmenu', 1 );
		parent::display();

	}
	
	function save(){
		
		## Getting the application for redirection.
		$app  = JFactory::getApplication();
		$link = 'index.php?option=com_ticketmasterext&controller=import';
		
		## Getting the post variables
		$post = JRequest::get('post');

		if ($post['multi'] == 0){
			
			if ($post['ticketid'] == 0) {
				JError::raiseWarning(500, JText::_('COM_TICKETMASTEREXT_SELECT_TICKET_TO_TRANSFER'));
				$app->redirect($link);	
			}	
			
			if ($post['dest_ticket'] == 0) {
				JError::raiseWarning(500, JText::_('COM_TICKETMASTEREXT_SELECT_TICKET_DESTINATION'));
				$app->redirect($link);	
			}						
						
		}
		
		if ($post['eventid'] == 0) {
			JError::raiseWarning(500, JText::_('COM_TICKETMASTEREXT_SELECT_EVENT_TO_TRANSFER'));
			$app->redirect($link);	
		}
		
		if ($post['dest_list'] == 0){
			JError::raiseWarning(500, JText::_('COM_TICKETMASTEREXT_SELECT_EVENT_DESTINATION'));
			$app->redirect($link);			
		}
		
		## Getting the proper model	
		$model	= $this->getModel('import');
		
		if ($post['multi'] == 0){
			
			if ($model->store($post, $post['dest_ticket'], $post['dest_list'], $post['ticketid'])) {
				$msg = JText::_( 'COM_TICKETMASTEREXT_SEATS_HAS_BEEN_TRANSFERED' );
			} else {
				$msg = JText::_( 'COM_TICKETMASTEREXT_SEATS_HAS_NOT_BEEN_TRANSFERED' );
			}
			
		}else{

			if ($model->storemulti($post['eventid'], $post['dest_list'])) {
				$msg = JText::_( 'COM_TICKETMASTEREXT_SEATS_HAS_BEEN_TRANSFERED' );
			} else {
				$msg = JText::_( 'COM_TICKETMASTEREXT_SEATS_HAS_NOT_BEEN_TRANSFERED' );
			}
		
		}
		 
		 ## OK, everything is done, redirect the user now.
		$app->redirect($link, $msg);		


		
	}
	
	function getTickets(){
		
		$db = JFactory::getDBO();
	
		$id = JRequest::getInt('id', 0);
		
		$sql = 'SELECT ticketid, ticketname 
				FROM #__ticketmaster_tickets 
				WHERE parent = '.(int)$id.'  
				ORDER BY ordering';
				
		$db->setQuery($sql);
		
		$tickets[]	= JHTML::_('select.option',  '0', JText::_( 'COM_TICKETMASTEREXT_SELECT_TICKET' ), 'ticketid', 'ticketname' );
		$tickets    = array_merge( $tickets, $db->loadObjectList() );
		$lists['ticketlist']  = JHTML::_('select.genericlist',  $tickets, 'ticketid', 'class="input" size="1" ', 'ticketid', 'ticketname', 0);
		 
		 echo $lists['ticketlist'];

	}	
	
	function getDestinationTickets(){
		
		$db = JFactory::getDBO();
	
		$id = JRequest::getInt('id', 0);
		
		$sql = 'SELECT ticketid, ticketname 
				FROM #__ticketmaster_tickets 
				WHERE parent = '.(int)$id.'  
				ORDER BY ordering';
				
		$db->setQuery($sql);
		
		$tickets[]	= JHTML::_('select.option',  '0', JText::_( 'COM_TICKETMASTEREXT_SELECT_TICKET_DESTINATION' ), 'ticketid', 'ticketname' );
		$tickets    = array_merge( $tickets, $db->loadObjectList() );
		$lists['ticketlist']  = JHTML::_('select.genericlist',  $tickets, 'dest_ticket', 'class="input" size="1" ', 'ticketid', 'ticketname', 0);
		 
		 echo $lists['ticketlist'];

	}	
	   
}	
?>
