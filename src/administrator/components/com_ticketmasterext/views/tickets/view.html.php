<?php
/****************************************************************
 * @version			Ticketmaster Pro 1.0.2						
 * @package			Ticketmaster									
 * @copyright		Copyright © 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

## no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view' );

class TicketmasterExtViewTickets extends JViewLegacy {
	
	function display($tpl = null) {

		## If we want the add/edit form..
		if($this->getLayout() == 'form') {
			$this->_displayForm($tpl);
			return;
		}
		
		## If we want the add/edit form..
		if($this->getLayout() == 'preferences') {
			$this->_displayPreferences($tpl);
			return;
		}			

		$mainframe = JFactory::getApplication();
		
		$db    = JFactory::getDBO();	
		
		$filter_order = $mainframe->getUserStateFromRequest( 'filter_order', 'filter_order', 'ordering', 'cmd' ); 

		## table ordering
		$lists['order']     = $filter_order;		
		
		## Model is defined in the controller
		$model	= $this->getModel('tickets');
		
		## Getting the items into a variable
		$added	  = $this->get('added');
		$settings = $this->get('settings');
		$items	  = $this->get('list');
		$childs	  = $this->get('childs');
		$totals	  = $this->get('totals');
		$booked	  = $this->get('totalbooked');
		$pagination =  $this->get( 'pagination' );

		$db    = JFactory::getDBO();	
		
		## Making the query for getting the config from the ticketmaster tables.
		$sql='SELECT  * FROM #__ticketmaster_config WHERE configid = 1'; 
	 
		$db->setQuery($sql);
		$config = $db->loadObject();		

		$filter_order = $mainframe->getUserStateFromRequest( 'filter_ordering_t', 'filter_ordering_t', 'asc', 'word' );

		$query = "SELECT eventid, CONCAT(groupname, ' - ' , eventname) AS name FROM #__ticketmaster_events
				  WHERE published = 1 ORDER BY eventid ASC"; 
		
		$db->setQuery($query);
		
		$eventlist[]	  = JHTML::_('select.option',  '0', JText::_( 'COM_TICKETMASTEREXT_PLS_SELECT' ), 'eventid', 'name' );
		$eventlist	      = array_merge( $eventlist, $db->loadObjectList() );
		$lists['eventid'] = JHTML::_('select.genericlist',  $eventlist, 'filter_ordering_t', 'class="input" size="1" ','eventid', 'name', intval($filter_order) );

		$this->assignRef('totals', $totals);
		$this->assignRef('settings', $settings);
		$this->assignRef('booked', $booked);
		$this->assignRef('items', $items);
		$this->assignRef('pagination', $pagination);
		$this->assignRef('config', $config);
		$this->assignRef('childs', $childs);
		$this->assignRef('lists', $lists);
		parent::display($tpl);		

	}

	function _displayPreferences($tpl = null){

		## Model is defined in the controller
		$model	= $this->getModel('tickets');
		
		## Getting the items into a variable
		$data	= $this->get('preferences');

		$multi = array(
			'0' => array('value' => '0', 'text' => JText::_( 'COM_TICKETMASTEREXT_NO' )),
			'1' => array('value' => '1', 'text' => JText::_( 'COM_TICKETMASTEREXT_YES' )),
		);
		$lists['multi_seat'] = JHTML::_('select.genericList', $multi, 'multi_seat', ' class="inputbox" '. '', 
										'value', 'text', $data->multi_seat );

		$type = array(
			'1' => array('value' => '1', 'text' => JText::_( 'COM_TICKETMASTEREXT_SEAT' )),
			'2' => array('value' => '2', 'text' => JText::_( 'COM_TICKETMASTEREXT_SECTOR' )),
		);
		$lists['type'] = JHTML::_('select.genericList', $type, 'type', ' class="inputbox" '. '', 
										'value', 'text', $data->type );	
			
		$this->assignRef('lists', $lists);	
		$this->assignRef('data', $data);
		parent::display($tpl);
		
	}
	
	function _displayForm($tpl){
		
		## Model is defined in the controller
		$model	= $this->getModel('tickets');
		
		## Getting the items into a variable
		$data	= $this->get('data');
		
		## Check if this is Joomla 2.5 or 3.0.+
		## We want to load the j3 form for JQ issues.
		
		$isJ30 = version_compare(JVERSION, '3.0.0', 'ge');
		
		if($isJ30) {	
			$tpl = 'j3';
		}
		
		if($data->multi_seat != 1) {
			$items = $this->get('seats');
		}else{
			$items = $this->get('nochilds');
		}
		
		$array    = JRequest::getVar('cid', array(0), '', 'array');
		$this->id = (int)$array[0]; 		
		
		$db 	  = JFactory::getDBO();
		
		if($data->multi_seat != 1) {
		
			$query = 'SELECT ticketid AS id, ticketname AS name FROM #__ticketmaster_tickets
					  WHERE published = 1 
					  AND parent = '.(int)$this->id.' 
					  ORDER BY ticketname ASC'; 
			
			$db->setQuery($query);
			
			$eventlist[]	  = JHTML::_('select.option',  '0', JText::_( 'COM_TICKETMASTEREXT_PLS_SELECT' ), 'id', 'name' );
			$eventlist	      = array_merge( $eventlist, $db->loadObjectList() );
			$lists['ticketid'] = JHTML::_('select.genericlist',  $eventlist, 'single', 'class="input" size="1" ', 'id', 'name', intval(0) );	
			
		}else{

			$query = 'SELECT ticketid AS id, ticketname AS name FROM #__ticketmaster_tickets
					  WHERE published = 1 
					  AND ticketid = '.(int)$this->id.''; 
			
			$db->setQuery($query);
			$eventinfo = $db->loadObject();
		
			$eventlist[]  = JHTML::_('select.option',  (int)$this->id, JText::_( 'COM_TICKETMASTEREXT_ALL_SEATS_POSSIBLE' ), 'id', 'name' );		
			$lists['ticketid'] = JHTML::_('select.genericlist',  $eventlist, 'single', 'class="input" size="1" ', 'id', 'name', intval(0) );				
		
		}

		$this->assignRef('items', $items);
		$this->assignRef('data', $data);
		$this->assignRef('lists', $lists);
		parent::display($tpl);
	}
}
?>