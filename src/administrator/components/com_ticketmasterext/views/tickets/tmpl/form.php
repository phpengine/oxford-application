<?php

/****************************************************************
 * @version			Ticketmaster Pro 1.0.1							
 * @package			Ticketmaster									
 * @copyright		Copyright � 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

## Check if the file is included in the Joomla Framework
defined('_JEXEC') or die ('No Acces to this file!');

## Get document type and add it.
$document = &JFactory::getDocument();
## Add bootstrap css file to this one:
$document->addStyleSheet( JURI::root(true).'/administrator/components/com_ticketmasterext/assets/bootstrap/css/bootstrap.css' );
## Add the required javascripts & CSS scripts in order to work.
$document->addScript( JURI::root(true).'/administrator/components/com_ticketmasterext/helpers/js/jquery-1.3.2.min.js');
$document->addScript( JURI::root(true).'/administrator/components/com_ticketmasterext/helpers/js/jquery-ui-1.7.2.custom.min.js');
$document->addScript( JURI::root(true).'/administrator/components/com_ticketmasterext/helpers/js/jquery.json-2.2.min.js');
$document->addStyleSheet( JURI::root(true).'/administrator/components/com_ticketmasterext/helpers/css/style.css' );

$document->addScript( JURI::root(true).'/administrator/components/com_ticketmasterext/helpers/js/jquery.json-2.2.min.js');

## The image of the seat chart
$seatchart = JURI::root(true).'/administrator/components/com_ticketmasterext/assets/seatcharts/'.$this->data->seat_chart; 

$image = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmasterext'.DS.'assets'.DS.'seatcharts'.DS.$this->data->seat_chart;
## Get the image size
list($width, $height, $type, $attr) = getimagesize($image);

?>

<style>

#glassbox {
	background:#FFF;
	height:<?php echo $height+20; ?>px;
	background-repeat:no-repeat;
	background-position: 0px 30px;
	margin:5px auto auto auto;
	position:relative;
	width:<?php echo $width; ?>px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
}

.seat-element {
	border:1px #198d02 solid;
	background-color:#e1fdda;
	cursor:move;
	cursor:crosshair;
	height:14px;
	width:14px;
	font-size:9px;
	color:#000;
	text-align:center;
	line-height:14px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
}

.seat-element:hover {
	background-color:#daf5fd;
	border:1px #CCC solid;
}

</style>

<div align="center">
<div align="center">
	<form class="form-inline" style="padding-top:8px;">
    <a href="index.php?option=com_ticketmasterext&controller=tickets" class="btn btn-primary"> 
		<?php echo JText::_( 'COM_TICKETMASTEREXT_BACK_TO_OVERVIEW' ); ?></a>	
	<?php echo $this->lists[ticketid]; ?> <a href="#" id="addSeat" class="btn btn-success">
		<?php echo JText::_( 'COM_TICKETMASTEREXT_ADD_SEAT_SECTOR' ); ?></a>
    | |
    <input name="remove" type="text" value="" size="1" maxlength="5" style="width:35px; text-align:center;" class="styled-imputbox" id="remove" /> 
    <a href="#" id="removeSeat" class="btn btn-danger"> 
		<?php echo JText::_( 'COM_TICKETMASTEREXT_REMOVE_SEAT_SECTOR' ); ?></a>
    <a href="index.php?option=com_ticketmasterext&controller=tickets" class="btn btn-primary"> 
		<?php echo JText::_( 'COM_TICKETMASTEREXT_BACK_TO_OVERVIEW' ); ?></a>	        
    </form>
</div>
</div>

<p style="width: 800px; height:20px; margin:8px auto 0 auto; color:#000; text-align:center;"></p>

<div id="glassbox" style="background-image: url(<?php echo $seatchart; ?>);">

<?php 

	   $k = 0;
	   for ($i = 0, $n = count($this->items); $i < $n; $i++ ){
			
			## Give give $row the this->item[$i]
			$row        = &$this->items[$i];
		
			$x = $row->x_pos;
			$y = $row->y_pos;
			
			if ($row->booked > 0){
				$style = 'color:#FFF; border-color:#000; background-color:#FF0000;';
			}else{
				$style = 'color:#'.$row->font_color.'; border-color:#'.$row->border_color.';';
				if ($row->background_color != ''){
					$background = $row->background_color;
				}else{
					$background = 'e1fdda';
				}
			}
			
			## This is a seat --> Load seat data.
			if ($row->type == 1){
				echo '<div id="'.$row->id.'" class="seat-element" 
						style="left:'.$x.'px; top:'.$y.'px; background-color:#'.$background.';
							   width:'.$row->width.'px; height:'.$row->heigth.'px; line-heigth:'.$row->heigth.'px; 
							   position:absolute; '.$style.'")">'.$row->seatid.'</div>';		
			}else{
				echo '<div id="'.$row->id.'" class="seat-element" 
						style="left:'.$x.'px; top:'.$y.'px;  background-color:#'.$background.'; 
							   width:'.$row->width.'px; height:'.$row->heigth.'px; line-heigth:'.$row->heigth.'px; 
							   position:absolute; '.$style.'")">
							   		<div style = "line-height:'.$row->heigth.'px;"><strong>'.$row->ticketname.'</strong></div>
							   </div>';					
			}
			
	   $k=1 - $k;
	   }			
?>

</div>

<p style="width: 800px; height:20px; margin:8px auto 0 auto; color:#000; text-align:center;"></p>

<div id="respond"></div>

<script type="text/javascript">

var JQ = jQuery.noConflict();
	
JQ(document).ready(function () {
    makeDraggable(JQ(".seat-element"));
});

function makeDraggable (jQueryElm) {
    jQueryElm.draggable({ 
            containment: '#glassbox', 
            scroll: false
     }).mousemove(function(){
			var coord = JQ(this).position();
			JQ("p:last").text( "Positions: Left: " + coord.left + ", Top: " + coord.top + " - Database ID: " + JQ(this).attr("id") );
			JQ("p:first").text( "Positions: Left: " + coord.left + ", Top: " + coord.top + " - Database ID: " + JQ(this).attr("id") );
     }).mouseup(function(){ 
        var coords=[];
        var currentId = JQ(this).attr('id');
        //alert(currentId);
        var coord = JQ(this).position();
        var item={ coordTop:  coord.left, coordLeft: coord.top, coordId: currentId };
        coords.push(item);
        var order = { coords: coords };
		JQ.post('index.php?option=com_ticketmasterext&controller=tickets&task=saverecord&format=raw', 'data='+JQ.toJSON(order), function(response){
        //JQ.post('updatecoords.php', 'data='+JQ.toJSON(order), function(response){
        if(response == "success")
            JQ("#respond").html('<div class="success"style="text-align:center;">Seat Position has been saved to the database.</div>').hide().fadeIn(1000);
            setTimeout(function(){ JQ('#respond').fadeOut(2000); }, 2000);
        }); 
    });
}
		
JQ("#addSeat").bind("click", function(e){
	
	var singleValue = JQ("#single").val();
	
	if (singleValue == 0){
		return false;
	}
	
	JQ.getJSON("index.php?option=com_ticketmasterext&controller=tickets&task=getrecord&ticketid="+ singleValue +"&format=raw",
	
	function(data){
		
		if (data.id == 0){
			return false;
		}
		
		// Create a new div in the glassbox div. 	
 		var elm = JQ('<div id="'+data.id+'" class="seat-element" style="left:30px; top:30px; position:absolute;">'+data.seatid+'</div>').appendTo("#glassbox");		
		
		// Pass the element to makeDraggable
		makeDraggable(elm); 	
		JQ("#"+data.id+"").animate({height: data.seat_heigth, width: data.seat_width})
		
	});
});


JQ("#removeSeat").bind("click", function(e){
	
	var removeValue = JQ("#remove").val();
	
	if (removeValue == 0){
		return false;
	}
	
	JQ.getJSON("index.php?option=com_ticketmasterext&controller=tickets&task=removerecord&id="+ removeValue +"&format=raw",
	
	function(data){

		JQ("#"+data.id+"").hide('slow');
		
		
	});
});
</script>
