<?php
/****************************************************************
 * @version			Ticketmaster Pro 1.0.2							
 * @package			Ticketmaster									
 * @copyright		Copyright © 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

## no direct access
defined('_JEXEC') or die('Restricted access');

## Add the CSS file for extra toolbars.
$document = & JFactory::getDocument();
$document->addStyleSheet('components/com_ticketmasterext/assets/css/ticketmasterext.css');
$document->addScript( JURI::root(true).'/administrator/components/com_ticketmasterext/assets/jquery/javascript3.js');

## Setup the toolbars.
JToolBarHelper::title( JText::_( 'COM_TICKETMASTEREXT_TICKETS_OVERVIEW' ), 'generic.png' );
JToolbarHelper::custom( 'preferences', 'preferences', '', JText::_( 'COM_TICKETMASTEREXT_PREFERENCES' ), false, false);

## Adding the old $option to the script
$option = 'com_ticketmaster';

## Require specific menu-file.
$path = JPATH_COMPONENT.DS.'assets'.DS.'menu.php';
if (file_exists($path)) {
	include_once $path;
}

## Check if this is Joomla 2.5 or 3.0.+
$isJ30 = version_compare(JVERSION, '3.0.0', 'ge');

if(!$isJ30) {
	## Adding mootools for J!2.5
	JHTML::_('behavior.modal');
	## Loading the mootools drivers.
	JHTML::_('behavior.mootools');	
	## Include the tooltip behaviour.
	JHTML::_('behavior.tooltip', '.hasTip');
	$document->addStyleSheet( JURI::root(true).'/administrator/components/com_ticketmasterext/assets/bootstrap/css/bootstrap.css' ); 
	$document->addScript( JURI::root(true).'/administrator/components/com_ticketmasterext/assets/bootstrap/js/bootstrap.js');
}else{
	$document->addScript( JURI::root(true).'/administrator/components/com_ticketmasterext/assets/jquery/jquery.js');
	jimport('joomla.html.html.bootstrap');	
}

## Check the settings for proper working:
$total_tickets = count($this->childs)+count($this->items);

if($total_tickets != $this->settings->total){ ?>
    
    <div class="alert alert-block">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <h4><?php echo JText::_( 'COM_TICKETMASTEREXT_ERORR' ); ?> <?php echo JText::_( 'COM_TICKETMASTEREXT_ERORR_SETTINGS_INCOMPLETE' ); ?></h4>
      <p><?php echo JText::_( 'COM_TICKETMASTEREXT_SETTINGS_INCOMPLETE' ); ?></p>
    </div>
    
<?php 
	$edit_button = 'btn-warning';
}
?>

<script language="javascript">
$(document).ready(function() {
  $('.alert').hide();
});;
</script>

<form action = "index.php" method="POST" name="adminForm" class="form-inline">
<table>
<tr>
	<td align="left" width="100%">
		
	</td>
	<td nowrap="nowrap">
		<?php echo $this->lists['eventid']; ?>
		<button class="btn btn-small" onclick="this.form.submit();"><?php echo JText::_( 'COM_TICKETMASTEREXT_SEARCH' ); ?></button>
	</td>
</tr>
</table>
<div id = "check">
  <table class="table table-striped" width="100%">
    <thead>
      <tr>
        <th class="title" width="5"><div align="center">##</div></th>
        <th class="title" width="5"><div align="center"></div></th>
        <th class="title"><div align="left"><?php echo JText::_( 'COM_TICKETMASTEREXT_TICKETNAME' ); ?></div></th>
        <th class="title" width="100"><div align="center"><?php echo JText::_( 'COM_TICKETMASTEREXT_BOOKEDSEATS' ); ?></div></th>
        <th class="title" width="100"><div align="center"><?php echo JText::_( 'COM_TICKETMASTEREXT_SEATS' ); ?></div></th>
        <th class="title" width="150"><div align="center"><?php echo JText::_( 'COM_TICKETMASTEREXT_SECTOR' ); ?></div></th>
        <th class="title" width="150"><div align="center"><?php echo JText::_( 'COM_TICKETMASTEREXT_SEATCHARTS' ); ?></div></th>
        <th class="title" width="128"><div align="center"><?php echo JText::_( 'COM_TICKETMASTEREXT_TICKETPRICE' ); ?></div></th>
        <th class="title" width="121"><div align="center"><?php echo JText::_( 'COM_TICKETMASTEREXT_PUBLISHED' ); ?></div></th>
      </tr>
    </thead>
    <?php 
	   
	   $k = 0;
	   for ($i = 0, $n = count($this->items); $i < $n; $i++ ){
		
		## Give give $row the this->item[$i]
		$row        = $this->items[$i];
		$published 	= JHTML::_('grid.published', $row, $i );
		$checked    = JHTML::_('grid.id', $i, $row->ticketid );
		$link       = 'index.php?option=com_ticketmasterext&controller=tickets&task=edit&tmpl=component&cid[]='.$row->ticketid;
		$prefs      = 'index.php?option=com_ticketmasterext&controller=tickets&task=preferences&cid[]='.$row->ticketid;
		

	?>
    <tr>
      <td><div align="center"><?php echo $checked; ?></div></td>
      <td colspan="2"><div align="left" style="font-size:105%;"><strong><?php echo $row->ticketname; ?></strong>
      	<a href="<?php echo $prefs; ?>" id="id_<?php echo $row->ticketid; ?>" class="btn btn-small pull-right <?php echo $edit_button; ?>" type="button">
			<i class="icon-pencil"> </i> <?php echo JText::_( 'COM_TICKETMASTEREXT_SETTINGS' ); ?>
        </a></div>
      </td>
      <td><div align="center">
        <?php showBooked($row->ticketid, $this->booked); ?>
      </div></td>
      <td><div align="center">
        <?php showCounter($row->ticketid, $this->totals, $row->totaltickets); ?>
      </div></td>
      <td><div align="center">
        <?php showCounter($row->ticketid, $this->totals, $row->totaltickets); ?>
      </div></td>
      <td>
          <div align="center"><a href="<?php echo $link; ?>"
               title="<?php echo $row->ticketname; ?>" class="btn btn-small"><i class="icon-star"> </i> <?php echo JText::_( 'COM_TICKETMASTEREXT_SEATCHARTS' ); ?></a> 
          </div>
      </td>
      <td><div align="center"><?php echo $this->config->valuta; ?> 
	  		<?php echo number_format($row->ticketprice, 2, ',', ''); ?></div></td>
      <td width="121">
  		<div align="center">
		  <?php if ($row->published == 1){ ?>
            <span class="label label-success"><?php echo JText::_( 'COM_TICKETMASTEREXT_YES' ); ?></span>
          <?php }else{ ?>
            <span class="label label-important"><?php echo JText::_( 'COM_TICKETMASTEREXT_NO' ); ?></span>
          <?php } ?>
  		</div></td>
    </tr>
    
   			<?php

               $k2 = 0;
               for ($i2 = 0, $n2 = count($this->childs); $i2 < $n2; $i2++ ){
                
                ## Give give $row the this->item[$i]
                $second       = $this->childs[$i2];
                $publishing   = JHTML::_('grid.published', $second, $i2 );
                $checked      = JHTML::_('grid.id', $i2, $second->ticketid );
                $link         = 'index.php?option=com_ticketmasterext&controller=tickets&task=edit&tmpl=component&cid[]='.$second->ticketid;
				$link_tm      = 'index.php?option=com_ticketmaster&controller=tickets&task=edit&cid[]='.$second->ticketid;
				$prefs_childs = 'index.php?option=com_ticketmasterext&controller=tickets&task=preferences&cid[]='.$second->ticketid;
        
            ?>
            <?php if ($row->ticketid == $second->parent) { ?>
            
            <tr class="<?php echo "row$k2"; ?>">
              <td valign="middle"><div align="center"></div></td>
              <td valign="middle"><div align="center"><?php echo $checked; ?></div></td>
              <td valign="middle">
              	<a href="<?php echo $link_tm; ?>"><?php echo $second->ticketname; ?></a> 
                <a href="<?php echo $prefs_childs; ?>" id="id_<?php echo $second->ticketid; ?>" class="btn btn-small pull-right <?php echo $edit_button; ?>" type="button">
                    <i class="icon-pencil"> </i> <?php echo JText::_( 'COM_TICKETMASTEREXT_SETTINGS' ); ?>
                </a>
              </td>
              <td valign="middle"><div align="center">
                <?php showBooked($second->ticketid, $this->booked); ?>
              </div></td>
              <td valign="middle"><div align="center"><?php showCounter($second->ticketid, $this->totals, $second->totaltickets); ?></div></td>
              <td valign="middle"><div align="center"><?php showSector($second->ticketid, $this->totals, $second->totaltickets); ?>
              </div></td>
              <td valign="middle"><div align="center"></div></td>
              <td valign="middle"><div align="center"><?php echo $this->config->valuta; ?> <?php echo number_format($second->ticketprice, 2, ',', ''); ?></div>
              </td>
              <td width="121" valign="middle">
                <div align="center">
                  <?php if ($second->published == 1){ ?>
                  	<span class="label label-success"><?php echo JText::_( 'COM_TICKETMASTEREXT_YES' ); ?></span>
                  <?php }else{ ?>
                  	<span class="label label-important"><?php echo JText::_( 'COM_TICKETMASTEREXT_NO' ); ?></span>
                  <?php } ?>
                </div>                
              </td>
    		</tr>
            
            <?php } ?>
            
            <?php
              $k=1 - $k;
              }
            ?>    
    
    
    <?php
	  $k=1 - $k;
	  }
	  ?>
  </table>

</div>    
  
  <input name = "option" type="hidden" value="com_ticketmasterext" />
  <input name = "task" type="hidden" value="" />
  <input name = "boxchecked" type="hidden" value="0"/>
  <input name = "controller" type="hidden" value="tickets"/>
  <input type="hidden" name="filter_order" value="ordering" />
  <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />  
  <?php echo JHTML::_( 'form.token' ); ?>  
  

</form>

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><?php echo JText::_( 'COM_TICKETMASTEREXT_TICKET_TOTAL_INCORRECT' ); ?></h3>
  </div>
  <div class="modal-body">
    <p><?php echo JText::_( 'COM_TICKETMASTEREXT_TICKET_TOTAL_INCORRECT_DESC' ); ?></p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><?php echo JText::_( 'COM_TICKETMASTEREXT_CLOSE' ); ?></button>
  </div>
</div>

<?php 
function showCounter($ticketid, $totals, $totaltickets){
	   
	$tmp = 0;
	for ($i5 = 0, $n5 = count($totals); $i5 < $n5; $i5++ ){
	
		## Give give $row the this->item[$i]
		$row = $totals[$i5];
		
		if ($row->ticketid == $ticketid){
			
			if ($row->total > 1){ 
			
				if ($row->total != 0){
					
					if ($row->total <= $totaltickets) { 
						## Show the amount of ticket to be done.
						echo '<strong>'.$row->total.'/'.$totaltickets.'</strong>';
					}else{
						## The ticket amount needs to be increased.
						echo '<a href="#myModal" role="button" id="warning" class="btn btn-small btn-danger" data-toggle="modal">'.
							JText::_( 'COM_TICKETMASTEREXT_UPDATE_TOTAL' ).'</a>';
							//<a href="#myModal" role="button" class="btn" data-toggle="modal">Launch demo modal</a>
					}
					
				}
				
			}
		
		}
		
	$tmp=1 - $tmp;
	}

}

function showSector($ticketid, $totals, $totaltickets){
	   
	$tmp = 0;
	for ($i5 = 0, $n5 = count($totals); $i5 < $n5; $i5++ ){
	
		## Give give $row the this->item[$i]
		$row = $totals[$i5];
		
		if ($row->ticketid == $ticketid){
			
			if ($row->total == 1){
				
				echo '<strong>'.$totaltickets.' ('.JText::_( 'COM_TICKETMASTEREXT_PERSONS').')</strong>';
				
			}
		
		}
		
	$tmp=1 - $tmp;
	}

}

function showBooked($ticketid, $totals){
	   
	$tmp = 0;
	for ($i5 = 0, $n5 = count($totals); $i5 < $n5; $i5++ ){
	
		$row = $totals[$i5];
		
		if ($row->ticketid == $ticketid){
			
			if ($row->totalbooked == 1){
				echo '<strong>'.$row->totalbooked.' '.JText::_( 'COM_TICKETMASTEREXT_SEATTOTAL').'</strong>';
			}else{
				echo '<strong>'.$row->totalbooked.' '.JText::_( 'COM_TICKETMASTEREXT_SEATSTOTAL').'</strong>';
			}
		}
		
	$tmp=1 - $tmp;
	}

}
