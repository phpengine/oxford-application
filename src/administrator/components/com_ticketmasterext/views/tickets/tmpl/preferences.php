<?php

/****************************************************************
 * @version			Ticketmaster Pro 1.0.1							
 * @package			Ticketmaster									
 * @copyright		Copyright � 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

## Check if the file is included in the Joomla Framework
defined('_JEXEC') or die ('No Acces to this file!');

JToolBarHelper::title(JText::_( 'COM_TICKETMASTEREXT_TICKET_PREFERENCES' ).' <small>[ '.$this->data->ticketname.' ]</small>', 'generic.png');
JToolBarHelper::save();
if ($this->data->ticketid < 1)  {
	## Cancel the operation
	JToolBarHelper::cancel();
} else {
	## For existing items the button is renamed `close`
	JToolBarHelper::cancel( 'cancel', JText::_( 'COM_TICKETMASTEREXT_CLOSE' ) );
};

$document = & JFactory::getDocument();

## Check if this is Joomla 2.5 or 3.0.+
$isJ30 = version_compare(JVERSION, '3.0.0', 'ge');

if(!$isJ30) {
	## Adding mootools for J!2.5
	JHTML::_('behavior.modal');
	## Loading the mootools drivers.
	JHTML::_('behavior.mootools');	
	## Include the tooltip behaviour.
	JHTML::_('behavior.tooltip', '.hasTip');
	$document->addStyleSheet( JURI::root(true).'/administrator/components/com_ticketmasterext/assets/bootstrap/css/bootstrap.css' ); 
	$document->addScript( JURI::root(true).'/administrator/components/com_ticketmasterext/assets/bootstrap/js/bootstrap.js');
}

?>

<form action = "index.php" method="POST" name="adminForm" id="adminForm" enctype="multipart/form-data">

<div class="row-fluid">
  <div class="span6">
  		
        <?php if ($this->data->parent == 0) { ?>

            <table class="table table-striped">
                <tr>
                    <td width="50%"><?php echo JText::_( 'COM_TICKETMASTEREXT_SEATCHART_CHOICE' ); ?></label></td>
                    <td  width="50%"><input name="seatchart" type="file" /></td>
                </tr>
                <tr>
                    <td  width="50%"><?php echo JText::_( 'COM_TICKETMASTEREXT_MULTISEAT' ); ?></td>
                    <td  width="50%"><?php echo $this->lists[multi_seat]; ?>      
                    </td>
                </tr> 
            </table>

            <div class="alert alert-success" style="font-size:125%;">
              <strong><?php echo JText::_( 'COM_TICKETMASTEREXT_FYI' ); ?></strong><br/>
			  <?php echo JText::_( 'COM_TICKETMASTEREXT_FYI_MULTISEAT_INFO' ); ?>
            </div>
                    
        <?php } ?>
        
        <table class="table table-striped">
            <tr>
                <td width="50%"><?php echo JText::_( 'COM_TICKETMASTEREXT_SEATCOLOR' ); ?></label></td>
                <td width="50%"><input class="text_area" type="text" name="background_color" id="background_color" size="6" maxlength="8" 
                        value="<?php echo $this->data->background_color; ?>" />
                </td>
            </tr>
            <tr>
                <td><?php echo JText::_( 'COM_TICKETMASTEREXT_BORDERCOLOR' ); ?></label></td>
                <td><input class="text_area" type="text" name="border_color" id="border_color" size="6" maxlength="8" 
                        value="<?php echo $this->data->border_color; ?>" />
                </td>
            </tr>
            <tr>
                <td><?php echo JText::_( 'COM_TICKETMASTEREXT_FONTCOLOR' ); ?></label></td>
                <td><input class="text_area" type="text" name="font_color" id="font_color" size="6" maxlength="8" 
                        value="<?php echo $this->data->font_color; ?>" />
                </td>
            </tr>          
            <tr>
                <td><?php echo JText::_( 'COM_TICKETMASTEREXT_SEATWIDTH' ); ?></label></td>
                <td><input class="text_area" type="text" name="seat_width" id="seat_width" size="6" maxlength="3" 
                        value="<?php echo $this->data->seat_width; ?>" />
                </td>
            </tr>
            <tr>
                <td><?php echo JText::_( 'COM_TICKETMASTEREXT_SEATHEIGTH' ); ?></label></td>
                <td><input class="text_area" type="text" name="seat_heigth" id="seat_heigth" size="6" maxlength="3" 
                        value="<?php echo $this->data->seat_heigth; ?>" />
                </td>
            </tr>                         
            <tr>
                <td><?php echo JText::_( 'COM_TICKETMASTEREXT_TYPE' ); ?></td>
                <td colspan="2"><?php echo $this->lists[type]; ?>      
                </td>
            </tr>  
        </table>        
                
  </div>
  <div class="span6">...</div>
</div>

<input type="hidden" name="ticketid" value="<?php echo $this->data->ticketid; ?>" />
<input type="hidden" name="id" value="<?php echo $this->data->id; ?>" />
<input type="hidden" name="option" value="com_ticketmasterext" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="tickets" />
</form>
