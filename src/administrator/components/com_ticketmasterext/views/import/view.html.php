<?php
/****************************************************************
 * @version			Ticketmaster Pro 1.0.1							
 * @package			Ticketmaster									
 * @copyright		Copyright © 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

## no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view' );

class TicketmasterExtViewImport extends JViewLegacy {
	

	function display($tpl = null) {

		## If we want the add/edit form..
		if($this->getLayout() == 'form') {
			$this->_displayForm($tpl);
			return;
		}
		
		$mainframe 	= JFactory::getApplication();
		$db    		= JFactory::getDBO();	
		
		$filter_order = $mainframe->getUserStateFromRequest( 'filter_order', 'filter_order', 'ordering', 'cmd' ); 

		## table ordering
		$lists['order']     = $filter_order;		
		
		## Model is defined in the controller
		$model		= $this->getModel('tickets');
		
		## Getting the items into a variable
		$items		= $this->get('list');
		$pagination = $this->get( 'pagination' );

		$db    = JFactory::getDBO();	
		
		## Making the query for getting the config from the ticketmaster tables.
		$sql='SELECT  * FROM #__ticketmaster_config WHERE configid = 1'; 
	 
		$db->setQuery($sql);
		$config = $db->loadObject();		

		$filter_order_Dir = $mainframe->getUserStateFromRequest( 'filter_order_Dir', 'filter_order_Dir', 'asc', 'word' );

		$multi = array(
			'0' => array('value' => '0', 'text' => JText::_( 'COM_TICKETMASTEREXT_NO' )),
			'1' => array('value' => '1', 'text' => JText::_( 'COM_TICKETMASTEREXT_YES' )),
		);	
		$lists['multi'] = JHTML::_('select.genericList', $multi, 'multi', ' class="input-mini" ', 'value', 'text', 0 );				
		
		## Original Ticket: 
		$query = "SELECT ticketid AS eventid, ticketname AS name 
				  FROM #__ticketmaster_tickets 
				  WHERE parent = 0 
				  ORDER BY ticketid ASC"; 
				  
		$db->setQuery($query);
		
		## Copy From Event:
		$eventlist[]	  = JHTML::_('select.option',  '0', JText::_( 'COM_TICKETMASTEREXT_PLS_SELECT' ), 'eventid', 'name' );
		$eventlist	      = array_merge( $eventlist, $db->loadObjectList() );
		$lists['eventid'] = JHTML::_('select.genericlist',  $eventlist, 'eventid', 'class="input" size="1" ','eventid', 'name');
		
		## Destination Event:
		$dest_list[]	  = JHTML::_('select.option',  '0', JText::_( 'COM_TICKETMASTEREXT_PLS_SELECT' ), 'eventid', 'name' );
		$dest_list	      = array_merge( $dest_list, $db->loadObjectList() );
		$lists['dest_list'] = JHTML::_('select.genericlist',  $dest_list, 'dest_list', 'class="input" size="1" ','eventid', 'name');		
		
		## AJAX dropdown with tickets from original ticket:		
		$ticketlist[]	   = JHTML::_('select.option',  '0', JText::_( 'COM_TICKETMASTEREXT_PLS_SELECT' ), 'ticketid', 'ticketname' );
		$lists['ticketid'] = JHTML::_('select.genericlist',  $ticketlist, 'ticketid', 'class="input" size="1" ','ticketid', 'ticketname');

		## AJAX dropdown with tickets to destination ticket:		
		$lists['dest_ticketid'] = JHTML::_('select.genericlist',  $ticketlist, 'dest_ticket', 'class="input" size="1" ','ticketid', 'ticketname');

		$this->assignRef('items', $items);
		$this->assignRef('pagination', $pagination);
		$this->assignRef('config', $config);
		$this->assignRef('lists', $lists);
		parent::display($tpl);		

	}
}
?>