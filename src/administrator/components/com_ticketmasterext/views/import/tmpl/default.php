<?php
/****************************************************************
 * @version			Ticketmaster Pro 1.0.1							
 * @package			Ticketmaster									
 * @copyright		Copyright © 2013 - All rights reserved.			
 * @license			GNU/GPL											
 * @author			Robert Dam										
 * @author mail		info@rd-media.org								
 * @website			http://www.rd-media.org							
 ***************************************************************/

## no direct access
defined('_JEXEC') or die('Restricted access');

## Add the CSS file for extra toolbars.
$document = & JFactory::getDocument();
$document->addStyleSheet('components/com_ticketmasterext/assets/css/ticketmasterext.css');

## Add the JQuery Libs to ensure AJAX parts!
$document->addScript('http://code.jquery.com/jquery-latest.js');

## Setup the toolbars.
JToolBarHelper::title( JText::_( 'COM_TICKETMASTEREXT_TICKETS_COPYSEATS' ), 'generic.png' );
JToolBarHelper::save();

## Require specific menu-file.
$path = JPATH_COMPONENT.DS.'assets'.DS.'menu.php';
if (file_exists($path)) {
	include_once $path;
}

## Check if this is Joomla 2.5 or 3.0.+
$isJ30 = version_compare(JVERSION, '3.0.0', 'ge');

if(!$isJ30) {
	## Adding mootools for J!2.5
	JHTML::_('behavior.modal');
	## Loading the mootools drivers.
	JHTML::_('behavior.mootools');	
	## Include the tooltip behaviour.
	JHTML::_('behavior.tooltip', '.hasTip');
	$document->addStyleSheet( JURI::root(true).'/administrator/components/com_ticketmasterext/assets/bootstrap/css/bootstrap.css' ); 
	$document->addScript( JURI::root(true).'/administrator/components/com_ticketmasterext/assets/bootstrap/js/bootstrap.js');
}

?>

<script language="javascript" type="text/javascript">

var JQ = jQuery.noConflict();

JQ(document).ready(function(){

	JQ("#eventid").change(function(){

		var id = jQuery("select#eventid").val();
	
		jQuery.ajax({
			type: "GET",
			url: "index.php",
			data: "option=com_ticketmasterext&controller=import&task=getTickets&format=raw&id="+id,
			success: function(data){
				jQuery("#ticketsfrom").html(data);
			}
		});

	});
	
	JQ("#dest_list").change(function(){

		var id = jQuery("select#dest_list").val();
	
		jQuery.ajax({
			type: "GET",
			url: "index.php",
			data: "option=com_ticketmasterext&controller=import&task=getDestinationTickets&format=raw&id="+id,
			success: function(data){
				jQuery("#ticketsto").html(data);
			}
		});

	});	
	
});

</script>

<div style="margin-right:10px; margin-bottom:10px; padding-bottom:15px;">
  <a class="btn btn-mini pull-right" target="_blank"
  	href="http://rd-media.org/support/knowledgebase/view-article/50-transferring-seats-from-event-to-event.html">
	  <?php echo JText::_( 'COM_TICKETMASTEREXT_TRANSFER_LINK'); ?></a>  
</div>

<div style="margin:10px; padding-top:10px;">

	<div class="alert alert-error" id="error" style="display: none;"></div>

    <form action = "index.php" method="POST" name="adminForm" id="adminForm" class="form-inline">
    
    <table class="table table-striped">
        
        <th width="20%"><?php echo JText::_( 'COM_TICKETMASTEREXT_TRANSFER_FROM_EVENT'); ?></th>
        <th width="20%"><?php echo JText::_( 'COM_TICKETMASTEREXT_TRANSFER_FROM_TICKETID'); ?></th>
        <th width="20%"><div align="center"><?php echo JText::_( 'COM_TICKETMASTEREXT_MULTI_TICKET'); ?></div></th> 
        <th width="20%"><div align="right"><?php echo JText::_( 'COM_TICKETMASTEREXT_TRANSFER_TO_EVENTID'); ?></div></th>
        <th width="20%"><div align="right"><?php echo JText::_( 'COM_TICKETMASTEREXT_TRANSFER_TO_TICKETID'); ?></div></th> 
        
        
        <tr>
        	<td width="20%"><div align="left"><?php echo $this->lists['eventid']; ?></div></td>
            <td width="20%"><div align="left" id="ticketsfrom"><?php echo $this->lists['ticketid']; ?></div></td>           
            <td width="20%"><div align="center"><?php echo $this->lists['multi']; ?></div></td>
            <td width="20%"><div align="right"><?php echo $this->lists['dest_list']; ?></div></td>
            <td width="20%"><div align="right" id="ticketsto"><?php echo $this->lists['dest_ticketid']; ?></div></td>
        </tr>     
           
    </table>	
      
      <input name = "option" type="hidden" value="com_ticketmasterext" />
      <input name = "task" type="hidden" value="" />
      <input name = "boxchecked" type="hidden" value="0"/>
      <input name = "controller" type="hidden" value="import"/>
      <input type="hidden" name="filter_order" value="ordering" />
      <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />  
      <?php echo JHTML::_( 'form.token' ); ?>  
      
    </form>

</div>
