<?php
/**
 * @version     1.0.0
 * @package     com_ticketmasterext
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      robert <info@rd-media.org> - https://rd-media.org
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Ticketmasterext helper.
 */
class TicketmasterextHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_TICKETMASTEREXT_TITLE_S'),
			'index.php?option=com_ticketmasterext&view=s',
			$vName == 's'
		);

	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_ticketmasterext';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
