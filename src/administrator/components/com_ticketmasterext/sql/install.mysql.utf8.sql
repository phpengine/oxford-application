CREATE TABLE IF NOT EXISTS `#__ticketmaster_coords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `x_pos` int(4) NOT NULL,
  `y_pos` int(4) NOT NULL,
  `ticketid` int(11) NOT NULL,
  `seatid` int(11) NOT NULL,
  `booked` tinyint(1) NOT NULL,
  `parent` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `width` int(11) NOT NULL,
  `heigth` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=422 ;


CREATE TABLE IF NOT EXISTS `#__ticketmaster_tickets_ext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticketid` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `seat_width` int(11) NOT NULL,
  `seat_heigth` int(11) NOT NULL,
  `seat_chart` varchar(150) NOT NULL,
  `background_color` varchar(8) NOT NULL,
  `multi_seat` tinyint(1) NOT NULL,
  `border_color` varchar(6) NOT NULL,
  `font_color` varchar(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

