<?php
/**
 * @version     1.0.2
 * @package     com_ticketmasterext
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      robert <info@rd-media.org> - https://rd-media.org
 */


## no direct access
defined('_JEXEC') or die;

## For Joomla! 3.0
if(!defined('DS')){
    define('DS',DIRECTORY_SEPARATOR);
}

## Check for PHP4
if(defined('PHP_VERSION')) {
	$version = PHP_VERSION;
} elseif(function_exists('phpversion')) {
	$version = phpversion();
} else {
	## No version info. I'll lie and hope for the best.
	$version = '5.0.0';
}

## Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_ticketmasterext')) 
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

## Require the base controller
require_once (JPATH_COMPONENT.DS.'controller.php');

## Require specific controller if requested.
if($controller = JRequest::getWord('controller', 'tickets')) {
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if (file_exists($path)) {
		require_once $path;
	} else {
		$controller = '';
	}
}

## Create the controller
$classname	= 'TicketmasterExtController'.ucfirst($controller);
$controller	= new $classname( );

## Perform the Request task, display will be loaded automatically.
$controller->execute( JRequest::getCmd( 'task' ) );

$controller->redirect();
