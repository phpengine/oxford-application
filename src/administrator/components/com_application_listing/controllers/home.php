<?php
    //initialize page
    $content = array();

    $gctype = (isset($_REQUEST["gctype"])) ? $_REQUEST["gctype"] : 'pending' ;

    switch ($gctype) {
      case 'pending':
        $content["all_apps"] = $this->model->getAllPendingApplications();
        $content["cur_app_state"] = "Pending";
        break;
      case 'review':
        $content["all_apps"] = $this->model->getAllUnderReviewApplications();
        $content["cur_app_state"] = "Under Review";
        break;
      case 'accepted':
        $content["all_apps"] = $this->model->getAllAcceptedApplications();
        $content["cur_app_state"] = "Accepted";
        break;
      case 'rejected':
        $content["all_apps"] = $this->model->getAllRejectedApplications();
        $content["cur_app_state"] = "Rejected";
        break;
    }

    $this->view->pageHome($content);