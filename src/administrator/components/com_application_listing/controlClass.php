<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'modelClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'viewClass.php';

class OUDCEApplicationBEControlClass {
	
	private $configs;
	private $view;
	private $model;
	private $messages;

	public function __construct(){
		$this->configs = new OUDCEApplicationConfigClass();
		$this->view = new OUDCEApplicationBEViewClass();
		$this->model = new OUDCEApplicationModelClass();
		$this->messages = array(); }

	public function pageHome() {
		include('controllers/home.php'); }
	
}
