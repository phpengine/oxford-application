<?php
/**
 * @version     1.0.0
 * @package     com_application_listing
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Big Boss <bigboss@golden-contact-computing.co.uk> - http://www.golden-contact-computing.co.uk
 */


// no direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_application_listing')) 
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

require_once JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'controlClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'configClass.php';

$control = new OUDCEApplicationBEControlClass();
$configs = new OUDCEApplicationConfigClass();

JToolbarHelper::title($configs->give("title_main"),'title');

$task = (isset($_REQUEST["task"])) ? $_REQUEST["task"] : 'home' ;
switch ($task) {
  case 'home':
    $control->pageHome();
    break;
}