<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * mod_gcarticlecomments is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

class OUDCEApplicationBEViewClass {
	
	private static $config;

	public function __construct(){
		$this->configs = new OUDCEApplicationConfigClass();
	}

	public function pageHome($pageVars) {
    ob_start();
		include('views/home.php');
    $htmlvar = ob_get_clean();
		echo $htmlvar; }

	/* HELPERS */
	private function openPage(){
		JHTML::stylesheet('template.css', "administrator/components/com_application_listing/css/");
		echo '<h1>'.$this->configs->give("title_main").'</h1>';
	}
	
}