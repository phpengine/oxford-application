<?php

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'viewClass.php';

class OUDCEApplicationModelClass {

  private $configs;
  private $db;

	public function __construct(){
		$this->configs = new OUDCEApplicationConfigClass();
    $this->db = JFactory::getDBO();
	}

  public function getAllPendingApplications() {
    $all_my_course_apps_query = 'SELECT id FROM #__content WHERE catid="18" AND state="1"';
    $all_my_course_app_ids = \JCckDatabase::loadResultArray($all_my_course_apps_query);
    $all_apps = array();
    foreach ($all_my_course_app_ids as $one_course_app_id) {
      $one_course_app = $ticketRow = \JCckContentArticle::getRow($one_course_app_id, "");
      if ($one_course_app->application_status=="Pending") {
        $all_apps[] = $one_course_app; } }
    return $all_apps;
  }

  public function getAllUnderReviewApplications() {
    $all_my_course_apps_query = 'SELECT id FROM #__content WHERE catid="18" AND state="1"';
    $all_my_course_app_ids = \JCckDatabase::loadResultArray($all_my_course_apps_query);
    $all_apps = array();
    foreach ($all_my_course_app_ids as $one_course_app_id) {
      $one_course_app = $ticketRow = \JCckContentArticle::getRow($one_course_app_id, "");
      if ($one_course_app->application_status=="Under Review") {
        $all_apps[] = $one_course_app; } }
    return $all_apps;
  }

  public function getAllAcceptedApplications() {
    $all_my_course_apps_query = 'SELECT id FROM #__content WHERE catid="18" AND state="1"';
    $all_my_course_app_ids = \JCckDatabase::loadResultArray($all_my_course_apps_query);
    $all_apps = array();
    foreach ($all_my_course_app_ids as $one_course_app_id) {
      $one_course_app = $ticketRow = \JCckContentArticle::getRow($one_course_app_id, "");
      if ($one_course_app->application_status=="Accepted") {
        $all_apps[] = $one_course_app; } }
    return $all_apps;
  }

  public function getAllRejectedApplications() {
    $all_my_course_apps_query = 'SELECT id FROM #__content WHERE catid="18" AND state="1"';
    $all_my_course_app_ids = \JCckDatabase::loadResultArray($all_my_course_apps_query);
    $all_apps = array();
    foreach ($all_my_course_app_ids as $one_course_app_id) {
      $one_course_app = $ticketRow = \JCckContentArticle::getRow($one_course_app_id, "");
      if ($one_course_app->application_status=="Rejected") {
        $all_apps[] = $one_course_app; } }
    return $all_apps;
  }

}
