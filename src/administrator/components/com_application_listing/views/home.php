<?php
    defined('_JEXEC') or die;
		$this->openPage();
?>

<h2> <?= $this->configs->give("subtitle_home") ?> </h2>
<p> <?= $this->configs->give("home_intro_text") ?> </p>
<br />
<ul class="oudce_apps_navigation">
  <li class="">
    <a href="index.php?option=com_application_listing&gctype=pending">Pending Applications</a>
  </li>
  <li class="">
    <a href="index.php?option=com_application_listing&gctype=review">Under Review Applications</a>
  </li>
  <li class="">
    <a href="index.php?option=com_application_listing&gctype=accepted">Accepted Applications</a>
  </li>
  <li class="">
    <a href="index.php?option=com_application_listing&gctype=rejected">Rejected Applications</a>
  </li>
</ul>
<br />
<p> Below is a list of OUDCE Applications with the status:
  <span class="app_state"> <?= $pageVars["cur_app_state"] ?></span>
</p>

<form action="index.php" method="POST">

  <div><?php

    $app_counter = 1;
    if (count($pageVars["all_apps"])>0) {

      foreach($pageVars["all_apps"] as $one_app) {

        $courseRow = \JCckContentArticle::getRow($one_app->course_selector, "");

        ?>

        <div class="one_app_in_list">
          <h2> Applicant: <?php echo $one_app->your_full_name; ?> </h2>
          <?php
          if (isset($courseRow)) {
            ?>
            <h4> For Course: <?php echo $courseRow->title; ?> </h4>
          <?php
          } else {
            ?>
            <h4> For Course: NONE SELECTED </h4>
          <?php
          }
          ?>
          <p> This application currently has the status: <?php echo $one_app->application_status ; ?> </p>

          <?php
          if (in_array($one_app->application_status, array("", "Pending", "Under Review", "Accepted", "Rejected")) ) {
            echo '<p> <a target="_blank" href="index.php?option=com_cck&view=form&return_o=content&type=event_instance&id='.$one_app->id.'">Click here to edit this Application</a> </p>'; }
          ?>

        </div>

        <?php
        $app_counter++ ;
      }
    }
    else {
       echo '<p> There are currently no applications in this state. </p>';
    }

    ?></div>

</form>