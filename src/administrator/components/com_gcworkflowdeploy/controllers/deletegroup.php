<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validation
		// Check the stage
		if($_REQUEST["stage"]==1) {
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ; }
		    $content["groupid"] = $_REQUEST["groupid"];
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $content["group"] = $this->model->getSingleGroupDetails($content["groupid"]);
        $this->view->pageDeleteGroup($content); }
    elseif($_REQUEST["stage"]==2) {
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ; }
		    $content["groupid"] = $_REQUEST["groupid"];
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $content["group"] = $this->model->getSingleGroupDetails($content["groupid"]);
        $this->view->pageDeleteGroup($content); }
    elseif($_REQUEST["stage"]==3) {
		    $content = array();
		    $content["groupid"] = $_REQUEST["groupid"];
		    $content["group"] = $this->model->getSingleGroupDetails($content["groupid"]);
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $this->model->deleteGroup($content["groupid"]);
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ; }
        $this->view->pageDeleteGroup($content); }
    elseif($_REQUEST["stage"]==4) {
			$this->pageHome();
		} else { // no stage page accessed in error
			$this->messages[] = "Delete Page accessed in error";
			$this->pageHome();
		}
	} else {
		$this->messages[] = "Delete Page accessed in error";
		$this->pageHome();
	}
