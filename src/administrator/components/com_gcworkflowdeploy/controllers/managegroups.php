<?php
  if (isset($_REQUEST["run"]) ) {
    if ( !isset($_REQUEST["feature-groupid"]) ) {
      $this->messages[] = "You need to Choose a Group"; }
    unset($_REQUEST["run"]); }
  else {
    $content = array();
    if ( count($this->messages)>0 ) {
      $content["messages"] = $this->messages ; }
    $content["groups"] = $this->model->getAllGroups();
    $this->view->pageManageGroups($content); }