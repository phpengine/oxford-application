<?php
  $content = array();
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		if ( isset($_REQUEST["groupid"]) && $_REQUEST["groupid"]=="new" ) {
      $_REQUEST["groupid"] = $this->model->setNewGroupID();
      $this->messages[] = "Group Addition Successful" ; }
    else {
      $this->messages[] = "Group Change Successful" ; }
      $this->model->setUpdateGroupDetail($_REQUEST["groupid"], 'group_name', $_REQUEST["group_name"]);
      $this->model->setUpdateGroupDetail($_REQUEST["groupid"], 'group_desc', $_REQUEST["group_desc"]);
      $this->pageEditGroupEntries(); }
  else {
    if ( count($this->messages)>0 ) {
      $content["messages"] = $this->messages ;  }
    if ( isset($_REQUEST["groupid"]) && is_numeric($_REQUEST["groupid"]) ) {
      $content["edit"] = 1;
      $content["groupdetails"] = $this->model->getSingleGroupDetails($_REQUEST["groupid"]); }
    else {
      $content["edit"] = 0; }
      $this->view->pageEditGroups($content); }