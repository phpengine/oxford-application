<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validation
		$validation = 0;
		if ( isset($_REQUEST["serverid"]) && $_REQUEST["serverid"]=="new" ) {
		    $content;
		    $content = array();
			$_REQUEST["serverid"] = $this->model->setNewServerID();
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_title', $_REQUEST["serv_title"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_desc', $_REQUEST["serv_desc"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_website_url', $_REQUEST["serv_website_url"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_webservice_url', $_REQUEST["serv_webservice_url"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_key', $_REQUEST["serv_key"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_upload_type', $_REQUEST["serv_upload_type"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_user', $_REQUEST["serv_ssh_user"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_pass', $_REQUEST["serv_ssh_pass"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_folder', $_REQUEST["serv_ssh_folder"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_user', $_REQUEST["serv_ssh_user"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_pass', $_REQUEST["serv_ssh_pass"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_folder', $_REQUEST["serv_ssh_folder"]);		    
	    	$this->messages[] = "Server Addition Successful" ;
	    	$this->pageEditServers();
		} else {		    
		    $content;
		    $content = array();
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_title', $_REQUEST["serv_title"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_desc', $_REQUEST["serv_desc"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_website_url', $_REQUEST["serv_website_url"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_webservice_url', $_REQUEST["serv_webservice_url"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_key', $_REQUEST["serv_key"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_upload_type', $_REQUEST["serv_upload_type"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_user', $_REQUEST["serv_ssh_user"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_pass', $_REQUEST["serv_ssh_pass"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_folder', $_REQUEST["serv_ssh_folder"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_user', $_REQUEST["serv_ssh_user"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_pass', $_REQUEST["serv_ssh_pass"]);
		    $this->model->setUpdateServerDetail($_REQUEST["serverid"], 'serv_ssh_folder', $_REQUEST["serv_ssh_folder"]);	
	    	$this->messages[] = "Server Change Successful" ;
	    	$this->pageEditServers();
		}
	} else {
		// if its not
	    // initialize page
	    $content;
	    $content = array();
	    if ( count($this->messages)>0 ) {
	    	$content["messages"] = $this->messages ;
	    }
	    if ( isset($_REQUEST["serverid"]) && is_numeric($_REQUEST["serverid"]) ) {
	    	$content["edit"] = 1;
	    	$content["serverdetails"] = $this->model->getSingleServerDetails($_REQUEST["serverid"]);
	    } else {
	    	$content["edit"] = 0;
	    }
        $this->view->pageEditServers($content);
	}
