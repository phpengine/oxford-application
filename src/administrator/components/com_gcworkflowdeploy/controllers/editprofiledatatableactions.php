<?php

	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
    if ( $_REQUEST["nochanges"] != "1" ) {
        $tablestodo = $this->model->getDBTablesToAction($_REQUEST["profileuniqueid"]);
        $itest = 0;
        foreach ($tablestodo as $table) {
            $tableActionData = array();
            foreach ($_REQUEST as $requestKey => $requestValue ) {
                $tNameLen = strlen($table["profile_datatable_tablename"]);
                $reqTName = substr($requestKey, 0, $tNameLen);
                $reqRemainder = substr($requestKey, $tNameLen+1) ;
                $reqOrderLength = strpos($reqRemainder, "_") ;
                $reqOrder = substr($reqRemainder, 0, $reqOrderLength) ;
                if ($reqTName."_".$reqOrder == $table["profile_datatable_tablename"].'_'.$table["profile_datatable_ordering"]) {
                    $tableActionData[] = array($requestKey => $requestValue); } }
            $this->model->setProfileDataTableAction(
                $_REQUEST["profileuniqueid"],
                $table["profile_datatable_tablename"],
                $_REQUEST['taction_'.$table["profile_datatable_tablename"].'_'.$table["profile_datatable_ordering"]],
                $table["profile_datatable_ordering"],
                serialize($tableActionData ) ) ;} }
		$this->pageEditProfileConfirm();
	} else {
	    $content = array();
	    if ( count($this->messages)>0 ) {
	    	$content["messages"] = $this->messages ; }
        $doc = JFactory::getDocument();
        $doc->addScript("/administrator/components/com_gcworkflowdeploy/scripts/jquery.js");
        $doc->addScript("/administrator/components/com_gcworkflowdeploy/scripts/gcjssetup.js");
        $doc->addScript("/administrator/components/com_gcworkflowdeploy/scripts/dbactionchooser.js");
        $content["profiledetails"] = $this->model->getSingleProfileDetails($_REQUEST["profileid"]);
        $content["datatablestoaction"] = $this->model->getDBTablesToAction($_REQUEST["profileuniqueid"]);
        if (isset($content["profiledetails"]["profile_datatable_action_details"])) {
            $content["current_profile_datatable_action_details"] = unserialize($content["profiledetails"]["profile_datatable_action_details"]); }
		    $content["availableactions"] = $this->model->getAvailableDBActions();
        $this->view->pageEditProfileDataTableActions($content);
	}
