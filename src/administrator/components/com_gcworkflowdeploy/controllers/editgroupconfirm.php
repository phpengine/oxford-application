<?php
	    //initialize page
		if (isset($_REQUEST["run"]) ) {
			unset($_REQUEST["run"]);
			$this->pageHome();
		} else {
      $content = array();
      if ( count($this->messages)>0 ) {
        $content["messages"] = $this->messages ; }
      $content["groupdetails"] = $this->model->getSingleGroupDetailsFromUnique( $_REQUEST["groupuniqueid"] );
      $content["groupentries"] = $this->model->getSingleGroupEntriesFromUnique( $_REQUEST["groupuniqueid"] );
      $this->view->pageEditGroupConfirm($content);
		}
