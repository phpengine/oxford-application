<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validation
		// Check the stage
		if($_REQUEST["stage"]==1) {		    
		    $content;
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ;
		    }
		    $content["serverid"] = $_REQUEST["serverid"];
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $content["server"] = $this->model->getSingleServerDetails($content["serverid"]);
        	$this->view->pageDeleteServer($content);
		} elseif($_REQUEST["stage"]==2) {		    
		    $content;
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ;
		    }
		    $content["serverid"] = $_REQUEST["serverid"];
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $content["server"] = $this->model->getSingleServerDetails($content["serverid"]);
        	$this->view->pageDeleteServer($content);
		}  elseif($_REQUEST["stage"]==3) {		    
		    $content;
		    $content = array();
		    $content["serverid"] = $_REQUEST["serverid"];
		    $content["server"] = $this->model->getSingleServerDetails($content["serverid"]);
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $this->model->deleteServer($content["serverid"]);
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ;
		    }
        	$this->view->pageDeleteServer($content);
		}  elseif($_REQUEST["stage"]==4) {	
			$this->pageHome();
		} else { // no stage page accessed in error
			$this->messages[] = "Delete Page accessed in error";
			$this->pageHome();
		}
	} else {
 			// no run page accessed in error
			$this->messages[] = "Delete Page accessed in error";
			$this->pageHome();
	}
