<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validation
		$validation = 0;
        $dtToAdd = array();
        $allDataTables = $this->model->getAllDBTables();
        foreach ($allDataTables as $oneOfAllDataTables) {
            if (isset($_REQUEST["table_".$oneOfAllDataTables]) && $_REQUEST["table_".$oneOfAllDataTables]>0) {
                for ($idt=1; $idt<=$_REQUEST["table_".$oneOfAllDataTables]; $idt++) {
                    $dtToAdd[] = array($oneOfAllDataTables, $idt); } } }
	    $this->model->setProfileDataTables($_REQUEST["profileuniqueid"], $dtToAdd) ;
		$this->pageEditProfileDataTableActions(); }
    else {
	    $content = array();
	    if ( count($this->messages)>0 ) {
	    	$content["messages"] = $this->messages ; }
        $content["profiledetails"] = $this->model->getSingleProfileDetails($_REQUEST["profileid"]);
        $content["datatables"] = $this->model->getAllDBTables();
        $content["datatablescurrent"] = $this->model->getDBTablesToAction($_REQUEST["profileuniqueid"]);
        $content["datatablescurrentonlynames"] = array();
        foreach ($content["datatablescurrent"] as $datatable) {
          $content["datatablescurrentonlynames"][] = $datatable["profile_datatable_tablename"]; }
        $content["tablecount"] = count($content["datatables"]);
        $this->view->pageEditProfileDataTables($content);
	}