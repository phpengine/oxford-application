<?php
	    //initialize page
	    $content;
	    $content = array();
	    $content["keys"] = $this->model->getAllKeysDetails();
      $content["servers"] = $this->model->getAllServersDetails();
      $content["profiles"] = $this->model->getAllProfiles();
      $content["groups"] = $this->model->getAllGroups();
      $this->view->pageHome($content);