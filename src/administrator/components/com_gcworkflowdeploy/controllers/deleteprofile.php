<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validation
		// Check the stage
		if($_REQUEST["stage"]==1) {
		    $content = array();
		    if ( count($this->messages)>0 ) { $content["messages"] = $this->messages ; }
		    $content["profileid"] = $_REQUEST["profileid"];
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $content["profile"] = $this->model->getSingleProfileDetails($content["profileid"]);
		    $content["profile"]["datatables"] = $this->model->getDBTablesToAction($content["profile"]["profile_uniqueid"]);
        $this->view->pageDeleteProfile($content); }
    elseif($_REQUEST["stage"]==2) {
        $content = array();
        if ( count($this->messages)>0 ) { $content["messages"] = $this->messages ; }
		    $content["profileid"] = $_REQUEST["profileid"];
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $content["profile"] = $this->model->getSingleProfileDetails($content["profileid"]);
		    $content["profile"]["datatables"] = $this->model->getDBTablesToAction($content["profile"]["profile_uniqueid"]);
		    $this->view->pageDeleteProfile($content); }
    elseif($_REQUEST["stage"]==3) {
		    $content = array();
		    $content["profileid"] = $_REQUEST["profileid"];
		    $content["profile"] = $this->model->getSingleProfileDetails($content["profileid"]);
		    $content["profile"]["datatables"] = $this->model->getDBTablesToAction($content["profile"]["profile_uniqueid"]);
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $this->model->deleteSingleProfile($content["profileid"]);
		    if ( count($this->messages)>0 ) { $content["messages"] = $this->messages ; }
        $this->view->pageDeleteProfile($content); }
    else if ($_REQUEST["stage"]==4) {
			$this->pageHome(); }
    else { // no stage page accessed in error
			$this->messages[] = "Delete Page accessed in error";
			$this->pageHome(); } }
  else {
 			// no run page accessed in error
			$this->messages[] = "Delete Page accessed in error";
			$this->pageHome(); }
