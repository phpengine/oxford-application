<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

$fservice = new GCWorkflowDeployerGroupEntryClass();
$fservice->executeme();

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'modelClass.php';

class GCWorkflowDeployerGroupEntryClass {
	
	private static $configs ;
  private $model ;
	private $actionreq ;
  private $groupunique ;
  private $entry ;
  private $entryType ;
  private $ordering ;

	public function __construct() {
		$this->configs = new GCWorkflowDeployerConfigClass();
		$this->model = new GCWorkflowDeployerModelClass();
		if (isset($_REQUEST["actionreq"])) {$this->actionreq = $_REQUEST["actionreq"];}
    if (isset($_REQUEST["groupunique"])) {$this->groupunique = $_REQUEST["groupunique"]; }
    if (isset($_REQUEST["entry"])) {$this->entry = $_REQUEST["entry"]; }
    if (isset($_REQUEST["entryType"])) {$this->entryType = $_REQUEST["entryType"]; }
    if (isset($_REQUEST["ordering"])) {$this->ordering = $_REQUEST["ordering"]; }
	}
	
	public function executeme() {
    if ($this->actionreq == "addentry") {
      $this->calculateOrdering();
			$this->addEntry($this->groupunique, $this->entry, $this->ordering);
			$this->displayAllEntries(); }
    else if ($this->actionreq == "delentry") {
			$this->deleteEntry($this->groupunique, $this->entry, $this->ordering);
			$this->displayAllEntries(); }
    else if ($this->actionreq == "curentries") {
			$this->displayAllEntries(); }
	}


	/*
	 *  For DB Entry
	 */
	private function addEntry() {
		$this->model->addGroupEntry($this->groupunique, $this->entryType, $this->entry, $this->ordering);
	}

  private function deleteEntry() {
    $this->model->deleteGroupEntry($this->groupunique, $this->entry, $this->ordering);
  }

  private function calculateOrdering() {
    $this->ordering = $this->model->getSingleGroupNextEntryOrderFromUnique($this->groupunique) ;
  }
	
	private function displayAllEntries() {
		$currententries = $this->aasort($this->model->getSingleGroupEntriesFromUnique($this->groupunique), "ordering");
		$htmlvar = '<div class="result">';
		$htmlvar .= '   <h3>Current Entries poo:</h3>';
    foreach ($currententries as $currententry ) {
      $htmlvar .= '<div class="entryrow"><div class="left"><input type="button" class="gcbuttonsml" ';
      $htmlvar .= 'value="X" onclick=\'deleteThisEntry("'.$currententry["ordering"].'", "'.$currententry["entry_type"] ;
      $htmlvar .= '", "'.$currententry["target"].'");\' /></div>';
      $htmlvar .= '<div class="right"><p>'.$currententry["ordering"].' - '.$currententry["entry_type"] ;
      $htmlvar .= ' '.$currententry["target"].'</p></div></div>'; }
		$htmlvar .= '</div> ';
		echo $htmlvar;
	}

  /*
   * Helpers
   */
  private function aasort (&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
      $sorter[$ii]=$va[$key]; }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
      $ret[$ii]=$array[$ii]; }
    $array=$ret;
    return $array;
  }

}
