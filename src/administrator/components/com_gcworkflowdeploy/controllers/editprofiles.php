<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validation
		$validation = 0;
		if ( isset($_REQUEST["profileid"]) && $_REQUEST["profileid"]=="new" ) {
		    $content;
		    $content = array();
			  $_REQUEST["profileid"] = $this->model->setNewProfileID();
		    $this->model->setUpdateProfileDetail($_REQUEST["profileid"], 'profile_title', $_REQUEST["profile_title"]);
		    $this->model->setUpdateProfileDetail($_REQUEST["profileid"], 'profile_description', $_REQUEST["profile_description"]);
	    	$this->messages[] = "Profile Addition Successful" ;
	    	$this->pageEditProfileFiles();
		} else {		    
		    $content;
		    $content = array();
		    $this->model->setUpdateProfileDetail($_REQUEST["profileid"], 'profile_title', $_REQUEST["profile_title"]);
		    $this->model->setUpdateProfileDetail($_REQUEST["profileid"], 'profile_description', $_REQUEST["profile_description"]);
	    	$this->messages[] = "Profile Change Successful" ;
	    	$this->pageEditProfileFiles();
		}
	} else {
		// if its not
	    // initialize page
	    $content;
	    $content = array();
	    if ( count($this->messages)>0 ) {
	    	$content["messages"] = $this->messages ;
	    }
	    if ( isset($_REQUEST["profileid"]) && is_numeric($_REQUEST["profileid"]) ) {
	    	$content["edit"] = 1;
	    	$content["profiledetails"] = $this->model->getSingleProfileDetails($_REQUEST["profileid"]);
	    } else {
	    	$content["edit"] = 0;
	    }
        $this->view->pageEditProfiles($content);
	}
