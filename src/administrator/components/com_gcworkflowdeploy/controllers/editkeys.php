<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validationecho "keyid:".$_REQUEST["keyid"];
		$validation = 0;
		if ( isset($_REQUEST["keyid"]) && $_REQUEST["keyid"]=="new" ) {
			$_REQUEST["keyid"] = $this->model->setNewKeyID();
		    $this->model->setUpdateKeyDetail($_REQUEST["keyid"], 'key_title', $_REQUEST["key_title"]);
		    $this->model->setUpdateKeyDetail($_REQUEST["keyid"], 'key_key', $_REQUEST["key_key"]);
		    $this->model->setUpdateKeyDetail($_REQUEST["keyid"], 'key_desc', $_REQUEST["key_desc"]);
		    $this->model->setUpdateKeyDetail($_REQUEST["keyid"], 'key_authlevel', $_REQUEST["key_authlevel"]);
	    	$this->messages[] = "New Key Addition Successful" ;
	    	$this->pageEditKeys();
		} else {		    
		    $content;
		    $content = array();
		    $this->model->setUpdateKeyDetail($_REQUEST["keyid"], 'key_title', $_REQUEST["key_title"]);
		    $this->model->setUpdateKeyDetail($_REQUEST["keyid"], 'key_key', $_REQUEST["key_key"]);
		    $this->model->setUpdateKeyDetail($_REQUEST["keyid"], 'key_desc', $_REQUEST["key_desc"]);
		    $this->model->setUpdateKeyDetail($_REQUEST["keyid"], 'key_authlevel', $_REQUEST["key_authlevel"]);
	    	$this->messages[] = "Key Change Successful" ;
	    	$this->pageEditKeys();
		}
	} else {
		// if its not
	    // initialize page
	    $content;
	    $content = array();
	    if ( count($this->messages)>0 ) {
	    	$content["messages"] = $this->messages ;
	    }
	    if ( isset($_REQUEST["keyid"]) && $_REQUEST["keyid"]=="new" ) {
	    	$content["edit"] = 0;
	    } else {
	    	$content["edit"] = 1;
	    	$content["keydetails"] = $this->model->getSingleKeyDetails($_REQUEST["keyid"]);
	    }
        $this->view->pageEditKeys($content);
	}
