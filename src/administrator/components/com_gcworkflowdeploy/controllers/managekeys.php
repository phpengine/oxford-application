<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validation				
		if ( !isset($_REQUEST["qs-typeid"]) ) {
		    $content["keys"] = $this->model->getAllKeysDetails();
	        $this->view->pageManageKeys($content);
		} else {
			$this->session->set( 'qs-typeid', $_REQUEST["qs-typeid"] );
			$this->pagepageManageKeys();
		}
	} else {
	    //initialize page
	    $content;
	    $content = array();
	    if ( count($this->messages)>0 ) {
	    	$content["messages"] = $this->messages ;
	    }
	    $content["keys"] = $this->model->getAllKeysDetails();
        $this->view->pageManageKeys($content);
	}