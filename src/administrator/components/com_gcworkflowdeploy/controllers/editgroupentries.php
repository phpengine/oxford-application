<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		$validation = 0;
    $this->messages[] = "Group Entry Changes Successful";
		$this->pageEditGroupConfirm(); }
  else {
		ini_set("max_execution_time", "360");
    $content = array();
    if ( count($this->messages)>0 ) {
      $content["messages"] = $this->messages ; }
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."groupEntryAvailableClass.php");
		$doc =& JFactory::getDocument();
		$doc->addScript("/administrator/components/com_gcworkflowdeploy/scripts/jquery.js");
		$doc->addScript("/administrator/components/com_gcworkflowdeploy/scripts/gcjssetup.js");
		$doc->addScript("/administrator/components/com_gcworkflowdeploy/scripts/groupentrychooser.js");
		$entryClass = new GCWorkflowDeployerGroupEntryAvailableClass();

    $content["entrygroups"] = $this->model->getAllOtherGroups($_REQUEST["groupid"]);
    $content["singleinstances"] = $entryClass->getAllInstances();
    $content["allprofiles"] = $entryClass->getAllAvailableProfiles();

    $content["group"] = $this->model->getSingleGroupDetails($_REQUEST["groupid"]);
    $content["groupdetails"] = $this->model->getSingleGroupDetailsFromUnique($content["group"]["uniqueid"]);
		$content["currententries"] = $this->model->getSingleGroupEntries($_REQUEST["groupid"]);
    $this->view->pageEditGroupEntries($content);
	}