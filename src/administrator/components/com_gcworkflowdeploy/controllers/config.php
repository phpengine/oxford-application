<?php
	    //initialize page
	    $content;
	    $content = array();
	    $uservar;
	    $uservar = array();

	    include ( JPATH_COMPONENT_ADMINISTRATOR.DS."configs".DS."userconfigs.php");
	    
	    for ($iuv=0;$iuv<count($userconfvar);$iuv++) {
			$uservar[$iuv] = $userconfvar[$iuv];
	    }

	    // update config object if the page is on a save function
	    if ( isset($_REQUEST["run"]) && $_REQUEST["run"] == "1" ) {
			// update config
			foreach ($uservar as $uvar) {
				$this->configs->setUserVar( $uvar["idString"], $_REQUEST["conf_".$uvar["idString"]]);
			}
	    }

	    // repopulate display array with values from object
	    for ($iuv=0;$iuv<count($uservar);$iuv++) {
			$uservar[$iuv]["curValue"] = $this->configs->give($uservar[$iuv]["idString"]);
	    }

	    $content["uservar"] = $uservar;
		$this->view->pageConfig($content);