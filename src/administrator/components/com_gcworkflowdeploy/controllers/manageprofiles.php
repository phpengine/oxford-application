<?php
			if (isset($_REQUEST["run"]) ) {
				// If its set to run do the validation				
				if ( !isset($_REQUEST["qs-servid"]) ) {
					$this->messages[] = "You need to Choose a Profile";
				}
				
				if (count($this->messages)>0 ){
					unset($_REQUEST["run"]);
					$this->pageManageProfiles();
				} else {
					unset($_REQUEST["run"]);
					$this->session->set( 'qs-servid', $_REQUEST["qs-servid"] );
					$this->pageVerifyInfo();
				}
				
			} else {
			    //initialize page
			    $content;
			    $content = array();
			    if ( count($this->messages)>0 ) {
			    	$content["messages"] = $this->messages ;
			    }
			    $content["profiles"] = $this->model->getAllProfiles();
		        $this->view->pageManageProfiles($content);
			}