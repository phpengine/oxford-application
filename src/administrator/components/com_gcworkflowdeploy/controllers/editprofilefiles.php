<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validation
		$validation = 0;
		$this->pageEditProfileDataTables();
	} else {
		ini_set("max_execution_time", "360");
		// if its not
	    // initialize page
	    $content;
	    $content = array();
	    if ( count($this->messages)>0 ) {
	    	$content["messages"] = $this->messages ;
	    }
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."filesClass.php");
		$doc = JFactory::getDocument();
		$doc->addScript("/administrator/components/com_gcworkflowdeploy/scripts/jquery.js");
		$doc->addScript("/administrator/components/com_gcworkflowdeploy/scripts/gcjssetup.js");
		$doc->addScript("/administrator/components/com_gcworkflowdeploy/scripts/filechooser.js");		
		$filesys = new GCWorkflowDeployerFilesClass();
		$content["allfiles"] = $filesys->getContents();
	    $content["profiledetails"] = $this->model->getSingleProfileDetails($_REQUEST["profileid"]);
		$content["currentfiles"] = $this->model->getSingleProfileDetailFileFolders($content["profiledetails"]["profile_uniqueid"]);
        $this->view->pageEditProfileFiles($content);
	}