<?php
	if (isset($_REQUEST["run"]) ) {
		unset($_REQUEST["run"]);
		// If its set to run do the validation
		// Check the stage
		if($_REQUEST["stage"]==1) {		    
		    $content;
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ;
		    }
		    $content["keyid"] = $_REQUEST["keyid"];
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $content["key"] = $this->model->getSingleKeyDetails($content["keyid"]);
        	$this->view->pageDeleteKey($content);
		} elseif($_REQUEST["stage"]==2) {		    
		    $content;
		    $content = array();
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ;
		    }
		    $content["keyid"] = $_REQUEST["keyid"];
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $content["key"] = $this->model->getSingleKeyDetails($content["keyid"]);
        	$this->view->pageDeleteKey($content);
		}  elseif($_REQUEST["stage"]==3) {		    
		    $content;
		    $content = array();
		    $content["keyid"] = $_REQUEST["keyid"];
		    $content["key"] = $this->model->getSingleKeyDetails($content["keyid"]);
		    $content["curstage"] = $_REQUEST["stage"];
		    $content["newstage"] = $_REQUEST["stage"] + 1;
		    $this->model->deleteKey($content["keyid"]);
		    if ( count($this->messages)>0 ) {
		    	$content["messages"] = $this->messages ;
		    }
        	$this->view->pageDeleteKey($content);
		}  elseif($_REQUEST["stage"]==4) {	
			$this->pageHome();
		} else { // no stage page accessed in error
			$this->messages[] = "Delete Page accessed in error";
			$this->pageHome();
		}
	} else {
		$this->messages[] = "Delete Page accessed in error";
		$this->pageHome();
	}
