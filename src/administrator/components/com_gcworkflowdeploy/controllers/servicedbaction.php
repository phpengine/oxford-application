<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

$actionservice = new GCWorkflowDeployerDBActionsClass();
$actionservice->executeme();

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'modelClass.php';

class GCWorkflowDeployerDBActionsClass {
	
	private static $config ;
	private $actioncode ;
	private $tblname;
	private $profileunique;
	private $model ;

	public function __construct(){
		$this->configs = new GCWorkflowDeployerConfigClass();
		$this->model = new GCWorkflowDeployerModelClass();
		if (isset($_REQUEST["actioncode"])) {$this->actioncode = $_REQUEST["actioncode"];}
		else { echo "Action Code Missing"; } 
		if (isset($_REQUEST["profileunique"])) {$this->profileunique = $_REQUEST["profileunique"];}
		else { echo "P Unique Missing"; } 
		if (isset($_REQUEST["tblname"])) {$this->tblname = $_REQUEST["tblname"];}
		else { echo "Table Name Missing"; } 
	}
	
	public function executeme() {
		$content = $this->control();
		$this->display($content);			
	}
	
	private function control(){
		$content = array();
		// REQUIRE THE ACTION CODE SPECIFIC CONTROL FUNCTIONS
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'actions'.DS.$this->actioncode.'control.php');
		return $content;
	}
	
	private function display($content){
		// REQUIRE THE ACTION CODE SPECIFIC DISPLAY FUNCTIONS
		echo '<div class="result">';
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'actions'.DS.$this->actioncode.'display.php');
		echo '</div>';	
	}
	
	
}
