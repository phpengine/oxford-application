<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

$fservice = new GCWorkflowDeployerFilesClass();
$fservice->executeme();

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'modelClass.php';

class GCWorkflowDeployerFilesClass {
	
	private static $config ;
	private $basedir ;
	private $dir ;
	private $fcid ;
	private $profileunique;
	private $model ;

	public function __construct(){
		$this->configs = new GCWorkflowDeployerConfigClass();
		$this->basedir = $this->getRootFolderToCalculate();
		$this->model = new GCWorkflowDeployerModelClass();
		// set dir and remove first / if needed
		if (isset($_REQUEST["dir"])) {$this->dir = $_REQUEST["dir"];}
        $firstchar = substr($this->dir,0,1);
        if ($firstchar=="/") {
        	$this->dir = substr($this->dir,1,(strlen($this->dir)-1) );
        }
		if ( isset($_REQUEST["fcid"]) ) {$this->fcid = $_REQUEST["fcid"];}
		if (isset($_REQUEST["actionreq"])) {$this->actionreq = $_REQUEST["actionreq"];}
		if (isset($_REQUEST["profileunique"])) {$this->profileunique = $_REQUEST["profileunique"];}
	}
	
	public function executeme() {
		if ($this->actionreq == "getdir") {
			$filelist = $this->getFileList($this->basedir.$this->dir);
			$this->displayFileList($filelist);			
		} else if ($this->actionreq == "checkisdir") {
			$checkeddir = $this->checkIsDir($this->basedir.$this->dir);
			$this->displayDirCheck($checkeddir);
		} else if ($this->actionreq == "addentry") {
			$this->addEntry($this->profileunique, $this->basedir.$this->dir);
			$this->displayAllEntries();
		} else if ($this->actionreq == "delentry") {
			$this->deleteEntry($this->profileunique, $this->basedir.$this->dir);
			$this->displayAllEntries();
		} else if ($this->actionreq == "curfiles") {
			$this->displayAllEntries();
		}
	}
	
	/*
	 * THESE ARE THE FILESYSTEM READERS
	 * */
	public function getContents() {
		$dir = $this->getRootFolderToCalculate();
		$filelist = array();
		$filelist = $this->getFileList($dir);
		return $filelist;
	} 

	private function getRootFolderToCalculate() {
		$folder  = JPATH_BASE;
		// this includes the administrator folder
		$fulllength = strlen(JPATH_BASE);
		$lengthminusadmin = $fulllength - 13;
		$folder = substr($folder,0,$lengthminusadmin);
		return $folder;		
	}
	
	public function getFileList($dir) {
		$filelist = array();
		$dh = opendir($dir);
        while (($file = readdir($dh)) !== false) {
        	$mini = array();
        	if ( $file=="." || $file==".." ) {
        	
        	} else if (filetype($dir.DS.$file) == "file" ) {
        		$mini["name"] = $file;
        		$mini["type"] = "file";
        		$filelist[] = $mini;  	
        	} else if (filetype($dir.DS.$file) == "dir" ) {
        		$mini["name"] = $file;
        		$mini["type"] = "dir";
        		$filelist[] = $mini;  	
        	}
        }
        closedir($dh);
        $this->aasort($filelist, "name");
		return $filelist;	
	}
	
	
	public function displayFileList($filelist) {
		$htmlvar = "";
		$htmlvar .= '<div class="checkboxholderBodyServiceWrap">';
		$htmlvar .= '<div class="checkboxholder" id="checkboxHolder'.$this->fcid.'" >';
		$htmlvar .= ' <div class="checkboxholderTitle">';
		$htmlvar .= '  <h3>Level '.$this->fcid.'</h3>';		
		$htmlvar .= ' </div> <!-- end checkboxholderTitle -->';
		$htmlvar .= ' <div class="checkboxholderBody">';
		$htmlvar .= '	<select name="fileroot" size="15" class="filechooser" id="filechooser'.$this->fcid.'" onchange="chooserChange('.$this->fcid.');">';
		foreach ($filelist as $file ) {
			if ($file["type"]=="file") {
				$htmlvar .= '<option class="chkbline file" value="'.$file["name"].'">'.$file["name"];
				$htmlvar .= '</option>';
			} else if ($file["type"]=="dir"){
				$htmlvar .= '<option class="chkbline dir" value="'.$file["name"].'">'.$file["name"];
				$htmlvar .= '</option>';
			}
		}
		$htmlvar .= '	</select>';
		$htmlvar .= '</div> <!-- end checkboxHolderBody --> ';
		$htmlvar .= '</div> <!-- end checkboxHolderBodyServiceWrap --> ';
		echo $htmlvar;
	}
	
	
	/*
	 * dir checker
	 */
	private function checkIsDir($tocheck) {
		if (filetype($tocheck) == "dir" ) {
			return "yes";
		} else {
			return "no";			
		}
	}
	
	private function displayDirCheck($result) {
		$htmlvar = "";
		$htmlvar .= '<div class="result">';
		$htmlvar .=  $result;
		$htmlvar .= '</div> ';
		echo $htmlvar;
	}
	
	
	
	/*
	 *  For DB Entry
	 */
	private function addEntry() {
		$this->model->addProfileFileFolder($this->profileunique, $this->dir);
	}
	
	private function deleteEntry() {
		$this->model->deleteProfileFileFolder($this->profileunique, $this->dir);
	}
	
	private function displayAllEntries() {
		$currententries = $this->model->getSingleProfileDetailFileFolders($this->profileunique);
		$htmlvar = "";
		$htmlvar .= '<div class="result">';
		$htmlvar .= '   <h3>Current Files:</h3>';
		foreach ($currententries as $currententry) {
			    // break this variable up on the space so that it break over lines
			    $currentfiletodisplay = str_replace("/", "/ ", $currententry["profile_file_path"]);
				$htmlvar .= '<div class="entryrow"><div class="left"><input type="button" class="gcbuttonsml" value="X" onclick=\'deleteThisValue("'.$currententry["profile_file_path"].'");\' /></div><div class="right"><p>'.$currentfiletodisplay.'</p></div></div>';	
		}
		$htmlvar .= '</div> ';
		echo $htmlvar;
	}
	
	

	
	/*
	 * Helpers
	 */
	private function aasort (&$array, $key) {
	    $sorter=array();
	    $ret=array();
	    reset($array);
	    foreach ($array as $ii => $va) {
	        $sorter[$ii]=$va[$key];
	    }
	    asort($sorter);
	    foreach ($sorter as $ii => $va) {
	        $ret[$ii]=$array[$ii];
	    }
	    $array=$ret;
	    return $array;
	}
	
}
