<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';

class GCWorkflowDeployerGroupEntryAvailableClass {
	
	private static $configs ;

	public function __construct() {
		$this->configs = new GCWorkflowDeployerConfigClass();
	}
	
	/*
	 * THESE ARE THE FILESYSTEM READERS
	 **/
  public function getAllInstances() {
    $feature_store = $this->configs->give("feature_store_folder");
    $instances = scandir($feature_store);
    $instancesFormatted = array();
    foreach ($instances as $instance) {
      if (!in_array($instance, array(".", "..", ".gitignore"))) {
        $miniRay = array();
        $miniRay["title"] = $instance ;
        $instancesFormatted[] = $miniRay ; } }
    $isInstances = ( is_array($instancesFormatted) && (count($instancesFormatted)>0) );
    return ($isInstances) ? $instancesFormatted : array() ;
  }

  public function getAllAvailableProfiles() {
    $feature_store = $this->configs->give("feature_store_folder");
    $instances = scandir($feature_store);
    $profileUniques = array();
    $profileUniqueIds = array();
    foreach ($instances as $instance) {
      if (!in_array($instance, array(".", "..", ".gitignore"))) {
        $profile_data = $this->getProfileFromFile($instance);
        $profile_id_only = substr($instance, 0, 16);
        if (!in_array($profile_id_only, $profileUniqueIds)) {
          $miniRay = array();
          $miniRay["pdata"] = $profile_data;
          $miniRay["unique"] = $profile_id_only;
          $profileUniques[] = $miniRay;
          $profileUniqueIds[] = $profile_id_only; } } }
    $isProfiles = ( is_array($profileUniques) && (count($profileUniques)>0) );
    return ($isProfiles) ? $profileUniques : array() ;
  }

	public function getProfileFromFile($file) {
    $extractDir = substr($file, 0, strlen($file)-4);
    $zip = new ZipArchive ;
    $zip->open($this->configs->give("feature_store_folder").DS.$file) ;
    $zip->extractTo($this->configs->give("temp_pull_folder").DS.$extractDir);
    $profileData = file_get_contents($this->configs->give("temp_pull_folder") . DS . $extractDir .
      DS . "gcworkflowprofile.profile" ) ;
    $profile = unserialize($profileData);
		return $profile ;
	}

	
	/*
	 * Helpers
	 */
	private function aasort (&$array, $key) {
	    $sorter=array();
	    $ret=array();
	    reset($array);
	    foreach ($array as $ii => $va) {
	        $sorter[$ii]=$va[$key]; }
	    asort($sorter);
	    foreach ($sorter as $ii => $va) {
	        $ret[$ii]=$array[$ii]; }
	    $array=$ret;
	    return $array;
	}

}
