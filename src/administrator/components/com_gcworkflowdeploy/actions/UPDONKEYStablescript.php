<?php

$querys = array();

$action_details = unserialize($stored_table["profile_datatable_action_details"]) ;
$action_details_formatted = array();
foreach ($action_details as $action_detail_miniray) {
    foreach ($action_detail_miniray as $action_detail_minikey => $action_detail_minival) {
        $action_details_formatted[$action_detail_minikey] =  $action_detail_minival; } }

$db =& JFactory::getDBO();
$tableFields = $db->getTableFields(array($stored_table["profile_datatable_tablename"]), false);

$fieldsToMatch = array();
$fieldsToUpdate = array();

foreach ($tableFields[$stored_table["profile_datatable_tablename"]] as $currentFieldName=>$currentFieldValue) {

    $curFieldToMatchKey = $stored_table["profile_datatable_tablename"]."_".$stored_table["profile_datatable_ordering"].
        "_".$currentFieldName."_on" ;

    $curFieldToUpdateKey = $stored_table["profile_datatable_tablename"]."_".$stored_table["profile_datatable_ordering"].
        "_".$currentFieldName."_update" ;

    if (array_key_exists($curFieldToMatchKey, $action_details_formatted) && $action_details_formatted[$curFieldToMatchKey]=="on") {
        $fieldsToMatch[] = $currentFieldName; }

    if (array_key_exists($curFieldToUpdateKey, $action_details_formatted) && $action_details_formatted[$curFieldToUpdateKey]=="on") {
        $fieldsToUpdate[] = $currentFieldName; }

}

// once we have the right fields, get the database table (temp one) so we can pull the data we need from it into queries
$queryForValueFromTemp = 'SELECT * FROM '.$temptable ;
$db->setQuery($queryForValueFromTemp);
$db->query();
$total_table_rows = $db->getNumRows();

//count is greater than 100 do it in groups of 100
$icounter = 0 ;
$number_of_data_runs = ceil($total_table_rows/100);

for ($i=0; $i<=$total_table_rows; $i++) {
    $countStart = $i * 100 ;
    $queryForDataRun = 'SELECT * FROM '.$temptable.' LIMIT '.$countStart.', 100' ;
    $db->setQuery($queryForDataRun);
    $db->query();
    $data_run_row_array = $db->loadAssocList() ;
    foreach ($data_run_row_array as $table_row) {
        foreach ($fieldsToUpdate as $curFieldToUpdate) {
            foreach ($fieldsToMatch as $curFieldToMatch) {
                $curValueToMatch = $db->loadResult();
                $query = 'UPDATE '.$origtable.' SET '.$curFieldToUpdate.'="'.
                    mysql_real_escape_string($table_row[$curFieldToUpdate]) . '" WHERE ' .
                    $curFieldToMatch.'=\''.$table_row[$curFieldToMatch].'\' ; ';
                $querys[] = $query; } } } }

foreach ( $querys as $query) {
    $db->setQuery($query);
    $db->query(); }