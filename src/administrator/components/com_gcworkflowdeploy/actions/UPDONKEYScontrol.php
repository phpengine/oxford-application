<?php

/*
 * THIS IS THE UPDATEON KEYS AJAX PAGE CONTROL FUNCTION
 */

$content = array();
$db =& JFactory::getDBO();

$content["ordering"] = $_REQUEST["ordering"];
$content["table"] = $db->getTableFields($_REQUEST["tblname"], false);
$content["tblname"] = $_REQUEST["tblname"];
$content["tableheadingsdata"] = $content["table"][$_REQUEST["tblname"]];

$content["tableheadings"] = array();

foreach($content["tableheadingsdata"]  as $tableHeadingData => $thdValues) {
	$content["tableheadings"][] = $thdValues->Field; }
