<?php

/*
 * THIS IS THE FULL INDIVIDUAL ROWS AJAX PAGE DISPLAY FUNCTION 
 */
$htmlvar = "<p>Full Rows: </p>";
$htmlvar .= "<table>";
$htmlvar .= "<tr>";
$htmlvar .= "<td>";
$htmlvar .= 'Where this key...';
$htmlvar .= "</td>";
$htmlvar .= "<td>";
$htmlvar .= '... Has this value ...';
$htmlvar .= "</td>";
$htmlvar .= "<td>";
$htmlvar .= '...Update This...';
$htmlvar .= "</td>";
$htmlvar .= "</tr>";
foreach ($content["tableheadings"] as $heading) {
  $htmlvar .= "<tr>";
  $htmlvar .= "<td>";
  $htmlvar .= '<input type="checkbox" name="'.$content["tblname"].'_'.$content["ordering"].'_'.$heading.'_on"></input>';
  $htmlvar .= $heading;
  $htmlvar .= "</td>";
  $htmlvar .= "<td>";
  $htmlvar .= '<input type="text" name="'.$content["tblname"].'_'.$content["ordering"].'_'.$heading.'_shouldbe"></input>';
  $htmlvar .= "</td>";
  $htmlvar .= "<td>";
  $htmlvar .= '<input type="checkbox" name="'.$content["tblname"].'_'.$content["ordering"].'_'.$heading.'_update"></input>';
  $htmlvar .= "</td>";
  $htmlvar .= "</tr>"; }

$htmlvar .= "<table>";
echo $htmlvar;