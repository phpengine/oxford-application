<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * mod_gcarticlecomments is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

class GCWorkflowDeployerBEViewClass {
	
	private static $config;

	public function __construct(){
		$this->configs = new GCWorkflowDeployerConfigClass();
	}

	public function pageHome($pageVars) {
		include('views/home.php');	
		echo $htmlvar; }

  public function pageDeleteGroup($pageVars) {
    include('views/deletegroup.php');
    echo $htmlvar; }

  public function pageDeleteKey($pageVars) {
    include('views/deletekey.php');
    echo $htmlvar; }

	public function pageDeleteServer($pageVars) {
		include('views/deleteserver.php');
		echo $htmlvar; }

	public function pageDeleteProfile($pageVars) {
		include('views/deleteprofile.php');
		echo $htmlvar; }

	public function pageConfig($pageVars) {
		include('views/config.php');		
		echo $htmlvar; }

  public function pageManageGroups($pageVars) {
    include('views/managegroups.php');
    echo $htmlvar; }

  public function pageManageProfiles($pageVars) {
    include('views/manageprofiles.php');
    echo $htmlvar; }

	public function pageManageServers($pageVars) {
		include('views/manageservers.php');		
		echo $htmlvar; }

	public function pageManageKeys($pageVars) {
		include('views/managekeys.php');		
		echo $htmlvar; }

  public function pageEditGroups($pageVars) {
    include('views/editgroups.php');
    echo $htmlvar; }

  public function pageEditGroupEntries($pageVars) {
    include('views/editgroupentries.php');
    echo $htmlvar; }

  public function pageEditGroupConfirm($pageVars) {
    include('views/editgroupconfirm.php');
    echo $htmlvar; }

  public function pageEditServers($pageVars) {
    include('views/editservers.php');
    echo $htmlvar; }

  public function pageEditKeys($pageVars) {
		include('views/editkeys.php');		
		echo $htmlvar; }

	public function pageEditProfiles($pageVars) {
		include('views/editprofiles.php');		
		echo $htmlvar; }

	public function pageEditProfileFiles($pageVars) {
		include('views/editprofilefiles.php');		
		echo $htmlvar; }

	public function pageEditProfileDataTables($pageVars) {
		include('views/editprofiledatatables.php');		
		echo $htmlvar; }

	public function pageEditProfileDataTableActions($pageVars) {
		include('views/editprofiledatatableactions.php');		
		echo $htmlvar; }

	public function pageEditProfileConfirm($pageVars) {
		include('views/editprofileconfirm.php');		
		echo $htmlvar; }
	
	/* HELPERS */
	private function openPage(){
		JHTML::stylesheet('template.css', "administrator/components/com_gcworkflowdeploy/css/");
		echo '<h1>'.$this->configs->give("title_main").'</h1>';
	}
	
}