var previousfullpath = "";
var currentfullpath = "";  
var htmlage;
var maxchooser = 1;
var chosenpaths;


function chooserChange(chooser) {
	  jQuery('.hiddenloader').hide();
	  jQuery('.hiddenloader2').hide();
	  jQuery('.hiddenloader3').hide();
	  // if maxchooser is currently lower than this chooser, update it
	  if (maxchooser<chooser) {
		  maxchooser = chooser;		  
	  }
	  isdir = checkIsDir(chooser);
	  if (isdir == "yes") {
		  //loadimg = '<img id="loadimg" src="com_gcworkflowdeploy/images/loading.png" />';
		  //jQuery('div.checkboxHolderWrapBodyInnerWide').append(loadimg);
		  // TODO: this should not just loop 100 times, it should check what is
		  // actually the real depth level
		  // loop through all choosers higher than this and delete
		  for (looper=chooser+1;looper<100;looper=looper+1) {
			  jQuery('#checkboxHolder'+looper).remove();
		  }
		  var fullchooserid = "filechooser"+chooser;
		  var htmlvar = jQuery('div.checkboxHolderWrapBodyInnerWide').html();
		  var fcid=chooser+1;
		  if (chooser>1) {
			  var dir = "";
			  // TODO: this should not just loop 100 times, it should check what is
			  // actually the real depth level
			  // loop through all choosers higher than this and delete
			  for (looper=1;looper<chooser+1;looper=looper+1) {
				  var fullchooserid = "filechooser"+looper;
				  if ( jQuery('#'+fullchooserid).val() !== undefined ) {
					  dir = dir+'/'+jQuery('#'+fullchooserid).val();				  
				  }
			  }
			  // alert(dir);
		  } else {
			  var fullchooserid = "filechooser"+chooser;
			  var dir = jQuery('#'+fullchooserid).val();		  
		  }
		  jQuery.ajax({
			  type: "GET",
			  url: 'index.php?option=com_gcworkflowdeploy&tmpl=component&task=servicefile&actionreq=getdir&fcid='+fcid+'&dir='+dir,
			  success: function (data) {
				jQuery('.hiddenloader').html(data);
				//jQuery('#loadimg').remove();
				cbh = jQuery('.hiddenloader').find('.checkboxholderBodyServiceWrap').html();
			    jQuery('div.checkboxHolderWrapBodyInnerWide').append(cbh);
			  }
		  });  		  
	  } 	  
}

function checkIsDir(chooser) {
	  if (chooser>1) {
		  var dir = "";
		  // TODO: this should not just loop 100 times, it should check what is
		  // actually the real depth level
		  // loop through all choosers higher than this and delete
		  for (looper=1;looper<chooser+1;looper=looper+1) {
			  var fullchooserid = "filechooser"+looper;
			  if ( jQuery('#'+fullchooserid).val() !== undefined ) {
				  dir = dir+'/'+jQuery('#'+fullchooserid).val();				  
			  }
		  }
		  // alert(dir);
	  } else {
		  var fullchooserid = "filechooser"+chooser;
		  var dir = jQuery('#'+fullchooserid).val();		  
	  }
	  var retvar;
	  jQuery.ajax({
		  type: "GET",
		  async: false,
		  url: 'index.php?option=com_gcworkflowdeploy&tmpl=component&task=servicefile&actionreq=checkisdir&dir='+dir,
		  success: function (data) {
			jQuery('.hiddenloader2').html(data);
			retvar = jQuery('.hiddenloader2').find('.result').html();
		  }
	  });
	  return retvar;
}

function addThisValue() {
	  dir = "";
	  for (looper=1;looper<100;looper=looper+1) {
		  var fullchooserid = "filechooser"+looper;
		  if ( jQuery('#'+fullchooserid).val() !== undefined ) {
			  dir = dir+'/'+jQuery('#'+fullchooserid).val();				  
		  }
	  }
	  profileunique = jQuery('#profileuniqueid').val();
	  //alert("dir:"+dir);
	  dirstring = dir;
	  dirstringlength = dirstring.length;
	  testlength = dirstringlength-4; 
	  testsub = dirstring.substr(testlength,4);
	  if (testsub == "null") {
		  dir = dir.substring(0, (dirstringlength-4) );
	  }
	  jQuery.ajax({
		  type: "GET",
		  async: false,
		  url: 'index.php?option=com_gcworkflowdeploy&tmpl=component&task=servicefile&actionreq=addentry&profileunique='+profileunique+'&dir='+dir,
		  success: function (data) {
		    updateCurrentFiles();
		  }
	  });	
}

function deleteThisValue(valuetodel) {
	  profileunique = jQuery('#profileuniqueid').val();
	  jQuery.ajax({
		  type: "GET",
		  async: false,
		  url: 'index.php?option=com_gcworkflowdeploy&tmpl=component&task=servicefile&actionreq=delentry&profileunique='+profileunique+'&dir='+valuetodel,
		  success: function (data) {
		    updateCurrentFiles();
		  }
	  });	
}

function updateCurrentFiles() {
	  jQuery('.hiddenloader').hide();
	  jQuery('.hiddenloader2').hide();
	  jQuery('.hiddenloader3').hide();

	  profileunique = jQuery('#profileuniqueid').val();
	  //alert("dir:"+dir);
	  
	  jQuery.ajax({
		  type: "GET",
		  async: false,
		  url: 'index.php?option=com_gcworkflowdeploy&tmpl=component&task=servicefile&actionreq=curfiles&profileunique='+profileunique,
		  success: function (data) {
			jQuery('.hiddenloader3').html(data);
			newvar = jQuery('.hiddenloader3').find('.result').html();
			jQuery('.currentFilesWrap').html(newvar);
		  }
	  });	
	
}