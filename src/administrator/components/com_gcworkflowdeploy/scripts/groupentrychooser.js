var previousfullpath = "";
var currentfullpath = "";  
var htmlage;
var maxchooser = 1;
var chosenpaths;


function addThisEntry(chooser) {
    if ( jQuery('#'+chooser).val() !== undefined ) {
        entry = jQuery('#'+chooser).val(); }
    groupunique = jQuery('#groupuniqueid').val();
    jQuery.ajax({
        type: "GET",
        async: false,
        url: 'index.php?option=com_gcworkflowdeploy&tmpl=component&task=servicegroupentry&actionreq=addentry'+
            '&groupunique='+groupunique+'&entry='+entry+'&entryType='+chooser,
        success: function (data) {
            updateCurrentEntries();
        }
    });
}

function deleteThisEntry(ordering, entryType, target) {
	  groupunique = jQuery('#groupuniqueid').val();
	  jQuery.ajax({
		  type: "GET",
		  async: false,
		  url: 'index.php?option=com_gcworkflowdeploy&tmpl=component&task=servicegroupentry&actionreq=delentry' +
             '&groupunique='+groupunique+'&ordering='+ordering+'&entryType='+entryType+'&entry='+target,
		  success: function (data) {
		    updateCurrentEntries();
		  }
	  });	
}

function updateCurrentEntries() {
	  jQuery('.hiddenloader').hide();
	  jQuery('.hiddenloader2').hide();
	  jQuery('.hiddenloader3').hide();
	  groupunique = jQuery('#groupuniqueid').val();
	  jQuery.ajax({
		  type: "GET",
		  async: false,
		  url: 'index.php?option=com_gcworkflowdeploy&tmpl=component&task=servicegroupentry&actionreq=curentries' +
              '&groupunique='+groupunique,
		  success: function (data) {
			jQuery('.hiddenloader3').html(data);
			newvar = jQuery('.hiddenloader3').find('.result').html();
			jQuery('.currentFilesWrap').html(newvar);
		  }
	  });	
	
}