<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );
if(!defined('DS')) { define('DS',DIRECTORY_SEPARATOR); }

// CLASS REQUIRED
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'controlClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';

$configs = new GCWorkflowDeployerConfigClass();
JToolbarHelper::title($configs->give("title_main"),'title');
// JHTML::stylesheet('template.css', JPATH_COMPONENT_ADMINISTRATOR.DS.'css');

echo '<link rel="stylesheet" href="http://'.$_SERVER["HTTP_HOST"].DS."administrator".DS."components".DS.
  "com_gcworkflowdeploy".DS."css".DS.'template.css" type="text/css" />';

$control = new GCWorkflowDeployerBEControlClass();

$task = (isset($_REQUEST["task"])) ? $_REQUEST["task"] : 'home' ;
switch ($task) {
  case 'home':
    $control->pageHome();
    break;
  case 'deletegroup':
    $control->pageDeleteGroup();
    break;
  case 'deletekey':
    $control->pageDeleteKey();
    break;
  case 'deleteserver': 
    $control->pageDeleteServer();
    break;
  case 'deleteprofile': 
    $control->pageDeleteProfile();
    break;
  case 'configure':
    $control->pageConfig();
    break;
  case 'manservers': 
    $control->pageManageServers();
    break;
  case 'manprofiles': 
    $control->pageManageProfiles();
    break;
  case 'mankeys':
    $control->pageManageKeys();
    break;
  case 'mangroups':
    $control->pageManageGroups();
    break;
  case 'editgroups':
    $control->pageEditGroups();
    break;
  case 'editgroupentries':
    $control->pageEditGroupEntries();
    break;
  case 'editgroupconfirm':
    $control->pageEditGroupConfirm();
    break;
  case 'editservers':
    $control->pageEditServers();
    break;
  case 'editprofiles': 
    $control->pageEditProfiles();
    break;
  case 'editprofilefiles': 
    $control->pageEditProfileFiles();
    break;
  case 'editprofiledatatables': 
    $control->pageEditProfileDataTables();
    break;
  case 'editprofiledatatableactions': 
    $control->pageEditProfileDataTableActions();
    break;
  case 'editkeys': 
    $control->pageEditKeys();
    break;
  case 'servicefile': 
    $control->pageServiceFile();
    break;
  case 'servicedbaction':
    $control->pageServiceDBAction();
    break;
  case 'servicegroupentry':
    $control->pageServiceGroupEntry();
    break;
  default:
    $control->pageHome();
    break;
}
