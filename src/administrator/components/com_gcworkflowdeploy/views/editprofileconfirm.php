<?php

		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_verify_deployment_info").'</h2>';	
		$htmlvar .= '
		   <form action="index.php" method="POST">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }

		$htmlvar .= '<table>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<th><h3>Name</h3></th>';
		$htmlvar .= '		<th><h3>Value</h3></th>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Profile Title:</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["profiledetails"]["profile_title"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Profile Description:</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["profiledetails"]["profile_description"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Profile Unique ID</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["profiledetails"]["profile_uniqueid"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Profile Files and Folders:</p></td>';
		if ( count($pageVars["profiledetails"]["profile_files"]) > 0 ) {
			$htmlvar .= '		<td>';
			foreach($pageVars["profiledetails"]["profile_files"] as $filefolder) {
				$htmlvar .= '			<p>'.$filefolder["profile_file_path"].'</p>'; }
			$htmlvar .= '		</td>'; }
		else {
			$htmlvar .= '		<td><p>No Files</p></td>'; }
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Data Tables:</p></td>';
		if ( count($pageVars["profiledetails"]["profile_datatables"]) > 0 ) {
			$htmlvar .= '		<td>';
			foreach($pageVars["profiledetails"]["profile_datatables"] as $datatable) {
                $htmlvar .= '			<p>Table Name: '.$datatable["profile_datatable_tablename"].'</p>';
                $htmlvar .= '			<p>Table Order: '.$datatable["profile_datatable_ordering"].'</p>';
                $htmlvar .= '			<p>Action Type: '.$datatable["profile_datatable_action_code"].'</p>';
				$actions = unserialize($datatable["profile_datatable_action_details"]); 
				foreach ($actions as $action => $actionVal) {
					foreach ($actionVal as $action => $val){
						if ($val != "") {
							ob_start();
							print_r($action);
							$actionText = ob_get_clean();
							$htmlvar .= '<p>Action: '.$actionText.' = '.$val.'</p>';  } } } }
			$htmlvar .= '		</td>'; }
		else {
			$htmlvar .= '		<td><p>No Data Tables</p></td>'; }
		$htmlvar .= '	</tr>';
		$htmlvar .= '</table>';
		
		$htmlvar .= ' <input type="hidden" name="profileid" id="profileid" value="'.$pageVars["profiledetails"]["id"].'" /> ';
		$htmlvar .= ' <input type="hidden" name="profileuniqueid" id="profileuniqueid" value="'.$pageVars["profiledetails"]["profile_uniqueid"].'" /> ';
		
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Return to Home" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="home" />
		   </form>';
