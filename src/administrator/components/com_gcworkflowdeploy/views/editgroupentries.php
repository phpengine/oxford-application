<?php
		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_edit_group_entries").'</h2>';
		
    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
      foreach($pageVars["messages"] as $message) {
        $htmlvar .= '<p class="appMessage">'.$message.'</p>'; } }
	    
		$htmlvar .= '<form action="index.php" method="POST">';
		$htmlvar .= ' <div class="contentWrap">';
		$htmlvar .= '  <div class="contentTitle">';
		$htmlvar .= '   <div class="titleLeft">';
		$htmlvar .= '    <div class="titleLeft1">';
		$htmlvar .= '     <p>Title</p></td>';
		$htmlvar .= '     <p>'.$pageVars["groupdetails"]["group_name"].'</p>';
		$htmlvar .= '    </div> <!-- end titleLeft1 -->';
		$htmlvar .= '    <div class="titleLeft2">';
		$htmlvar .= '     <p>Description</p></td>';
		$htmlvar .= '     <p>'.$pageVars["groupdetails"]["group_desc"].'</p>';
		$htmlvar .= '    </div> <!-- end titleLeft2 -->';
		$htmlvar .= '   </div> <!-- end titleLeft -->';
		$htmlvar .= '   <div class="titleRight">';
		$htmlvar .= '   </div> <!-- end titleRight -->';
		$htmlvar .= '  </div> <!-- end contentTitle -->';
		$htmlvar .= '  <div class="contentBody">';
		
		$htmlvar .= '<div class="currentFilesWrap">';
		$htmlvar .= '   <h3>Current Entries:</h3>';
		foreach ($pageVars["currententries"] as $currententry ) {
      $htmlvar .= '<div class="entryrow"><div class="left"><input type="button" class="gcbuttonsml" ';
      $htmlvar .= 'value="X" onclick=\'deleteThisEntry("'.$currententry["ordering"].'", "'.$currententry["entry_type"] ;
      $htmlvar .= '", "'.$currententry["target"].'");\' /></div>';
      $htmlvar .= '<div class="right"><p>'.$currententry["ordering"].' - '.$currententry["entry_type"] ;
      $htmlvar .= ' '.$currententry["target"].'</p></div></div>'; }
		$htmlvar .= '</div> <!-- currentFilesWrap -->';

    $htmlvar .= '<div class="checkboxHolderWrap">';

    $htmlvar .= '<div class="checkboxHolderGroupWrap checkBoxHolderColumn">';
    $htmlvar .= ' <div class="checkboxHolderWrapTitle">';
    $htmlvar .= '   <h3>Choose a Group to add:</h3>';
    $htmlvar .= ' </div>';
    $htmlvar .= ' <div class="checkboxHolderWrapBody">';
    $htmlvar .= ' <div class="checkboxHolderWrapBodyInnerWide">';
    $htmlvar .= ' <div class="checkboxholder" id="checkboxHolder1">';
    $htmlvar .= '  <div class="checkboxholderTitle">';
    $htmlvar .= '  <h3>Group add below:</h3>';
    $htmlvar .= ' </div> <!-- end checkboxholderTitle -->';
    $htmlvar .= '<div class="checkboxholderBody">';

    if (count($pageVars["entrygroups"])>0) {
      $htmlvar .= '	<select name="group" size="15" class="filechooser" id="group">';
      foreach ($pageVars["entrygroups"] as $group ) {
        $htmlvar .= '<option class="chkbline file" value="'.$group["group_name"].'">' .
          $group["group_name"] . ' - ' . $group["group_desc"]. '</option>'; }
      $htmlvar .= '	</select>'; }
    else {
      $htmlvar .= '<p>No Groups Available</p>'; }

    $htmlvar .= '</div> <!-- end checkboxHolderBody --> ';
    if (count($pageVars["entrygroups"])>0) {
        $htmlvar .= '<div class="minibutton"> <!-- end checkboxminibutton --> ';
        $htmlvar .= '    <input type="button" class="gcbutton" value="Add This Entry" ' ;
        $htmlvar .= 'onclick="addThisEntry(\'group\')" />';
        $htmlvar .= '</div> <!-- end checkboxminibutton --> '; }

    $htmlvar .= '</div> <!-- end checkboxholder --> ';
    $htmlvar .= '</div> <!-- end checkboxHolderBodyInnerWide -->';
    $htmlvar .= '</div> <!-- end checkboxHolderWrapBody -->';
    $htmlvar .= '</div> <!-- end checkBoxHolderGroupWrap -->';


    $htmlvar .= '<div class="checkboxHolderSingleInstanceWrap checkBoxHolderColumn">';
    $htmlvar .= ' <div class="checkboxHolderWrapTitle">';
    $htmlvar .= '  <h3>Choose Instance to add:</h3>';
    $htmlvar .= ' </div>';
    $htmlvar .= '  <div class="checkboxHolderWrapBody">';
    $htmlvar .= '   <div class="checkboxHolderWrapBodyInnerWide">';
    $htmlvar .= '    <div class="checkboxholder" id="checkboxHolder1">';
    $htmlvar .= '     <div class="checkboxholderTitle">';
    $htmlvar .= '      <h3>Pick an available single instances here:</h3>';
    $htmlvar .= '     </div> <!-- end checkboxholderTitle -->';
    $htmlvar .= '     <div class="checkboxholderBody">';
    $htmlvar .= '	<select name="instance" size="15" class="filechooser" id="instance">';
    foreach ($pageVars["singleinstances"] as $instance ) {
      $htmlvar .= '<option class="chkbline file" value="'.$instance["title"].'">'.$instance["title"].'</option>';}
    $htmlvar .= '	</select>';
    $htmlvar .= '     </div> <!-- end checkboxHolderBody --> ';
    $htmlvar .= '    <div class="minibutton"> <!-- end checkboxminibutton --> ';
    $htmlvar .= '     <input type="button" class="gcbutton" value="Add This Entry"';
    $htmlvar .= ' onclick="addThisEntry(\'instance\')" />';
    $htmlvar .= '    </div> <!-- end checkboxminibutton --> ';
    $htmlvar .= '   </div> <!-- end checkboxholder --> ';
    $htmlvar .= '  </div> <!-- end checkboxHolderBodyInnerWide -->';
    $htmlvar .= ' </div> <!-- end checkboxHolderWrapBody -->';
    $htmlvar .= '</div> <!-- end checkboxHolderSingleInstanceWrap -->';

    $htmlvar .= '<div class="checkboxHolderAllProfiles checkBoxHolderColumn">';
    $htmlvar .= ' <div class="checkboxHolderWrapTitle">';
    $htmlvar .= '   <h3>Choose a Profile to add:</h3>';
    $htmlvar .= ' </div>';
    $htmlvar .= '  <div class="checkboxHolderWrapBody">';
    $htmlvar .= '   <div class="checkboxHolderWrapBodyInnerWide">';
    $htmlvar .= '    <div class="checkboxholder" id="checkboxHolder1">';
    $htmlvar .= '     <div class="checkboxholderTitle">';
    $htmlvar .= '      <h3>You can pick any of the available profiles here:</h3>';
    $htmlvar .= '     </div> <!-- end checkboxholderTitle -->';
    $htmlvar .= '      <div class="checkboxholderBody">';
    $htmlvar .= '	<select name="allinprofile" size="15" class="filechooser" id="allinprofile">';
    foreach ($pageVars["allprofiles"] as $profile ) {
      $htmlvar .= '<option class="chkbline file" value="' . $profile["pdata"]["push_profile_uniqueid"] . '">' .
        $profile["pdata"]["push_profile_title"] . ' - ' . $profile["pdata"]["push_profile_uniqueid"] .' - ' .
        $profile["pdata"]["push_profile_description"] . '</option>'; }
    $htmlvar .= '	</select>';
    $htmlvar .= '      </div> <!-- end checkboxHolderBody --> ';
    $htmlvar .= '    <div class="minibutton"> <!-- end checkboxminibutton --> ';
    $htmlvar .= '     <input type="button" class="gcbutton" value="Add This Entry"';
    $htmlvar .= ' onclick="addThisEntry(\'allinprofile\')" />';
    $htmlvar .= '    </div> <!-- end checkboxminibutton --> ';
    $htmlvar .= '     </div> <!-- end checkboxholder --> ';
    $htmlvar .= '    </div> <!-- end checkboxHolderBodyInnerWide -->';
    $htmlvar .= '   </div> <!-- end checkboxHolderWrapBody -->';
    $htmlvar .= '  </div> <!-- end checkboxHolderAllProfiles -->';
    $htmlvar .= ' </div> <!-- end checkBoxHolderWrap -->';
		$htmlvar .= '</div> <!-- end contentBody -->';
		
		$htmlvar .= '<input type="hidden" name="jsfilenamelevel" id="jsfilenamelevel" value="1" /> ';	
		$htmlvar .= '<input type="hidden" name="groupid" id="groupid" value="'.$pageVars["groupdetails"]["id"].'" /> ';
	  $htmlvar .= '<input type="hidden" name="groupuniqueid" id="groupuniqueid" value="'.$pageVars["groupdetails"]["uniqueid"].'" /> ';

		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Go To Next Stage" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editgroupentries" />
		   </form>';
		$htmlvar .= '</div> <!-- end contentwrap -->';
					
		$htmlvar .= '  <div class="hiddenloader"> ';
		$htmlvar .= '  </div> <!-- end hiddenloader -->';
		$htmlvar .= '  <div class="hiddenloader2"> ';
		$htmlvar .= '  </div> <!-- end hiddenloader -->';
		$htmlvar .= '  <div class="hiddenloader3"> ';
		$htmlvar .= '  </div> <!-- end hiddenloader -->';
		