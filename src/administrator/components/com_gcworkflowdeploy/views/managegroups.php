<?php

		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_manage_groups").'</h2>';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
		
		if (count($pageVars["groups"])>0 ) {
			$htmlvar .= '<table width="100%">';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<th><h3>Name</h3></th>';
      $htmlvar .= '		<th><h3>Description</h3></th>';
      $htmlvar .= '		<th><h3>Unique ID</h3></th>';
			$htmlvar .= '		<th></th>';
			$htmlvar .= '	</tr>';
			foreach ( $pageVars["groups"] as $group ) {
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<td><p>'.$group["group_name"].'</p></td>';
        $htmlvar .= '		<td><p>'.$group["group_desc"].'</p></td>';
        $htmlvar .= '		<td><p>'.$group["uniqueid"].'</p></td>';
				$htmlvar .= '		<td>';
				$htmlvar .= '
				   <form action="index.php" method="POST">
				    <p style="text-align:center;">
				     <input type="submit" name="submit" class="gcbutton" value="Edit This Group" />
				    </p>
				    <input type="hidden" name="groupid" id="groupid" value="'.$group["id"].'" />
				    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
				    <input type="hidden" name="task" id="task" value="editgroups" />
				   </form>';
				$htmlvar .= '		</td>';
				$htmlvar .= '		<td>';
				$htmlvar .= '
				   <form action="index.php" method="POST">
				    <p style="text-align:center;">
				     <input type="submit" name="submit" class="gcbutton" value="Delete This Group" />
				    </p>
				    <input type="hidden" name="groupid" id="groupid" value="'.$group["id"].'" />
				    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
				    <input type="hidden" name="task" id="task" value="deletegroup" />
				    <input type="hidden" name="stage" id="stage" value="1" />
				    <input type="hidden" name="run" id="run" value="1" />
				   </form>';
				$htmlvar .= '		</td>';
				$htmlvar .= '	</tr>';
			}
			$htmlvar .= '</table>';
		} else {
			$htmlvar .= '<h3>No Groups Added Yet</h3>';
		}
			
		$htmlvar .= '
		   <form action="index.php" method="POST">
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Create New Group" />
		    </p>
			<input type="hidden" name="groupid" id="groupid" value="new" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editgroups" />
		   </form>';