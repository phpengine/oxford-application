<?php
		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_edit_profile_data_table_actions").'</h2>';
		
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
		$htmlvar .= '<form action="index.php" method="POST">';
		$htmlvar .= ' <div class="contentWrap">';
		$htmlvar .= '  <div class="contentTitle">';
		$htmlvar .= '   <div class="titleLeft">';
		$htmlvar .= '    <div class="titleLeft1">';
		$htmlvar .= '     <p>Title</p></td>';
		$htmlvar .= '     <p>'.$pageVars["profiledetails"]["profile_title"].'</p>';
		$htmlvar .= '    </div> <!-- end titleLeft1 -->';
		$htmlvar .= '    <div class="titleLeft2">';
		$htmlvar .= '     <p>Description</p></td>';
		$htmlvar .= '     <p>'.$pageVars["profiledetails"]["profile_description"].'</p>';
		$htmlvar .= '    </div> <!-- end titleLeft2 -->';
		$htmlvar .= '   </div> <!-- end titleLeft -->';
		$htmlvar .= '   <div class="titleRight">';
		$htmlvar .= '   </div> <!-- end titleRight -->';
		$htmlvar .= '  </div> <!-- end contentTitle -->';
		$htmlvar .= '  <div class="contentBody">';

        $htmlvar .= '<div class="datatableActionHolderWrap">';
        $htmlvar .= '<div class="datatableActionHolderWrapLeft">';
		$htmlvar .= ' <div class="datatableActionHolderWrapTitle">';
		$htmlvar .= '   <h3>Choose which Data Tables to add</h3>';
		$htmlvar .= ' </div>';
		$htmlvar .= '<div class="datatableActionHolderWrapBody">';
		$htmlvar .= '<div class="datatableActionHolderWrapBodyInnerWide">';
				
		$itables = 1;
		foreach ($pageVars["datatablestoaction"] as $table ) {
			$htmlvar .= '<div class="datatable-action">';
			$htmlvar .= ' <h3 class="dtaction-title">Table '.$itables.' - '.$table["profile_datatable_tablename"].' '.$table["profile_datatable_ordering"].'</h3>';
			foreach ($pageVars["availableactions"] as $availableaction ) {
				$htmlvar .= '<div class="tblrow">';	
				$htmlvar .= '	<input type="radio" onchange=\'dbActionChange("'.$table["profile_datatable_tablename"].'", "'.$table["profile_datatable_ordering"].'");\' ' .
                    'name="taction_'.$table["profile_datatable_tablename"].'_'.$table["profile_datatable_ordering"].'" id="taction_'.
                    $table["profile_datatable_tablename"].'_'.$table["profile_datatable_ordering"].'" value="'.$availableaction["profile_actioncode_code"].'">'.
                    $availableaction["profile_actioncode_title"].'</input>';
				$htmlvar .= '</div>'; }
			$htmlvar .= ' <div id="ajaxtarget_'.$table["profile_datatable_tablename"].'_'.$table["profile_datatable_ordering"].'">';
			$htmlvar .= ' </div>';
			$htmlvar .= '</div>';
			$itables++;
		}
		
		$htmlvar .= '</div> <!-- end datatableActionHolderBodyInnerWide -->';
		$htmlvar .= '  </div> <!-- end datatableActionHolderWrapBody -->';
        $htmlvar .= '  </div> <!-- end datatableActionHolderWrapLeft -->';
        $htmlvar .= '<div class="datatableActionHolderWrapRight">';

        $htmlvar .= '<table>';
        $htmlvar .= '	<tr>';
        $htmlvar .= '		<td><p>Data Tables:</p></td>';
        if ( count($pageVars["datatablestoaction"]) > 0 ) {
            $htmlvar .= '		<td>';
            foreach($pageVars["datatablestoaction"] as $datatable) {
                $htmlvar .= '			<p>Table Name: '.$datatable["profile_datatable_tablename"].'</p>';
                $htmlvar .= '			<p>Table Order: '.$datatable["profile_datatable_ordering"].'</p>';
                $htmlvar .= '			<p>Action Type: '.$datatable["profile_datatable_action_code"].'</p>';
                if (isset($datatable["current_profile_datatable_action_details"])) {
                    $actions = unserialize($datatable["current_profile_datatable_action_details"]);
                    foreach ($actions as $action => $actionVal) {
                        foreach ($actionVal as $action => $val){
                            if ($val != "") {
                                ob_start();
                                print_r($action);
                                $actionText = ob_get_clean();
                                $htmlvar .= '<p>Action: '.$actionText.' = '.$val.'</p>';  } } } } }
            $htmlvar .= '		</td>'; }
        else {
            $htmlvar .= '		<td><p>No Data Tables</p></td>'; }
        $htmlvar .= '	</tr>';
        $htmlvar .= '</table>';
        $htmlvar .= '  </div> <!-- end datatableActionHolderWrapRight -->';
        $htmlvar .= '  </div> <!-- end datatableActionHolderWrap -->';
		$htmlvar .= '  </div> <!-- end contentBody -->';
		$htmlvar .= ' <input type="hidden" name="profileid" id="profileid" value="'.$pageVars["profiledetails"]["id"].'" /> ';
		$htmlvar .= ' <input type="hidden" name="profileuniqueid" id="profileuniqueid" value="'.$pageVars["profiledetails"]["profile_uniqueid"].'" /> ';

        $htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Go To Next Stage" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editprofiledatatableactions" />
            </form>';
        $htmlvar .= '
		    <form action="index.php" method="POST">;
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Go To Next Stage, Discard Changes" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="nochanges" id="nochanges" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editprofiledatatableactions" />
            <input type="hidden" name="profileid" id="profileid" value="'.$pageVars["profiledetails"]["id"].'" />
            <input type="hidden" name="profileuniqueid" id="profileuniqueid" value="'.$pageVars["profiledetails"]["profile_uniqueid"].'" />
		   </form>';
		$htmlvar .= '</div> <!-- end contentwrap -->';

					
		$htmlvar .= '  <div class="hiddenloader"> ';
		$htmlvar .= '  </div> <!-- end hiddenloader -->';
		$htmlvar .= '  <div class="hiddenloader2"> ';
		$htmlvar .= '  </div> <!-- end hiddenloader -->';
		$htmlvar .= '  <div class="hiddenloader3"> ';
		$htmlvar .= '  </div> <!-- end hiddenloader -->';
		
		