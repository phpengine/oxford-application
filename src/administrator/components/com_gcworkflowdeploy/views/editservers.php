<?php
		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_edit_servers").'</h2>';
		
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
		$htmlvar .= '<form action="index.php" method="POST">';
		$htmlvar .= ' <table>';
		
		if ($pageVars["edit"]==1) {
			$htmlvar .= '<tr>';
			$htmlvar .= ' <td><p>ID</p></td>';
			$htmlvar .= ' <td><p style="text-align:left;">'.$pageVars["serverdetails"]["id"].'</p></td>';
			$htmlvar .= '</tr>';
		}

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Title</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_title" value="';
		if ( isset($pageVars["serverdetails"]["serv_title"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_title"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Description</p></td>';
		$htmlvar .= ' <td><input type="textarea" name="serv_desc" value="';
		if ( isset($pageVars["serverdetails"]["serv_desc"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_desc"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';
		
		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Website URL</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_website_url" value="';
		if ( isset($pageVars["serverdetails"]["serv_website_url"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_website_url"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';
		
		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Webservice URL</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_webservice_url" value="';
		if ( isset($pageVars["serverdetails"]["serv_webservice_url"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_webservice_url"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';
		
		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Server Key</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_key" value="';
		if ( isset($pageVars["serverdetails"]["serv_key"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_key"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Upload Type</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_upload_type" value="';
		if ( isset($pageVars["serverdetails"]["serv_upload_type"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_upload_type"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>SSH User</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_ssh_user" value="';
		if ( isset($pageVars["serverdetails"]["serv_ssh_user"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_ssh_user"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';
		
		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>SSH Pass</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_ssh_pass" value="';
		if ( isset($pageVars["serverdetails"]["serv_ssh_pass"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_ssh_pass"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>SSH Folder</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_ssh_folder" value="';
		if ( isset($pageVars["serverdetails"]["serv_ssh_folder"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_ssh_folder"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>FTP User</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_ftp_user" value="';
		if ( isset($pageVars["serverdetails"]["serv_ftp_user"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_ftp_user"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';
		
		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>FTP Pass</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_ftp_pass" value="';
		if ( isset($pageVars["serverdetails"]["serv_ftp_pass"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_ftp_pass"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>FTP Folder</p></td>';
		$htmlvar .= ' <td><input type="text" name="serv_ftp_folder" value="';
		if ( isset($pageVars["serverdetails"]["serv_ftp_folder"]) ) {$htmlvar .= $pageVars["serverdetails"]["serv_ftp_folder"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= ' </table>';
		
		if ($pageVars["edit"]==1) {
		   $htmlvar .= ' <input type="hidden" name="serverid" id="serverid" value="'.$pageVars["serverdetails"]["id"].'" /> ';			
		} else {
		   $htmlvar .= ' <input type="hidden" name="serverid" id="serverid" value="new" /> ';
		}

		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Submit" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editservers" />
		   </form>';
