<?php
		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_edit_profiles").'</h2>';
		
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
		$htmlvar .= '<form action="index.php" method="POST">';
		$htmlvar .= ' <table>';
		
		if ($pageVars["edit"]==1) {
			$htmlvar .= '<tr>';
			$htmlvar .= ' <td><p>ID</p></td>';
			$htmlvar .= ' <td><p style="text-align:left;">'.$pageVars["profiledetails"]["id"].'</p></td>';
			$htmlvar .= '</tr>';
		}

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Title</p></td>';
		$htmlvar .= ' <td><input type="text" name="profile_title" value="';
		if ( isset($pageVars["profiledetails"]["profile_title"]) ) {$htmlvar .= $pageVars["profiledetails"]["profile_title"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Description</p></td>';
		$htmlvar .= ' <td><textarea name="profile_description" >';
		if ( isset($pageVars["profiledetails"]["profile_description"]) ) {$htmlvar .= $pageVars["profiledetails"]["profile_description"]; }
		$htmlvar .= '</textarea></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= ' </table>';
		
		if ($pageVars["edit"]==1) {
		   $htmlvar .= ' <input type="hidden" name="profileid" id="profileid" value="'.$pageVars["profiledetails"]["id"].'" /> ';
		   $htmlvar .= ' <input type="hidden" name="profileuniqueid" id="profileuniqueid" value="'.$pageVars["profiledetails"]["profile_uniqueid"].'" /> ';			
		} else {
		   $htmlvar .= ' <input type="hidden" name="profileid" id="profileid" value="new" /> ';
		}

		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Next" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editprofiles" />
		   </form>';
