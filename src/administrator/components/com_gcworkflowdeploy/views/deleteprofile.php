<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_delete_profile").'</h2>';		
		$htmlvar .= '
		   <form action="index.php" method="POST">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
	    if ($pageVars["curstage"]==1) {
			$htmlvar .= '<h2>Are you sure you want to delete this?</h2>';
	    } else if ($pageVars["curstage"]==2) {
			$htmlvar .= '<h2>Are you ABSOLUTELY sure you want to delete this?</h2>';
	    } else if ($pageVars["curstage"]==3) {
			$htmlvar .= '<h2>You have just deleted:</h2>';
	    }
		
		$htmlvar .= '<table>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><h3>ID</h3></td>';
		$htmlvar .= '		<td><h3>Title</h3></td>';
		$htmlvar .= '		<td><h3>Description</h3></td>';
		$htmlvar .= '		<td><h3>Unique ID</h3></td>';
		$htmlvar .= '		<td><h3>Files</h3></td>';
		$htmlvar .= '		<td><h3>Tables/Actions</h3></td>';		
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>'.$pageVars["profile"]["id"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["profile"]["profile_title"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["profile"]["profile_description"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["profile"]["profile_uniqueid"].'</p></td>';
		$htmlvar .= '		<td><p>';
		if ( count($pageVars["profile"]["filefolders"])>0 ) {
			foreach ($pageVars["profile"]["filefolders"] as $filefolder) {
				$htmlvar .= $filefolder;
			}
		} else {
			$htmlvar .= "No files included in this Profile";		
		}
		$htmlvar .= '		</p></td>';
		$htmlvar .= '		<td><p>';
		if ( count($pageVars["profile"]["datatables"])>0 ) {
			foreach ($pageVars["profile"]["datatables"] as $datatable) {
				$htmlvar .= $datatable;
			}
		} else {
			$htmlvar .= "No Data tables included in this Profile";		
		}
		$htmlvar .= '		</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '</table>';
		
		if ($pageVars["curstage"]==3) {
			$htmlvar .= '
			    <p style="text-align:center;">
			     <input type="submit" name="submit" class="gcbutton" value="Return to Home Page" />
			    </p>
			    <input type="hidden" name="run" id="run" value="1" />
			    <input type="hidden" name="profileid" id="profileid" value="'.$pageVars["profile"]["id"].'" />
			    <input type="hidden" name="stage" id="stage" value="'.$pageVars["newstage"].'" />
			    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
			    <input type="hidden" name="task" id="task" value="deleteprofile" />
			   </form>';
		} else {
			$htmlvar .= '
			    <p style="text-align:center;">
			     <input type="submit" name="submit" class="gcbutton" value="Delete Profile" />
			    </p>
			    <input type="hidden" name="run" id="run" value="1" />
			    <input type="hidden" name="profileid" id="profileid" value="'.$pageVars["profile"]["id"].'" />
			    <input type="hidden" name="stage" id="stage" value="'.$pageVars["newstage"].'" />
			    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
			    <input type="hidden" name="task" id="task" value="deleteprofile" />
			   </form>';
		}