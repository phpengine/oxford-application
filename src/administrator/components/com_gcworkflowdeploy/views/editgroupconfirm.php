<?php

		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_verify_group_info").'</h2>';
		$htmlvar .= ' <form action="index.php" method="POST">';
		   
    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
      foreach($pageVars["messages"] as $message) {
        $htmlvar .= '<p class="appMessage">'.$message.'</p>';
      }
    }

		$htmlvar .= '<table>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<th><h3>Name</h3></th>';
		$htmlvar .= '		<th><h3>Value</h3></th>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Group Title:</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["groupdetails"]["group_name"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Group Description:</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["groupdetails"]["group_desc"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Group Unique ID</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["groupdetails"]["uniqueid"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>Group Entries:</p></td>';
		if ( count($pageVars["groupentries"]) > 0 ) {
			$htmlvar .= '		<td>';
			foreach($pageVars["groupentries"] as $g_entry) {
				$htmlvar .= '			<p>'.$g_entry["ordering"].' - '.$g_entry["entry_type"].' - '.$g_entry["target"].'</p>'; }
			$htmlvar .= '		</td>'; }
		else {
			$htmlvar .= '		<td><p>No Files</p></td>'; }
		$htmlvar .= '	</tr>';
		$htmlvar .= '</table>';
		
		$htmlvar .= ' <input type="hidden" name="groupid" id="groupid" value="'.$pageVars["groupdetails"]["id"].'" /> ';
		$htmlvar .= ' <input type="hidden" name="groupuniqueid" id="groupuniqueid" value="'.$pageVars["groupdetails"]["uniqueid"].'" /> ';
		
		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" value="Return to Home" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="home" />
		   </form>';
