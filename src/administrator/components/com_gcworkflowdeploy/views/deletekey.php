<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_delete_key").'</h2>';		
		$htmlvar .= '
		   <form action="index.php" method="POST">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
	    if ($pageVars["curstage"]==1) {
			$htmlvar .= '<h2>Are you sure you want to delete this?</h2>';
	    } else if ($pageVars["curstage"]==2) {
			$htmlvar .= '<h2>Are you ABSOLUTELY sure you want to delete this?</h2>';
	    } else if ($pageVars["curstage"]==3) {
			$htmlvar .= '<h2>You have just deleted:</h2>';
	    }
		
		$htmlvar .= '<table>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>'.$pageVars["key"]["id"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["key"]["key_title"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["key"]["key_desc"].'</a></p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["key"]["key_key"].'</p></td>';
		$htmlvar .= '	</tr>';
		$htmlvar .= '</table>';
		
		if ($pageVars["curstage"]==3) {
		
			$htmlvar .= '
			    <p style="text-align:center;">
			     <input type="submit" name="submit" class="gcbutton" value="Return to Home Page" />
			    </p>
			    <input type="hidden" name="run" id="run" value="1" />
			    <input type="hidden" name="keyid" id="keyid" value="'.$pageVars["key"]["id"].'" />
			    <input type="hidden" name="stage" id="stage" value="'.$pageVars["newstage"].'" />
			    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
			    <input type="hidden" name="task" id="task" value="deletekey" />
			   </form>';
		} else {

			$htmlvar .= '
			    <p style="text-align:center;">
			     <input type="submit" name="submit" class="gcbutton" value="Delete Key" />
			    </p>
			    <input type="hidden" name="run" id="run" value="1" />
			    <input type="hidden" name="keyid" id="keyid" value="'.$pageVars["key"]["id"].'" />
			    <input type="hidden" name="stage" id="stage" value="'.$pageVars["newstage"].'" />
			    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
			    <input type="hidden" name="task" id="task" value="deletekey" />
			   </form>';
		
		}
		
