<?php
		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_config").'</h2>';
    $htmlvar .= '<p>'.$this->configs->give("config_intro_text").'</p>';
    $htmlvar .= '<p class="configurationNotice">'.$this->configs->give("config_intro_defaults_notice_text").'</p>';
		$htmlvar .= '<form action="index.php" method="POST">';

		if (isset($pageVars["uservar"]) && is_array($pageVars["uservar"]) && count($pageVars["uservar"])>0 ) {
			$htmlvar .= ' <table>';
			foreach ($pageVars["uservar"] as $uservar) {
				if ($uservar["type"]=="text") {
					$htmlvar .= '<tr>';
					$htmlvar .= ' <td><p>'.$uservar["title"].'</p></td>';
					$htmlvar .= ' <td><input class="configTextField" type="text" name="conf_'.$uservar["idString"].'" id="conf_'.$uservar["idString"].'"';
					if ($uservar["curValue"]!="novalue") {
 						$htmlvar .= ' value="'.$uservar["curValue"].'"'; }
					$htmlvar .= '  /></td>';
					$htmlvar .= ' <td><p class="leftAlignText">'.$uservar["description"].'</p></td>';
					$htmlvar .= '</tr>'; } }
			$htmlvar .= ' </table>'; }

		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Submit" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="configure" />
		   </form>';
