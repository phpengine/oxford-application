<?php
		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_edit_profile_data_tables").'</h2>';
		
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
		$htmlvar .= '<form action="index.php" method="POST">';
		$htmlvar .= ' <div class="contentWrap">';
		$htmlvar .= '  <div class="contentTitle">';
		$htmlvar .= '   <div class="titleLeft">';
		$htmlvar .= '    <div class="titleLeft1">';
		$htmlvar .= '     <p>Title</p></td>';
		$htmlvar .= '     <p>'.$pageVars["profiledetails"]["profile_title"].'</p>';
		$htmlvar .= '    </div> <!-- end titleLeft1 -->';
		$htmlvar .= '    <div class="titleLeft2">';
		$htmlvar .= '     <p>Description</p></td>';
		$htmlvar .= '     <p>'.$pageVars["profiledetails"]["profile_description"].'</p>';
		$htmlvar .= '    </div> <!-- end titleLeft2 -->';
		$htmlvar .= '   </div> <!-- end titleLeft -->';
		$htmlvar .= '   <div class="titleRight">';
		$htmlvar .= '   </div> <!-- end titleRight -->';
		$htmlvar .= '  </div> <!-- end contentTitle -->';
		$htmlvar .= '  <div class="contentBody">';
		
		$htmlvar .= '<div class="datatableHolderWrapWrap">';
		$htmlvar .= '<div class="datatableHolderWrap">';
		$htmlvar .= ' <div class="datatableHolderWrapTitle">';
		$htmlvar .= '   <h3>Choose which Data Tables to add</h3>';
		$htmlvar .= ' </div>';
		$htmlvar .= '<div class="datatableHolderWrapBody">';
		$htmlvar .= '<div class="datatableHolderWrapBodyInnerWide">';
		
		$htmlvar .= '<div class="datatableholder">';
		$htmlvar .= ' <div class="datatableholderTitle">';
		$htmlvar .= '  <h3>Level 1</h3>';		
		$htmlvar .= ' </div> <!-- end datatableholderTitle -->';
		$htmlvar .= '<div class="datatableholderBody">';
		
		$htmlvar .= '	<ul>';
		$itable = 0;
        $tableCountsInProfile = array_count_values($pageVars["datatablescurrentonlynames"]);
		foreach ($pageVars["datatables"] as $table ) {
				$htmlvar .= '<li class="chkbline">';
				$htmlvar .= ' <p class="minimargins">';
                if (isset($tableCountsInProfile[$table])) {
                    $htmlvar .= '  <input type="text" name="table_'.$table.'" id="table_'.$table.'" ' .
                        'value="'.$tableCountsInProfile[$table].'" ></input>'; }
                else {
                    $htmlvar .= '  <input type="text" name="table_'.$table.'" id="table_'.$table.'" ' .
                        'value="" ></input>'; }
				$htmlvar .= ' '.$table;
				$htmlvar .= ' </p>';
				$htmlvar .= '</li>';
				$itable++ ; }
		$htmlvar .= '	</ul>';
		$htmlvar .= '<input type="hidden" name="tablecount" id="tablecount" value="'.$pageVars["tablecount"].'" /> ';			

		$htmlvar .= '</div> <!-- end datatableHolderBody --> ';
		$htmlvar .= '</div> <!-- end datatableholder --> ';
		$htmlvar .= '</div> <!-- end datatableHolderBodyInnerWide -->';
		$htmlvar .= '</div> <!-- end datatableHolderWrap -->';
		$htmlvar .= '</div> <!-- end datatableHolderWrap -->';
		
		$htmlvar .= '<div class="datatableHolderWrap">';
		$htmlvar .= ' <div class="datatableHolderWrapTitle">';
		$htmlvar .= '   <h3>The following tables are currently added:</h3>';
		$htmlvar .= ' </div>';
		$htmlvar .= '<div class="datatableHolderWrapBody">';
		
		$htmlvar .= '	<ul>';
		$itable = 0;
		foreach ($pageVars["datatablescurrent"] as $table ) {
				$htmlvar .= '<li class="chkbline">';
				$htmlvar .= ' <p class="minimargins">';
				$htmlvar .= ' '.$table["profile_datatable_tablename"];
				$htmlvar .= ' </p>';
				$htmlvar .= '</li>';
				$itable++;
		}
		$htmlvar .= '	</ul>';
		$htmlvar .= '</div> <!-- end datatableHolderWrapBody -->';
		$htmlvar .= '</div> <!-- end datatableHolderWrap -->';
		
		$htmlvar .= '  </div> <!-- end contentBody -->';
		$htmlvar .= '<input type="hidden" name="profileid" id="profileid" value="'.$pageVars["profiledetails"]["id"].'" /> ';
		$htmlvar .= '<input type="hidden" name="profileuniqueid" id="profileuniqueid" value="'.$pageVars["profiledetails"]["profile_uniqueid"].'" /> ';				

		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Go To Next Stage" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editprofiledatatables" />
		   </form>';
		$htmlvar .= '</div> <!-- end contentwrap -->';
