<?php

		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_home").'</h2>';
		$htmlvar .= '<p>';
		$htmlvar .= $this->configs->give("home_intro_text");
		$htmlvar .= '</p>';		
		$htmlvar .= '
		   <form action="index.php" method="POST">';
		   
    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
      foreach($pageVars["messages"] as $message) {
        $htmlvar .= '<p class="appMessage">'.$message.'</p>'; } }
	    
		if (count($pageVars["keys"])>0 ) {
      $htmlvar .= '<h3>Keys Configured: Click <a href="index.php?option=com_gcworkflowdeploy&task=mankeys">';
      $htmlvar .= 'here</a> to manage.</h3>';
			$htmlvar .= '<table>';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<th><h3>ID</h3></th>';
			$htmlvar .= '		<th><h3>Title</h3></th>';
			$htmlvar .= '		<th><h3>Description</h3></th>';
			$htmlvar .= '	</tr>';
			foreach ( $pageVars["keys"] as $key ) {
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<td><p>'.$key["id"].'</p></td>';
				$htmlvar .= '		<td><p>'.$key["key_title"].'</p></td>';
				$htmlvar .= '		<td><p>'.$key["key_desc"].'</p></td>';
				$htmlvar .= '	</tr>'; }
			$htmlvar .= '</table>'; }
    else {
			$htmlvar .= '<h3>No Keys Configured. Click <a href="index.php?option=com_gcworkflowdeploy&task=mankeys">';
      $htmlvar .= 'here</a> to configure.</h3>'; }

    if (count($pageVars["servers"])>0 ) {
      $htmlvar .= '<h3>Servers Configured: Click <a href="index.php?option=com_gcworkflowdeploy&task=manservers">';
      $htmlvar .= 'here</a> to manage.</h3>';
      $htmlvar .= '<table>';
      $htmlvar .= '	<tr>';
      $htmlvar .= '		<th>ID</th>';
      $htmlvar .= '		<th><h3>Title</h3></th>';
      $htmlvar .= '		<th><h3>Description</h3></th>';
      $htmlvar .= '		<th><h3>Web Site URL</h3></th>';
      $htmlvar .= '		<th><h3>Web Service URL</h3></th>';
      $htmlvar .= '	</tr>';
      foreach ( $pageVars["servers"] as $server ) {
        $htmlvar .= '	<tr>';
        $htmlvar .= '		<td><p>'.$server["id"].'</p></td>';
        $htmlvar .= '		<td><p>'.$server["serv_title"].'</p></td>';
        $htmlvar .= '		<td><p>'.$server["serv_desc"].'</p></td>';
        $htmlvar .= '		<td><p>'.$server["serv_website_url"].'</p></td>';
        $htmlvar .= '		<td><p>'.$server["serv_webservice_url"].'</p></td>';
        $htmlvar .= '	</tr>'; }
      $htmlvar .= '</table>'; }
    else {
      $htmlvar .= '<h3>No Servers Configured. Click <a href="index.php?option=com_gcworkflowdeploy&task=manservers">';
      $htmlvar .= 'here</a> to configure.</h3>'; }

    if (count($pageVars["groups"])>0 ) {
      $htmlvar .= '<h3>Feature Groups Configured: Click <a href="index.php?option=com_gcworkflowdeploy&task=mangroups">';
      $htmlvar .= 'here</a> to manage.</h3>';
      $htmlvar .= '<table>';
      $htmlvar .= '	<tr>';
      $htmlvar .= '		<th>ID</th>';
      $htmlvar .= '		<th><h3>Name</h3></th>';
      $htmlvar .= '		<th><h3>Description</h3></th>';
      $htmlvar .= '	</tr>';
      foreach ( $pageVars["groups"] as $group ) {
        $htmlvar .= '	<tr>';
        $htmlvar .= '		<td><p>'.$group["id"].'</p></td>';
        $htmlvar .= '		<td><p>'.$group["group_name"].'</p></td>';
        $htmlvar .= '		<td><p>'.$group["group_desc"].'</p></td>';
        $htmlvar .= '	</tr>';
      }
      $htmlvar .= '</table>'; }
    else {
      $htmlvar .= '<h3>No Feature Groups Configured. Click <a href="index.php?option=com_gcworkflowdeploy&task=mangroups">';
      $htmlvar .= 'here</a> to configure.</h3>'; }

    if (count($pageVars["profiles"])>0 ) {
      $htmlvar .= '<h3>Feature Profiles Configured: Click <a href="index.php?option=com_gcworkflowdeploy&task=manprofiles">';
      $htmlvar .= 'here</a> to manage.</h3>';
      $htmlvar .= '<table width="100%">';
      $htmlvar .= '	<tr>';
      $htmlvar .= '		<th><h3>Title</h3></th>';
      $htmlvar .= '		<th><h3>Description</h3></th>';
      $htmlvar .= '	</tr>';
      foreach ( $pageVars["profiles"] as $profile ) {
        $htmlvar .= '	<tr>';
        $htmlvar .= '		<td><p>'.$profile["profile_title"].'</p></td>';
        $htmlvar .= '		<td><p>'.$profile["profile_description"].'</p></td>';
        $htmlvar .= '	</tr>';
      }
      $htmlvar .= '</table>'; }
    else {
      $htmlvar .= '<h3>No Feature Profiles Configured. Click <a href="index.php?option=com_gcworkflowdeploy&task=manprofiles">';
      $htmlvar .= 'here</a> to configure.</h3>'; }
