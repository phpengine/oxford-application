<?php
		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_edit_groups").'</h2>';
		
    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
      foreach($pageVars["messages"] as $message) {
        $htmlvar .= '<p class="appMessage">'.$message.'</p>'; } }
	    
		$htmlvar .= '<form action="index.php" method="POST">';
		$htmlvar .= ' <table>';
		
		if ($pageVars["edit"]==1) {
			$htmlvar .= '<tr>';
			$htmlvar .= ' <td><p>ID</p></td>';
			$htmlvar .= ' <td><p style="text-align:left;">'.$pageVars["groupdetails"]["id"].'</p></td>';
			$htmlvar .= '</tr>'; }

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Title</p></td>';
		$htmlvar .= ' <td><input type="text" name="group_name" value="';
		if ( isset($pageVars["groupdetails"]["group_name"]) ) {$htmlvar .= $pageVars["groupdetails"]["group_name"]; }
		$htmlvar .= '"></input></td>';
		$htmlvar .= '</tr>';

		$htmlvar .= '<tr>';
		$htmlvar .= ' <td><p>Description</p></td>';
		$htmlvar .= ' <td><textarea name="group_desc" >';
		if ( isset($pageVars["groupdetails"]["group_desc"]) ) {$htmlvar .= $pageVars["groupdetails"]["group_desc"]; }
		$htmlvar .= '</textarea></td>';
		$htmlvar .= '</tr>';

    if ($pageVars["edit"]==1) {
      $htmlvar .= '<tr>';
      $htmlvar .= ' <td><p>Unique ID</p></td>';
      $htmlvar .= ' <td><p style="text-align:left;">'.$pageVars["groupdetails"]["uniqueid"].'</p></td>';
      $htmlvar .= '</tr>'; }

		$htmlvar .= ' </table>';
		
		if ($pageVars["edit"]==1) {
		   $htmlvar .= ' <input type="hidden" name="groupid" id="groupid" value="'.$pageVars["groupdetails"]["id"].'" /> ';
		   $htmlvar .= ' <input type="hidden" name="groupuniqueid" id="groupuniqueid" value="'.$pageVars["groupdetails"]["uniqueid"].'" /> '; }
    else {
		   $htmlvar .= ' <input type="hidden" name="groupid" id="groupid" value="new" /> '; }

		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Next" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editgroups" />
		   </form>';
