<?php

		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_manage_profiles").'</h2>';	
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
		
		if (count($pageVars["profiles"])>0 ) {
			$htmlvar .= '<table width="100%">';
			$htmlvar .= '	<tr>';
			$htmlvar .= '		<th><h3>Title</h3></th>';
			$htmlvar .= '		<th><h3>Description</h3></th>';
			$htmlvar .= '		<th></th>';
			$htmlvar .= '	</tr>';
			foreach ( $pageVars["profiles"] as $profile ) {
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<td><p>'.$profile["profile_title"].'</p></td>';
				$htmlvar .= '		<td><p>'.$profile["profile_description"].'</p></td>';
				$htmlvar .= '		<td>';
				$htmlvar .= '
				   <form action="index.php" method="POST">
				    <p style="text-align:center;">
				     <input type="submit" name="submit" class="gcbutton" value="Edit This Profile" />
				    </p>
				    <input type="hidden" name="profileid" id="profileid" value="'.$profile["id"].'" />
				    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
				    <input type="hidden" name="task" id="task" value="editprofiles" />
				   </form>';
				$htmlvar .= '		</td>';
				$htmlvar .= '		<td>';
				$htmlvar .= '
				   <form action="index.php" method="POST">
				    <p style="text-align:center;">
				     <input type="submit" name="submit" class="gcbutton" value="Delete This Profile" />
				    </p>
				    <input type="hidden" name="profileid" id="profileid" value="'.$profile["id"].'" />
				    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
				    <input type="hidden" name="task" id="task" value="deleteprofile" />
				    <input type="hidden" name="stage" id="stage" value="1" />
				    <input type="hidden" name="run" id="run" value="1" />
				   </form>';
				$htmlvar .= '		</td>';
				$htmlvar .= '	</tr>';
			}
			$htmlvar .= '</table>';
		} else {
			$htmlvar .= '<h3>No Profiles Added Yet</h3>';
		}
			
		$htmlvar .= '
		   <form action="index.php" method="POST">
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Create New Profile" />
		    </p>
			<input type="hidden" name="profileid" id="profileid" value="new" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editprofiles" />
		   </form>';