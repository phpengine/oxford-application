<?php

		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_manage_keys").'</h2>';	
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
		
		if (count($pageVars["keys"])>0 ) {
			$htmlvar .= '<table width="100%">';
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<th><h3>Title</h3></th>';
				$htmlvar .= '		<th><h3>Description</h3></th>';
				$htmlvar .= '		<th><h3>Auth Level</h3></th>';
				$htmlvar .= '		<th></th>';
				$htmlvar .= '	</tr>';
			foreach ( $pageVars["keys"] as $key ) {
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<td><p>'.$key["key_title"].'</p></td>';
				$htmlvar .= '		<td><p>'.$key["key_desc"].'</p></td>';
				$htmlvar .= '		<td><p>'.$key["key_authlevel"].'</p></td>';
				$htmlvar .= '		<td>';
				$htmlvar .= '
				   <form action="index.php" method="POST">
				    <p style="text-align:center;">
				     <input type="submit" name="submit" class="gcbutton" value="Edit This Key" />
				    </p>
				    <input type="hidden" name="keyid" id="keyid" value="'.$key["id"].'" />
				    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
				    <input type="hidden" name="task" id="task" value="editkeys" />
				   </form>';
				$htmlvar .= '		</td>';
				$htmlvar .= '		<td>';
				$htmlvar .= '
				   <form action="index.php" method="POST">
				    <p style="text-align:center;">
				     <input type="submit" name="submit" class="gcbutton" value="Delete This Key" />
				    </p>
				    <input type="hidden" name="keyid" id="keyid" value="'.$key["id"].'" />
				    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
				    <input type="hidden" name="task" id="task" value="deletekey" />
				    <input type="hidden" name="stage" id="stage" value="1" />
				    <input type="hidden" name="run" id="run" value="1" />
				   </form>';
				$htmlvar .= '		</td>';
				$htmlvar .= '	</tr>';
			}
			$htmlvar .= '</table>';
		} else {
			$htmlvar .= '<h3>No Keys Added Yet</h3>';
		}
		
		$htmlvar .= '
		   <form action="index.php" method="POST">
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="New Key" />
		    </p>
			<input type="hidden" name="keyid" id="keyid" value="new" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editkeys" />
		   </form>';