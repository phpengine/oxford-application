<?php
		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_edit_profile_files").'</h2>';
		
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
		$htmlvar .= '<form action="index.php" method="POST">';
		$htmlvar .= ' <div class="contentWrap">';
		$htmlvar .= '  <div class="contentTitle">';
		$htmlvar .= '   <div class="titleLeft">';
		$htmlvar .= '    <div class="titleLeft1">';
		$htmlvar .= '     <p>Title</p></td>';
		$htmlvar .= '     <p>'.$pageVars["profiledetails"]["profile_title"].'</p>';
		$htmlvar .= '    </div> <!-- end titleLeft1 -->';
		$htmlvar .= '    <div class="titleLeft2">';
		$htmlvar .= '     <p>Description</p></td>';
		$htmlvar .= '     <p>'.$pageVars["profiledetails"]["profile_description"].'</p>';
		$htmlvar .= '    </div> <!-- end titleLeft2 -->';
		$htmlvar .= '   </div> <!-- end titleLeft -->';
		$htmlvar .= '   <div class="titleRight">';
		$htmlvar .= '    <input type="button" class="gcbutton" value="Add This File/Folder" onclick="addThisValue()"></input>';
		$htmlvar .= '   </div> <!-- end titleRight -->';
		$htmlvar .= '  </div> <!-- end contentTitle -->';
		$htmlvar .= '  <div class="contentBody">';
		
		$htmlvar .= '<div class="currentFilesWrap">';
		$htmlvar .= '   <h3>Current Files:</h3>';
		foreach ($pageVars["currentfiles"] as $currentfile ) {
			    // break this variable up on the space so that it break over lines
			    $currentfiletodisplay = str_replace("/", "/ ", $currentfile["profile_file_path"]);
				$htmlvar .= '<div class="entryrow"><div class="left"><input type="button" class="gcbuttonsml" value="X" onclick=\'deleteThisValue("'.$currentfile["profile_file_path"].'");\' /></div><div class="right"><p>'.$currentfiletodisplay.'</p></div></div>';	
		}
		$htmlvar .= '</div> <!-- currentFilesWrap -->';
		
		$htmlvar .= '<div class="checkboxHolderWrap">';
		$htmlvar .= ' <div class="checkboxHolderWrapTitle">';
		$htmlvar .= '   <h3>Choose a File or Folder to Add</h3>';
		$htmlvar .= ' </div>';
		$htmlvar .= '<div class="checkboxHolderWrapBody">';
		$htmlvar .= '<div class="checkboxHolderWrapBodyInnerWide">';
		
		$htmlvar .= '<div class="checkboxholder" id="checkboxHolder1">';
		$htmlvar .= ' <div class="checkboxholderTitle">';
		$htmlvar .= '  <h3>Level 1</h3>';		
		$htmlvar .= ' </div> <!-- end checkboxholderTitle -->';
		$htmlvar .= '<div class="checkboxholderBody">';
		$htmlvar .= '	<select name="fileroot" size="15" class="filechooser" id="filechooser1" onchange="chooserChange(1);">';
		foreach ($pageVars["allfiles"] as $file ) {
			if ($file["type"]=="file") {
				$htmlvar .= '<option class="chkbline file" value="'.$file["name"].'">'.$file["name"];
				$htmlvar .= '</option>';
			} else if ($file["type"]=="dir"){
				$htmlvar .= '<option class="chkbline dir" value="'.$file["name"].'">'.$file["name"];
				$htmlvar .= '</option>';
			}
		}
		$htmlvar .= '	</select>';
		$htmlvar .= '</div> <!-- end checkboxHolderBody --> ';
		$htmlvar .= '</div> <!-- end checkboxholder --> ';
		
		$htmlvar .= '</div> <!-- end checkboxHolderBodyInnerWide -->';
		$htmlvar .= '  </div> <!-- end checkBoxHolderWrap -->';
		$htmlvar .= '  </div> <!-- end contentBody -->';
		
		$htmlvar .= '<input type="hidden" name="jsfilenamelevel" id="jsfilenamelevel" value="1" /> ';	
		$htmlvar .= '<input type="hidden" name="profileid" id="profileid" value="'.$pageVars["profiledetails"]["id"].'" /> ';
	    $htmlvar .= '<input type="hidden" name="profileuniqueid" id="profileuniqueid" value="'.$pageVars["profiledetails"]["profile_uniqueid"].'" /> ';				

		$htmlvar .= '
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Go To Next Stage" />
		    </p>
		    <input type="hidden" name="run" id="run" value="1" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editprofilefiles" />
		   </form>';
		$htmlvar .= '</div> <!-- end contentwrap -->';
					
		$htmlvar .= '  <div class="hiddenloader"> ';
		$htmlvar .= '  </div> <!-- end hiddenloader -->';
		$htmlvar .= '  <div class="hiddenloader2"> ';
		$htmlvar .= '  </div> <!-- end hiddenloader -->';
		$htmlvar .= '  <div class="hiddenloader3"> ';
		$htmlvar .= '  </div> <!-- end hiddenloader -->';
		