<?php
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_delete_server").'</h2>';		
		$htmlvar .= '
		   <form action="index.php" method="POST">';
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
	    
	    if ($pageVars["curstage"]==1) {
			$htmlvar .= '<h2>Are you sure you want to delete this?</h2>';
	    } else if ($pageVars["curstage"]==2) {
			$htmlvar .= '<h2>Are you ABSOLUTELY sure you want to delete this?</h2>';
	    } else if ($pageVars["curstage"]==3) {
			$htmlvar .= '<h2>You have just deleted:</h2>';
	    }
		
		$htmlvar .= '<table>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><h3>ID</h3></td>';
		$htmlvar .= '		<td><h3>Title</h3></td>';
		$htmlvar .= '		<td><h3>Description</h3></td>';
		$htmlvar .= '		<td><h3>Web URL</h3></td>';
		$htmlvar .= '		<td><h3>Service URL</h3></td>';
		$htmlvar .= '		<td><h3>Upload Type</h3></td>';
		
		if ($pageVars["server"]["serv_upload_type"]=="SSH") {
			$htmlvar .= '		<td><h3>SSH User</h3></td>';
			$htmlvar .= '		<td><h3>SSH Pass</h3></td>';	
			$htmlvar .= '		<td><h3>SSH Folder</h3></td>';	
		} else if ($pageVars["server"]["serv_upload_type"]=="FTP") {
			$htmlvar .= '		<td><h3>SSH User</h3></td>';
			$htmlvar .= '		<td><h3>FTP Pass</h3></td>';	
			$htmlvar .= '		<td><h3>FTP Folder</h3></td>';		
		}
		
		$htmlvar .= '	</tr>';
		$htmlvar .= '	<tr>';
		$htmlvar .= '		<td><p>'.$pageVars["server"]["id"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_title"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_desc"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_website_url"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_webservice_url"].'</p></td>';
		$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_upload_type"].'</p></td>';
		
		if ($pageVars["server"]["serv_upload_type"]=="SSH") {
			$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_ssh_user"].'</p></td>';
			$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_ssh_pass"].'</p></td>';	
			$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_ssh_folder"].'</p></td>';	
		} else if ($pageVars["server"]["serv_upload_type"]=="FTP") {
			$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_ftp_user"].'</p></td>';
			$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_ftp_pass"].'</p></td>';	
			$htmlvar .= '		<td><p>'.$pageVars["server"]["serv_ftp_folder"].'</p></td>';		
		}
		
		$htmlvar .= '	</tr>';
		$htmlvar .= '</table>';
		
		if ($pageVars["curstage"]==3) {
			$htmlvar .= '
			    <p style="text-align:center;">
			     <input type="submit" name="submit" class="gcbutton" value="Return to Home Page" />
			    </p>
			    <input type="hidden" name="run" id="run" value="1" />
			    <input type="hidden" name="serverid" id="serverid" value="'.$pageVars["server"]["id"].'" />
			    <input type="hidden" name="stage" id="stage" value="'.$pageVars["newstage"].'" />
			    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
			    <input type="hidden" name="task" id="task" value="deleteserver" />
			   </form>';
		} else {
			$htmlvar .= '
			    <p style="text-align:center;">
			     <input type="submit" name="submit" class="gcbutton" value="Delete Server" />
			    </p>
			    <input type="hidden" name="run" id="run" value="1" />
			    <input type="hidden" name="serverid" id="serverid" value="'.$pageVars["server"]["id"].'" />
			    <input type="hidden" name="stage" id="stage" value="'.$pageVars["newstage"].'" />
			    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
			    <input type="hidden" name="task" id="task" value="deleteserver" />
			   </form>';
		}