<?php

		$this->openPage();
		$htmlvar = '';
		$htmlvar .= '<h2>'.$this->configs->give("subtitle_manage_servers").'</h2>';	
		   
	    if ( isset($pageVars["messages"]) && count($pageVars["messages"])>0 ) {
	    	foreach($pageVars["messages"] as $message) {
	    		$htmlvar .= '<p class="appMessage">'.$message.'</p>';
	    	}
	    }
		
		if (count($pageVars["servers"])>0 ) {
			$htmlvar .= '<table width="100%">';
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<th><h3>Title</h3></th>';
				$htmlvar .= '		<th><h3>Description</h3></th>';
				$htmlvar .= '		<th><h3>Webservice URL</h3></th>';
				$htmlvar .= '		<th><h3>Website URL</h3></th>';
				$htmlvar .= '		<th></th>';
				$htmlvar .= '	</tr>';
			foreach ( $pageVars["servers"] as $server ) {
				$htmlvar .= '	<tr>';
				$htmlvar .= '		<td><p>'.$server["serv_title"].'</p></td>';
				$htmlvar .= '		<td><p>'.$server["serv_desc"].'</p></td>';
				$htmlvar .= '		<td><p>'.$server["serv_webservice_url"].'</p></td>';
				$htmlvar .= '		<td><p>'.$server["serv_website_url"].'</p></td>';
				$htmlvar .= '		<td>';
				$htmlvar .= '
				   <form action="index.php" method="POST">
				    <p style="text-align:center;">
				     <input type="submit" name="submit" class="gcbutton" value="Edit This Server" />
				    </p>
				    <input type="hidden" name="serverid" id="serverid" value="'.$server["id"].'" />
				    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
				    <input type="hidden" name="task" id="task" value="editservers" />
				   </form>';
				$htmlvar .= '		</td>';
				$htmlvar .= '		<td>';
				$htmlvar .= '
				   <form action="index.php" method="POST">
				    <p style="text-align:center;">
				     <input type="submit" name="submit" class="gcbutton" value="Delete This Server" />
				    </p>
				    <input type="hidden" name="serverid" id="serverid" value="'.$server["id"].'" />
				    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
				    <input type="hidden" name="task" id="task" value="deleteserver" />
				    <input type="hidden" name="stage" id="stage" value="1" />
				    <input type="hidden" name="run" id="run" value="1" />
				   </form>';
				$htmlvar .= '		</td>';
				$htmlvar .= '	</tr>';
			}
			$htmlvar .= '</table>';
		} else {
			$htmlvar .= '<h3>No Servers Added Yet</h3>';
		}
			
		$htmlvar .= '
		   <form action="index.php" method="POST">
		    <p style="text-align:center;">
		     <input type="submit" name="submit" class="gcbutton" value="Create New Server" />
		    </p>
			<input type="hidden" name="serverid" id="serverid" value="new" />
		    <input type="hidden" name="option" id="option" value="'.$this->configs->give("com_name").'" />
		    <input type="hidden" name="task" id="task" value="editservers" />
		   </form>';