<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'modelClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'viewClass.php';

class GCWorkflowDeployerBEControlClass {
	
	private $configs;
	private $view;
	private $model;
	private $messages;
	private $loginstatus;

	function __construct(){
		$this->configs = new GCWorkflowDeployerConfigClass();
		$this->view = new GCWorkflowDeployerBEViewClass();
		$this->model = new GCWorkflowDeployerModelClass();
		$this->messages = array(); }

	public function pageHome() {
		include('controllers/home.php'); }

  public function pageDeleteGroup() {
    include('controllers/deletegroup.php'); }

  public function pageDeleteKey() {
    include('controllers/deletekey.php'); }

	public function pageDeleteServer() {
		include('controllers/deleteserver.php'); }

	public function pageDeleteProfile() {
		include('controllers/deleteprofile.php'); }

	public function pageConfig() {
		include('controllers/config.php'); }

  public function pageManageGroups() {
    include('controllers/managegroups.php'); }

  public function pageManageKeys() {
    include('controllers/managekeys.php'); }

	public function pageManageServers() {
		include('controllers/manageservers.php'); }

	public function pageManageProfiles() {
		include('controllers/manageprofiles.php'); }

  public function pageEditGroups() {
    include('controllers/editgroups.php'); }

  public function pageEditGroupEntries() {
    include('controllers/editgroupentries.php'); }

  public function pageEditGroupConfirm() {
    include('controllers/editgroupconfirm.php'); }

  public function pageEditKeys() {
    include('controllers/editkeys.php'); }

  public function pageEditServers() {
		include('controllers/editservers.php'); }
	
	public function pageEditProfiles() {
		include('controllers/editprofiles.php'); }
	
	public function pageEditProfileFiles() {
		include('controllers/editprofilefiles.php'); }
	
	public function pageEditProfileDataTables() {
		include('controllers/editprofiledatatables.php'); }
	
	public function pageEditProfileDataTableActions() {
		include('controllers/editprofiledatatableactions.php'); }
	
	public function pageEditProfileConfirm() {
		include('controllers/editprofileconfirm.php'); }
	
	public function pageServiceFile() {
		include('controllers/servicefile.php'); }

  public function pageServiceDBAction() {
    include('controllers/servicedbaction.php'); }

  public function pageServiceGroupEntry() {
    include('controllers/servicegroupentry.php'); }
	
}
