<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'modelClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'viewClass.php';

class GCWorkflowDeployerPushClass {
	
	private $configs;
	private $view;
	private $model;
	private $messages;
	private $loginstatus;
	private $tempfolder;
	private $pushid;
	private $pushdetails;
	private $folderroot;
	private $foldername;
	private $archivename;
	private $siteroot;
	private $ziperrors;

	function __construct($pushid){
		$this->configs = new GCWorkflowDeployerConfigClass();
		$this->view = new GCWorkflowDeployerBEViewClass();
		$this->model = new GCWorkflowDeployerModelClass();
		$this->pushid = $pushid;
		$this->pushdetails = $this->model->getSinglePushDetails($pushid);
		$this->siteroot = $this->getRootFolderToCalculate();
		if (substr($this->siteroot, "-1", "1") != "/") {
			$this->siteroot .= DS;
		}
		$this->folderroot = $this->configs->give("temp_push_folder");
		if (substr($this->folderroot, "-1", "1") != "/") {
			$this->folderroot .= DS;
		}
		$this->foldername = $this->pushdetails["push_profile_uniqueid"].'_'.$this->pushdetails["push_time"].DS; 
		$this->archivename = $this->pushdetails["push_profile_uniqueid"].'_'.$this->pushdetails["push_time"].'.zip';
		$this->ziperrors = array(); 
	}

	public function statusUpdate($value) {	
	    //initialize page
	    $this->model->setUpdatePush($this->pushid, "push_status", $value);
	    return "OK";
	}

	public function doAuthTest() {
		$server = $this->model->getSingleServerDetails($this->pushdetails["push_serv_id"]);
		// echo "serv-service:".$server["serv_webservice_url"];
	    $ch = curl_init($server["serv_webservice_url"]);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $post = array(
	        "key"=>$server["serv_key"],
	    	"action"=>"auth",
	    	"tmpl"=>"component"
	    );
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
        $output = curl_exec($ch);  
        curl_close($ch);
        $result = $this->getXmlResult($output);
        return $result;
	}
	
	public function doSpaceCreate() {
		if ( mkdir($this->folderroot.$this->foldername) ) {
			return "OK";
		} else {
			return "NOTOK";
		}
	}
	
	public function doLocalFileCopy() {
		$allfilesandfolders = $this->model->getSingleProfileDetailFileFolders($this->pushdetails["push_profile_uniqueid"]);
		$errors = array();
		foreach ($allfilesandfolders as $filefolder) {
          if ( $filefolder["profile_file_path"]=="." || $filefolder["profile_file_path"]==".." ) {
            if (substr($filefolder["profile_file_path"], -1, 1)=='/') {
              $len = strlen($filefolder["profile_file_path"]) - 1;
              $filefolder["profile_file_path"] = substr($filefolder["profile_file_path"], 0, $len); } }
          else if (filetype($this->siteroot.$filefolder["profile_file_path"]) == "file" ) {
        		if ( !$this->copyrecursive($this->siteroot.$filefolder["profile_file_path"], $this->folderroot.$this->foldername.$filefolder["profile_file_path"]) ) {$errors[] = "file: ".$filefolder["profile_file_path"]; } }
          else if (filetype($this->siteroot.$filefolder["profile_file_path"]) == "dir" ) {
        		if ( !$this->copyrecursive($this->siteroot.$filefolder["profile_file_path"], $this->folderroot.$this->foldername.$filefolder["profile_file_path"]) ) {$errors[] = "dir: ".$filefolder["profile_file_path"]; } } }
        if ( count($errors)>0 ) { return $errors; }
        else { return "OK"; }
	}
	
	
	public function doTableDump() {
	    $datatables = $this->model->getDBTablesToAction($this->pushdetails["push_profile_uniqueid"]);
	    $dbUserDetails = $this->getDbDumpUserDetails();
		$config = JFactory::getConfig();
		$dbname = $config->get('db');
		if ( count($datatables)>0 ) {
	    	$dumpcommand = 'mysqldump -u'.$dbUserDetails["user"].' -p'.$dbUserDetails["pass"].' '.$dbname.' ';
		    foreach ($datatables as $datatable) {
		    	$dumpcommand .= $datatable["profile_datatable_tablename"].' '; }
		    $dumpcommand .= '> '.$this->folderroot.$this->foldername.'gcworkflowdeploy.sql';
		    $output = array();
		    exec( $dumpcommand, $output, $retval );
		    $retvals[] = $retval;
		}
	    return "OK";
	}
	    
	public function createArchive() {
		$zip = new ZipArchive;
        if(!$zip->open($this->folderroot.$this->foldername.$this->archivename, ZIPARCHIVE::CREATE) ) {
	      $this->ziperrors[] = "Can't Create Zipfile"; }
	    $zip->addEmptyDir("gcworkflowfiles"); 
		$allfilesandfolders = $this->model->getSingleProfileDetailFileFolders($this->pushdetails["push_profile_uniqueid"]);
		foreach ($allfilesandfolders as $filefolder) {
			if (is_file($this->folderroot.$this->foldername.$filefolder["profile_file_path"]) &&
                is_readable($this->folderroot.$this->foldername.$filefolder["profile_file_path"])) {
		        if (!$zip->addFile($this->folderroot.$this->foldername.$filefolder["profile_file_path"],
                    "gcworkflowfiles/".$filefolder["profile_file_path"])) {
		      	    $this->ziperrors[] = $filefolder["profile_file_path"]; } }
            else if (is_dir($this->folderroot.$this->foldername.$filefolder["profile_file_path"]) ) {
				$zip = $this->zipAddDir($zip, $this->siteroot, $filefolder["profile_file_path"]); } }
        if (file_exists($this->folderroot.$this->foldername.'gcworkflowdeploy.sql')) {
            if (!$zip->addFile($this->folderroot.$this->foldername.'gcworkflowdeploy.sql', 'gcworkflowdeploy.sql') ) {
                $this->ziperrors[] = "Can't Add SQL"; } }
        $profilestring = $this->getProfileToString() ;
        if (!$zip->addFromString('gcworkflowprofile.profile', $profilestring) ) {
            $this->ziperrors[] = "Can't Add Profile"; }
        if ($zip->close() == false) {
            $this->ziperrors[] = "zip status:".$zip->getStatusString() ;
            $this->ziperrors[] = "Unspecified Zip Error"; }
        if ( count($this->ziperrors)>0 ) {
			return $this->ziperrors; }
        else {
			return $this->archivename; }
	}
	    
	public function getArchiveSize() {
		if ($filesize = filesize($this->folderroot.$this->foldername.$this->archivename)) {
			return $filesize;
		} else {
			return "NOTOK";
		}
	}

    public function doFileSend() {
        $server = $this->model->getSingleServerDetails($this->pushdetails["push_serv_id"]);
        $ch = curl_init($server["serv_webservice_url"]);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $post = array(
            "key"=>$server["serv_key"],
            "action"=>"send",
            "tmpl"=>"component",
            "arch"=>"@".$this->folderroot.$this->foldername.$this->archivename
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $output = curl_exec($ch);
        curl_close($ch);
        $result = $this->getXmlResult($output);
        return $result;
    }

    public function doFileMove() {
        $copyStatus = copy($this->folderroot.$this->foldername.$this->archivename,
            $this->configs->give("feature_store_folder").DS.$this->archivename ) ;
        $result = ($copyStatus == true) ? "OK" : "NOT OK" ;
        return $result;
    }
	
	public function getRemoteArchiveSize($fname) {
		$server = $this->model->getSingleServerDetails($this->pushdetails["push_serv_id"]);
		$ch = curl_init($server["serv_webservice_url"]);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $post = array(
	        "key"=>$server["serv_key"],
	    	"action"=>"fsize",
	    	"fname"=>$fname,
	    	"tmpl"=>"component"
	    );
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
        $output = curl_exec($ch);      
        curl_close($ch);
        $result = $this->getXmlResult($output);
        return $result;
	}
	

	public function cleanupFiles() {
		$errors = array();
		if (!$this->recursivermdir($this->folderroot.$this->foldername) ) {
			$errors[] = "Cannot delete $this->folderroot.$this->foldername"; 
		}
	    //initialize page
	    if (count($errors)>0) {
	    	return $errors;
	    } else {
	        return "OK";
	    }
	}
	
	
	/*
	 * Helpers
	 */
	private function getRootFolderToCalculate() {
		$folder  = JPATH_BASE;
		if (strstr(JPATH_BASE, "administrator")) {
			// this includes the administrator folder if its present
			$fulllength = strlen(JPATH_BASE);
			$lengthminusadmin = $fulllength - 13;
			$folder = substr($folder,0,$lengthminusadmin);
		}
		return $folder;		
	}
	
	private function getDbDumpUserDetails() {
		$details = array();
		$details["user"] = $this->configs->give("db_dump_user_name");	
		$details["pass"] = $this->configs->give("db_dump_user_pass");
		if ($details["user"] == "novalue" || $details["pass"] == "novalue") {
			$config = JFactory::getConfig();
			$details["user"] = $config->get('user');
			$details["pass"] = $config->get('password');
		}
		return $details;
	}	
	
	private function copyrecursive($source, $dest) {
		/* first check that the destination directory structure exists.
		 * If not, to a recursive mkdir	 */
		$foldertocheck = dirname($dest);
		if ( !file_exists($foldertocheck) ) {
	        mkdir($foldertocheck, 0777, true);
		}	
		if (is_dir($source)) {
		    $dir = dir($source);
		    while (false !== $entry = $dir->read()) {
		        // Skip pointers
		        if ($entry == '.' || $entry == '..') {
		            continue;
		        }
		        // Deep copy directories
		        $this->copyrecursive("$source/$entry", "$dest/$entry");
		    }
		    return true;
		} else if (is_file($source)) {
			if ( copy($source, $dest) ) {
				return true;	
			}
		}
		return false;
	}
		
	
	private function getXmlResult($xmlstr) {
		$firsttag = strpos($xmlstr,'<result>'); 
		$minusstart = substr($xmlstr,$firsttag);
		$secondtag = strpos($minusstart,'</result>'); 
		$minusend = substr($minusstart,8, $secondtag);
		return $minusend;
	}
		
	
	private function zipAddDir($zip, $fileroot, $filepath) {
	    if ( $filepath == "." || $filepath == ".." ) { }
      else {
          // first add the empty directory
          $fileroot = str_replace($this->siteroot."gcworkflowfiles","",$fileroot );
          $fileroot = str_replace($this->siteroot,"",$fileroot );
          $zip->addEmptyDir("gcworkflowfiles".DS.$fileroot.$filepath);
          $lastchar = substr($fileroot.$filepath, -1, 1);
          if ( $lastchar != "." ) {
              $filerootlastchar = substr($fileroot.$filepath, -1, 1);
              if ( $filerootlastchar !== DS ) {
                $filepath .= DS; }
              $dh = opendir($this->siteroot.$fileroot.$filepath);
              while ($file = readdir($dh) ) {
                  $lastchar = substr($fileroot.$filepath.$file, -1, 1);
                  var_dump($this->siteroot.$fileroot.$filepath.$file, "gcworkflowfiles".DS.$fileroot.$filepath.$file);
                  if ( $lastchar!=="." ) { }
                  else if (filetype($this->siteroot.$fileroot.$filepath.$file) == "file" && is_readable($this->siteroot.$fileroot.$filepath.$file) ) {
                      if (!$zip->addFile($this->siteroot.$fileroot.$filepath.$file, "gcworkflowfiles".DS.$fileroot.$filepath.$file) ) {
                          $this->ziperrors[] = $filepath;  } }
                  else if (filetype($this->siteroot.$fileroot.$filepath.$file) == "dir" ) {
                      $zip = $this->zipAddDir($zip, $fileroot.$filepath, $file); } } } }
		  return $zip;
	}
	
	private function recursivermdir($dir) {
	    $fp = opendir($dir);
	    if ( $fp ) {
	        while ( $f = readdir($fp) ) {
	            $file = $dir . "/" . $f;
	            if ( $f == "." || $f == ".." ) {
	                continue;
	            }
	            else if ( is_dir($file) ) {
	                $this->recursivermdir($file); }
	            else {
	                unlink($file); } }
	        closedir($fp);
	        rmdir($dir);
	        return true; }
	}

    private function getProfileToString() {
        $profileray = array();
        $profileray = $this->pushdetails;
        $profileray["files"] = $this->model->getSingleProfileDetailFileFolders($this->pushdetails["push_profile_uniqueid"]);
        $profileray["datatables"] = $this->model->getDBTablesToAction($this->pushdetails["push_profile_uniqueid"]);
        $profilestring = serialize($profileray);
        return $profilestring;
    }

    private function getProfileInstanceToString() {
        $profileray = array();
        $profileray = $this->pushdetails;
        $profileray["files"] = $this->model->getSingleProfileDetailFileFolders($this->pushdetails["push_profile_uniqueid"]);
        $profileray["datatables"] = $this->model->getDBTablesToAction($this->pushdetails["push_profile_uniqueid"]);
        $profilestring = serialize($profileray);
        return $profilestring;
    }
	
	
}
