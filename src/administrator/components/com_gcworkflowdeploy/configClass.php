<?php
/**
 * GCFW  
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/******************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *                                           *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

// CLASS REQUIRED
require_once (JPATH_COMPONENT_ADMINISTRATOR.DS.'modelClass.php');

class GCWorkflowDeployerConfigClass {

	public function give($wanted) {

        // FOR MAIN APPLICATION COMPONENT
		$confVars["title_main"] = "GC Features for Joomla";
		// $confVars["subtitle_view_pledges"] = "Main Page";
		$confVars["subtitle_home"] = "Home Page";
    $confVars["subtitle_pushstart"] = "Initiate Push";
    $confVars["subtitle_pushtype"] = "Push Type";
    $confVars["subtitle_pushdetails"] = "Push Details";
		$confVars["subtitle_pullstart"] = "Initiate Pull";
		$confVars["subtitle_push_history"] = "Push History";
		$confVars["subtitle_pull_history"] = "Pull History";
    $confVars["subtitle_delete_group"] = "Delete a Group";
    $confVars["subtitle_delete_key"] = "Delete a Key";
		$confVars["subtitle_delete_server"] = "Delete a Server";
		$confVars["subtitle_delete_profile"] = "Delete a Profile";
		$confVars["subtitle_site_details"] = "Configure Your Deployment";
		$confVars["subtitle_server_details"] = "Choose Your Server";
    $confVars["subtitle_manage_groups"] = "Manage Groups";
    $confVars["subtitle_manage_profiles"] = "Manage Profiles";
		$confVars["subtitle_manage_servers"] = "Manage Servers";
		$confVars["subtitle_manage_keys"] = "Manage Keys";
    $confVars["subtitle_edit_groups"] = "Edit Groups";
    $confVars["subtitle_edit_profiles"] = "Edit Profiles";
		$confVars["subtitle_edit_profile_files"] = "Edit Profile Files";
		$confVars["subtitle_edit_profile_data_tables"] = "Edit Profile Data Tables";
		$confVars["subtitle_edit_profile_data_table_actions"] = "Edit Profile Data Table Actions";
		$confVars["subtitle_edit_servers"] = "Edit Server";
    $confVars["subtitle_edit_keys"] = "Edit Keys";
    $confVars["subtitle_verify_group_info"] = "Verify Group Information";
		$confVars["subtitle_verify_deployment_info"] = "Verify The Deployment Info";
    $confVars["subtitle_process_remote_push"] = "Processing Remote Push";
    $confVars["subtitle_process_local_push"] = "Processing Local Push";
    $confVars["subtitle_process_push"] = "Processing Push";
		$confVars["subtitle_process_pull"] = "Processing Pull";
		$confVars["subtitle_confirm_deployment"] = "Deployment Processed - Confirming Details";
		$confVars["subtitle_not_logged_in"] = "You Must Login to continue";
		$confVars["subtitle_config"] = "Configure Page";

		//Component home intro text
		$confVars["home_intro_text"] = "GC Features for Joomla.";
		$confVars["config_intro_text"] = "Configure the Component here";
    $confVars["config_intro_defaults_notice_text"] = "You can submit the form with any empty field, and a suitable " .
      "replacement will be automatically used.";

		//This com name
		$confVars["com_name"] = "com_gcworkflowdeploy";
		if (isset($confVars[$wanted]) ) {
			return $confVars[$wanted]; }
    else {
      $userConfDefaults = $this->getUserConfDefaults();
      $dbConfVars = $this->getDbConfVar($wanted);
      $isDefaultEmpty = (isset($userConfDefaults[$wanted]) && strlen($userConfDefaults[$wanted]==""));
			if (count($dbConfVars)>0 && (strlen($dbConfVars[0]["xonfig_val"])>0 || $isDefaultEmpty ) ) {
				return $dbConfVars[0]["xonfig_val"]; }
      else if (isset($userConfDefaults[$wanted]) && $userConfDefaults[$wanted] != NULL) {
          return $userConfDefaults[$wanted]; }
      else {
          return "novalue"; } }
  }

  private function getUserConfDefaults(){
      $defaults = array();
      $userconfvar = array();
      require (JPATH_COMPONENT_ADMINISTRATOR.DS."configs".DS."userconfigs.php") ;
      foreach ($userconfvar as $userconf) {
          if (isset($userconf["default"])) {
              $key = $userconf["idString"];
              $val =$userconf['default'] ;
              $defaults[$key] = $val ; } }
      return $defaults;
  }

  public function setUserVar($var, $val){
      $this->setDbConfVar($var, $val);
  }

  private function getDbConfVar($wanted){
		$db = JFactory::getDBO();
		$query  = 'SELECT xonfig_val FROM #__gcworkflowdeploy_xonfig ' ;
		$query .= 'WHERE xonfig_var="'.$wanted.'"; ';
		$db->setQuery($query);
		$rows = $db->loadAssocList();
		return $rows;
	}

	/**
	*  This
	*/
	private function setDbConfVar($var, $val){
		$db = JFactory::getDBO();
		$query  = 'SELECT * FROM #__gcworkflowdeploy_xonfig ' ;
		$query .= 'WHERE xonfig_var="'.$var.'"; ';
		$db->setQuery($query);
		$db->query();
		$rows = $db->loadAssocList();
		// if var exists, update it, if not insert a new row
		if (count($rows)>0) {
			// if exists, update it
			$query  = "";
			$query .= 'UPDATE #__gcworkflowdeploy_xonfig SET xonfig_val=\''.$val.'\' ' ;
			$query .= 'WHERE xonfig_var="'.$var.'"; ';
			$db->setQuery($query);
			$db->query(); }
    else {
			// if not insert a new row
			$query  = "";
			$query .= 'INSERT INTO #__gcworkflowdeploy_xonfig (xonfig_var, xonfig_val) VALUES (\''.$var.'\',\''.$val.'\' ) ; ' ;
			$db->setQuery($query);
			$db->query(); }

	}

}
