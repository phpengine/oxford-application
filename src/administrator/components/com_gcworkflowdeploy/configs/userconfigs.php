<?php
    // Each one of these is a User Configured Variable
    // $userconfvar[] = array("idString", "Title", "Type", "description" );
    $userconfvar = array();
    $userconfvar[] = array("idString"=>"my_server_name", "title"=>"My Server Name", "type"=>"text", "description"=>"The human readable name used to identify this Server to/against others in the chain.");
    $userconfvar[] = array("idString"=>"auto_push_file_cleanup", "title"=>"Auto Clean Push Folder", "type"=>"text", "description"=>"Whether to automatically delete temporary push files. Boolean 1 or 0.", "default"=>"0");
    $userconfvar[] = array("idString"=>"auto_pull_file_cleanup", "title"=>"Auto Clean Pull Folder", "type"=>"text", "description"=>"Whether to automatically delete temporary pull files. Boolean 1 or 0.", "default"=>"0");
    $userconfvar[] = array("idString"=>"pull_backup_first", "title"=>"Backup Files Before Pull Folder", "type"=>"text", "description"=>"Whether to backup files before performing Pull. Boolean 1 or 0.", "default"=>"0");
    $userconfvar[] = array("idString"=>"temp_push_folder", "title"=>"Temp Push Folder", "type"=>"text", "description"=>"The path where files are temporarily stored when being pushed from this server.", "default"=>JPATH_COMPONENT_ADMINISTRATOR.DS."temp_push");
    $userconfvar[] = array("idString"=>"temp_pull_folder", "title"=>"Temp Pull Folder", "type"=>"text", "description"=>"The path where files are temporarily stored when being pulled to this server.", "default"=>JPATH_COMPONENT_ADMINISTRATOR.DS."temp_pull");
    $userconfvar[] = array("idString"=>"feature_store_folder", "title"=>"Feature Store Folder", "type"=>"text", "description"=>"The path where features for this instance are stored.", "default"=>JPATH_COMPONENT_ADMINISTRATOR.DS."feature_store");
    $userconfvar[] = array("idString"=>"webservice_status", "title"=>"Webservice Status", "type"=>"text", "description"=>"Turn this to 0 (off) to disallow communications from other installations. This overrides the below setting.", "default"=>"0");
    $userconfvar[] = array("idString"=>"webservice_allow_passive_pull", "title"=>"Allow Passive Pull", "type"=>"text", "description"=>"Turn this to 1 (on) to allow completely passive pushes to be made and executed on this site without any human intervention. This requires the above setting to be 1.", "default"=>"0");
    $userconfvar[] = array("idString"=>"items_per_page", "title"=>"No. of Items shown per Page", "type"=>"text", "description"=>"Enter a Number here, it denotes how many Items are shown on a Page.", "default"=>"10");
    $userconfvar[] = array("idString"=>"hide_php_errors", "title"=>"Hide PHP errors", "type"=>"text", "description"=>"Hide PHP generated errors in this component.", "default"=>"0");
