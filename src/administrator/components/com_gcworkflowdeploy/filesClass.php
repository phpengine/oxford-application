<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';

class GCWorkflowDeployerFilesClass {
	
	private static $config ;
	private $basedir ;
	private $dir ;

	public function __construct(){
		$this->configs = new GCWorkflowDeployerConfigClass();
		$this->getRootFolderToCalculate();
	}
	
	/*
	 * THESE ARE THE FILESYSTEM READERS
	 * */
	public function getContents() {
		$dir = $this->getRootFolderToCalculate();
		$filelist = array();
		$filelist = $this->getFileList($dir);
		return $filelist;
	} 

	private function getRootFolderToCalculate() {
		$folder  = JPATH_BASE;
		// this includes the administrator folder
		$fulllength = strlen(JPATH_BASE);
		$lengthminusadmin = $fulllength - 13;
		$folder = substr($folder,0,$lengthminusadmin);
		return $folder;	
	}
	
	public function getFileList($dir) {
		$filelist = array();
		$dh = opendir($dir);
        while (($file = readdir($dh)) !== false) {
        	$mini = array();
        	if ( $file=="." || $file==".." ) {
        	
        	} else if (filetype($dir.DS.$file) == "file" ) {
        		$mini["name"] = $file;
        		$mini["type"] = "file";
        		$filelist[] = $mini;  	
        	} else if (filetype($dir.DS.$file) == "dir" ) {
        		$mini["name"] = $file;
        		$mini["type"] = "dir";
        		$filelist[] = $mini;  	
        	}
        }
        closedir($dh);
        $this->aasort($filelist, "name");
		return $filelist;	
	}
	
	public function displayFileList($filelist) {
		$htmlvar .= '<div class="checkboxholderBody" >';
		$htmlvar .= '	<select name="fileroot" size="15" class="filechooser" id="filechooser'.$this->fcid.'" onchange="chooserChange('.$this->fcid.');">';
		foreach ($filelist as $file ) {
			if ($file["type"]=="file") {
				$htmlvar .= '<option class="chkbline file" value="'.$file["name"].'">'.$file["name"];
				$htmlvar .= '</option>';
			} else if ($file["type"]=="dir"){
				$htmlvar .= '<option class="chkbline dir" value="'.$file["name"].'">'.$file["name"];
				$htmlvar .= '</option>';
			}
		}
		$htmlvar .= '	</select>';
		$htmlvar .= '</div> <!-- end checkboxHolderBody --> ';
	}

	
	/*
	 * Helpers
	 */
	private function aasort (&$array, $key) {
	    $sorter=array();
	    $ret=array();
	    reset($array);
	    foreach ($array as $ii => $va) {
	        $sorter[$ii]=$va[$key];
	    }
	    asort($sorter);
	    foreach ($sorter as $ii => $va) {
	        $ret[$ii]=$array[$ii];
	    }
	    $array=$ret;
	    return $array;
	}

}
