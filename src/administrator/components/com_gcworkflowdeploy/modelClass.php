<?php
/**
 * GCFW Hello World
 * 
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer0100 is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
* AUTHOR:GOLDEN CONTACT COMPUTING *
*******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'viewClass.php';

class GCWorkflowDeployerModelClass {

  private $configs;
  private $db;

	public function __construct(){
		$this->configs = new GCWorkflowDeployerConfigClass();
    $this->db = JFactory::getDBO();
	}
	
	/*
	 * THESE ARE THE DATA
	 * GETTERS FOR QUICKSTART TMPL'S
	 * 
	 * */
	
	public function getAllKeysDetails() {
		$query = "SELECT * FROM #__gcworkflowdeploy_keys";
		$this->db->setQuery($query);
		$rows = $this->db->loadAssocList();
		return $rows;	
	}
	
	public function setUpdateKeyDetail($keyid, $field, $value) {
		$query = 'UPDATE #__gcworkflowdeploy_keys SET '.$field.'="'.$value.'" WHERE id="'.$keyid.'"';
		$this->db->setQuery($query);
		$this->db->query();
	}
	
	public function setNewKeyID() {
		$query = 'INSERT INTO #__gcworkflowdeploy_keys VALUES (NULL, "", "", "", "" )';
		$this->db->setQuery($query);
		$this->db->query();
		return $this->db->insertid();
	}
	
	public function getSingleKeyDetailsByKey($key){
		$query = 'SELECT * FROM #__gcworkflowdeploy_keys WHERE key_key="'.$key.'"';
		$this->db->setQuery($query);
		$row = $this->db->loadAssoc();
		if ( count($row)>0 ) { return $row; }
    else { return false; }
	}
	
	public function getSingleKeyDetails($keyid){
		$query = 'SELECT * FROM #__gcworkflowdeploy_keys WHERE id="'.$keyid.'"';
		$this->db->setQuery($query);
		$rows = $this->db->loadAssocList();
		return $rows[0];	
	}
	
	public function isKey($key){
		$query = 'SELECT * FROM #__gcworkflowdeploy_keys WHERE key_key="'.$key.'"';
		$this->db->setQuery($query);
		$rows = $this->db->loadAssocList();
		if ( count($rows)>0 ) { return true; }
    else { return false; }
	}
	
	public function getKeyNameFromId($keyid){
		$query = 'SELECT key_title FROM #__gcworkflowdeploy_keys WHERE id="'.$keyid.'"';
		$this->db->setQuery($query);
		$result = $this->db->loadResult();
		return $result;	
	}
	
	public function getKeyFromId($keyid){
		$query = 'SELECT key_key FROM #__gcworkflowdeploy_keys WHERE id="'.$keyid.'"';
		$this->db->setQuery($query);
		$result = $this->db->loadResult();
		return $result;	
	}
	
	public function deleteKey($keyid){
		$query = 'DELETE FROM #__gcworkflowdeploy_keys WHERE id="'.$keyid.'"';
		$this->db->setQuery($query);
		$this->db->query();
	}


  /*
   * THESE ARE THE DATA
   * GETTERS FOR ALL GROUPS
   *
   * */

  public function getAllGroups() {
    $query = "SELECT * FROM #__gcworkflowdeploy_groups";
    $this->db->setQuery($query);
    $rows = $this->db->loadAssocList();
    return $rows;
  }

  public function getAllOtherGroups($groupToIgnore) {
    $query = "SELECT * FROM #__gcworkflowdeploy_groups";
    $this->db->setQuery($query);
    $rows = $this->db->loadAssocList();
    foreach ($rows as &$row) {
        if ($row["id"]==$groupToIgnore) {
            unset ($rows[--$groupToIgnore]) ; } }
    return $rows;
  }

  public function setNewGroupID($uniqueid=null) {
    if ($uniqueid == null) {$uniqueid = $this->createUniqueID(); }
    $query = 'INSERT INTO #__gcworkflowdeploy_groups VALUES (NULL, "", "", "'.$uniqueid.'" )';
    $this->db->setQuery($query);
    $this->db->query();
    return $this->db->insertid();
  }

  public function getSingleGroupDetails($gid) {
    $query = 'SELECT * FROM #__gcworkflowdeploy_groups WHERE id="'.$gid.'"';
    $this->db->setQuery($query);
    $row = $this->db->loadAssoc();
    return $row;
  }

  public function deleteGroup($gid) {
    $query = 'DELETE FROM #__gcworkflowdeploy_groups WHERE id="'.$gid.'"';
    $this->db->setQuery($query);
    $this->db->query();
    return ($this->db->query()==true) ? true : false ;
  }

  public function getGroupIdFromUnique($unique) {
    $query = 'SELECT id FROM #__gcworkflowdeploy_groups WHERE uniqueid="'.$unique.'"';
    $this->db->setQuery($query);
    $row = $this->db->loadResult();
    return $row;
  }

  public function getSingleGroupDetailsFromUnique($guniqueid){
    $query = 'SELECT * FROM #__gcworkflowdeploy_groups WHERE uniqueid="'.$guniqueid.'"';
    $this->db->setQuery($query);
    $row = $this->db->loadAssoc();
    $row["entries"] = $this->getSingleGroupEntries($row["id"]);
    return $row;
  }

  public function setUpdateGroupDetail($servid, $field, $value) {
    $query  = 'UPDATE #__gcworkflowdeploy_groups SET '.$field.'="'.$value.'"';
    $query .= 'WHERE id="'.$servid.'"';
    $this->db->setQuery($query);
    $this->db->query();
  }

  public function getSingleGroupEntries($group_id) {
    $query = 'SELECT * FROM #__gcworkflowdeploy_group_entries WHERE group_id="'.$group_id.'"';
    $query .= ' ORDER BY ordering';
    $this->db->setQuery($query);
    $row = $this->db->loadAssocList();
    return ($row == null) ? array() : $row ;
  }

  public function getSingleGroupEntriesFromUnique($uniqueid){
    $query = 'SELECT id FROM #__gcworkflowdeploy_groups WHERE uniqueid="'.$uniqueid.'"';
    $this->db->setQuery($query);
    $group_id = $this->db->loadResult();
    $query = 'SELECT * FROM #__gcworkflowdeploy_group_entries WHERE group_id="'.$group_id.'"';
    $query .= ' ORDER BY ordering';
    $this->db->setQuery($query);
    $row = $this->db->loadAssocList();
    return ($row == null) ? array() : $row ;
  }

  public function getSingleGroupNextEntryOrderFromUnique($uniqueid){
    $query = 'SELECT id FROM #__gcworkflowdeploy_groups WHERE uniqueid="'.$uniqueid.'"';
    $this->db->setQuery($query);
    $group_id = $this->db->loadResult();
    $query = 'SELECT MAX(ordering) FROM #__gcworkflowdeploy_group_entries' .
      ' WHERE group_id="'.$group_id.'"';
    $this->db->setQuery($query);
    $this->db->query();
    $ordering = $this->db->loadResult();
    $ordering++;
    return $ordering ;
  }

  public function addGroupEntry($uniqueid, $entryType, $path, $ordering) {
    $query = 'SELECT id FROM #__gcworkflowdeploy_groups WHERE uniqueid="'.$uniqueid.'"';
    $this->db->setQuery($query);
    $group_id = $this->db->loadResult();
    $query  = 'INSERT INTO #__gcworkflowdeploy_group_entries VALUES (NULL, "'.$group_id.'",' ;
    $query .= ' "'.$entryType.'", "'.$path.'", "'.$ordering.'" ) ';
    $this->db->setQuery($query);
    return ($this->db->query() == true) ? true : false ;
  }

  public function addGroupEntryByDBID($group_id, $entryType, $path, $ordering) {
    $query  = 'INSERT INTO #__gcworkflowdeploy_group_entries VALUES (NULL, "'.$group_id.'",' ;
    $query .= ' "'.$entryType.'", "'.$path.'", "'.$ordering.'" ) ';
    $this->db->setQuery($query);
    return ($this->db->query() == true) ? true : false ;
  }

  public function deleteGroupEntry($uniqueid, $entry, $ordering) {
    $query = 'SELECT id FROM #__gcworkflowdeploy_groups WHERE uniqueid="'.$uniqueid.'"';
    $this->db->setQuery($query);
    $group_id = $this->db->loadResult();
    $query = 'DELETE FROM #__gcworkflowdeploy_group_entries WHERE group_id="' .
      $group_id.'" AND target="'.$entry.'" AND ordering="'.$ordering.'"';
    $this->db->setQuery($query);
    return ($this->db->query() == true) ? true : false ;
  }

  public function deleteAllGroupEntries($groupid) {
    $query = 'DELETE FROM #__gcworkflowdeploy_group_entries WHERE group_id="' .
      $groupid.'"';
    $this->db->setQuery($query);
    return ($this->db->query() == true) ? true : false ;
  }

  /*
   * THESE ARE THE DATA
   * GETTERS FOR ALL SERVERS
   *
   * */

  public function getAllServersDetails(){
    $query = "SELECT * FROM #__gcworkflowdeploy_servers";
    $this->db->setQuery($query);
    $rows = $this->db->loadAssocList();
    return $rows;
  }
	
	public function getSingleServerDetails($servid){
		$query = 'SELECT * FROM #__gcworkflowdeploy_servers WHERE id="'.$servid.'"';
		$this->db->setQuery($query);
		$row = $this->db->loadAssoc();
		return $row;
	}

	public function setUpdateServerDetail($servid, $field, $value){
		$query = 'UPDATE #__gcworkflowdeploy_servers SET '.$field.'="'.$value.'" WHERE id="'.$servid.'"';
		$this->db->setQuery($query);
		$this->db->query();
	}
	
	public function setNewServerID() {
		$query = 'INSERT INTO #__gcworkflowdeploy_servers VALUES (NULL, "", "", "", "", "", "", "", "", "", "", "", "" )';
		$this->db->setQuery($query);
		$this->db->query();
		return $this->db->insertid();
	}
	
	public function deleteServer($servid) {
		$query = 'DELETE FROM #__gcworkflowdeploy_servers WHERE id="'.$servid.'"';
		$this->db->setQuery($query);
		$this->db->query();
	}
		
	/*
	 * THESE ARE THE SERVER
	 * PROFILE GETTERS
	 * 
	 * */
	
	public function getServerProfileTitle($servid) {
		$query = 'SELECT serv_title FROM #__gcworkflowdeploy_servers WHERE id="'.$servid.'"';
		$this->db->setQuery($query);
		$result = $this->db->loadResult();
		return $result;	
	}
	
	public function getServerProfileVhostPath($servid) {
		$query = 'SELECT serv_vhost_path FROM #__gcworkflowdeploy_servers WHERE id="'.$servid.'"';
		$this->db->setQuery($query);
		$result = $this->db->loadResult();
		return $result;	
	}
	
	
	
	/*
	 * THESE ARE THE DATA SETTERS &
	 * GETTERS FOR DATA PROFILES
	 * */
	
	public function setNewProfileID(){
		$uniqueid = $this->createUniqueID();
		$query = 'INSERT INTO #__gcworkflowdeploy_profiles VALUES (NULL, "", "", "'.$uniqueid.'" ) ';
		$this->db->setQuery($query);
		$this->db->query();
		$result = $this->db->insertid();
		return $result;
	}
	
	public function setUpdateProfileDetail($profileid, $field, $value){
		$query = 'UPDATE #__gcworkflowdeploy_profiles SET '.$field.'="'.$value.'" WHERE id="'.$profileid.'"';
		$this->db->setQuery($query);
		$this->db->query();
	}

	public function getAllProfiles(){
		$query = "SELECT * FROM #__gcworkflowdeploy_profiles";
		$this->db->setQuery($query);
		$rows = $this->db->loadAssocList();
		return $rows;	
	}
	
	public function getSingleProfileDetails($profileid){
		$query = 'SELECT * FROM #__gcworkflowdeploy_profiles WHERE id="'.$profileid.'"';
		$this->db->setQuery($query);
		$single = $this->db->loadAssoc();
		$single["filefolders"] = $this->getSingleProfileDetailFileFolders($single["profile_uniqueid"]);
		return $single;	
	}
	
	public function getSingleProfileDetailsFromUnique($profileuniqueid){
		$query = 'SELECT * FROM #__gcworkflowdeploy_profiles WHERE profile_uniqueid="'.$profileuniqueid.'"';
		$this->db->setQuery($query);
		$row = $this->db->loadAssoc();
		$single = $row;
		$single["filefolders"] = $this->getSingleProfileDetailFileFolders($single["profile_uniqueid"]);
		return $single;	
	}
	
	public function getSingleProfileDetailFileFolders($profileuniqueid){
		$query = 'SELECT profile_file_path FROM #__gcworkflowdeploy_profile_files WHERE profile_uniqueid="'.$profileuniqueid.'"';
		$this->db->setQuery($query);
		$rows = $this->db->loadAssocList();
		if (is_array($rows)) { return $rows; }
    else { return array();}
	}
	
	public function deleteSingleProfile($profileid){
		$query = 'DELETE FROM #__gcworkflowdeploy_profiles WHERE id="'.$profileid.'"';
		$this->db->setQuery($query);
		if ($this->db->query() ) { return true; }
    else { return false; }
	}
	
	public function addProfileFileFolder($uniqueid, $path) {
		$query = 'INSERT INTO #__gcworkflowdeploy_profile_files VALUES (NULL, "'.$uniqueid.'", "'.$path.'" ) ';
		$this->db->setQuery($query);
		$this->db->query();
	}
	
	public function deleteProfileFileFolder($uniqueid, $path) {
		$query = 'DELETE FROM #__gcworkflowdeploy_profile_files WHERE profile_uniqueid="'.$uniqueid.'" AND profile_file_path="'.$path.'"';
		$this->db->setQuery($query);
		if ($this->db->query() ) { return true; }
    else { return false; }
	}
	
	public function checkProfileExists($profileuniqueid) {
		$query = 'SELECT * FROM #__gcworkflowdeploy_profiles WHERE profile_uniqueid="'.$profileuniqueid.'"';
		$this->db->setQuery($query);
		$row = $this->db->loadAssoc();
		$single = $row;
		if (count($single)>0 ) { return true; }
    else { return false; }
	}

	
	/*
	 * DATATABLES DATA
	 * THINGS
	 */
	
	public function getAllDBTables() {
		$query = "SHOW TABLES";
		$this->db->setQuery($query);
		if ($this->db->query() ) { return $this->db->loadColumn(); }
    else { return false; }
	}
	
	public function setProfileDataTables($profileunique, $tables) {
		foreach ($tables as $table) {
            $query = 'SELECT id FROM #__gcworkflowdeploy_profile_data_tables' .
                ' WHERE profile_uniqueid="'.$profileunique.'" AND profile_datatable_tablename="'.$table[0].'" ' .
                'AND profile_datatable_ordering="'.$table[1].'" ' ;
            $this->db->setQuery($query);
            $this->db->query();
            $idOfCurrentRecord = $this->db->loadResult();
            if ($idOfCurrentRecord>0) {
                $query = 'UPDATE #__gcworkflowdeploy_profile_data_tables SET  ';
                $query .= 'profile_datatable_tablename="'.$table[0].'", profile_datatable_ordering="'.$table[1].'" ';
                $query .= 'WHERE id="'.$idOfCurrentRecord.'"';}
            else {
                $query = 'INSERT INTO #__gcworkflowdeploy_profile_data_tables (profile_uniqueid, ';
                $query .= 'profile_datatable_tablename, profile_datatable_ordering) VALUES ("'.$profileunique.'", ';
                $query .= '"'.$table[0].'", "'.$table[1].'") '; }
			$this->db->setQuery($query);
			$this->db->query();
		}
	}
	
	public function getDBTablesToAction($profileunique) {
		$query = 'SELECT * FROM #__gcworkflowdeploy_profile_data_tables WHERE profile_uniqueid="'.$profileunique.'"';
		$this->db->setQuery($query);
		$this->db->query();
		$result = $this->db->loadAssocList();
		return $result;
	}
	
	public function setProfileDataTableAction($profileunique, $tablename, $actcode, $tableorder, $actiondata) {
		$query = 'UPDATE #__gcworkflowdeploy_profile_data_tables SET profile_datatable_action_code="' .
            $actcode.'", profile_datatable_action_details=\''.$actiondata.'\' WHERE profile_uniqueid="'.$profileunique .
            '" AND profile_datatable_tablename="'.$tablename.'" AND profile_datatable_ordering="'.$tableorder.'"';
		$this->db->setQuery($query);
		$this->db->query();
	}
	
	public function getAvailableDBActions() {
		$query = 'SELECT * FROM #__gcworkflowdeploy_profile_action_codes' ;
		$this->db->setQuery($query);
		$this->db->query();
		$result = $this->db->loadAssocList();
		if (is_array($result)) { return $result; }
    else { return array(); }
	}

	public function setDropTempTable($tablename) {
		$query = 'DROP TABLE "'.$tablename.'"';
		$this->db->setQuery($query);
		$this->db->query();
	}

	
	/*
	 * PUSH HISTORY
	 * THINGS
	 */
	
	public function getAllPushHistory() {
		$query = 'SELECT * FROM #__gcworkflowdeploy_push_history' ;
		$this->db->setQuery($query);
		$this->db->query();
		$result = $this->db->loadAssocList();
		if (is_array($result)) { return $result; }
    else { return array(); }
	}
	
	public function getPushHistoryPage($pagenum) {
		$itemsperpage = $this->configs->give("items_per_page");
		$startrow = ($pagenum-1) * $itemsperpage;
		$query = 'SELECT * FROM #__gcworkflowdeploy_push_history ORDER BY id DESC LIMIT '.$startrow.', '.$itemsperpage ;
		$this->db->setQuery($query);
		$this->db->query();
		$result = $this->db->loadAssocList();
		if (is_array($result)) { return $result; }
    else { return array(); }
	}
	
	public function getSinglePushDetails($pushid){
		$query = 'SELECT * FROM #__gcworkflowdeploy_push_history WHERE id="'.$pushid.'"' ;
		$this->db->setQuery($query);
		$this->db->query();
		$result = $this->db->loadAssoc();
    if (is_array($result)) { return $result; }
    else { return array(); }
	}
	
	public function setUpdatePush($pushid, $field, $value) {
		$query = 'UPDATE #__gcworkflowdeploy_push_history SET '.$field.'="'.$value.'" WHERE id="'.$pushid.'"';
		$this->db->setQuery($query);
		$this->db->query();
	}
	
	public function setNewPushID() {
		$query = 'INSERT INTO #__gcworkflowdeploy_push_history VALUES (NULL, "", "", "", "", "", "", "", "", "", "", "" )';
		$this->db->setQuery($query);
		$this->db->query();
		return $this->db->insertid();
	}

	
	/*
	 * PULL HISTORY
	 * THINGS
	 */
	public function setNewPullID() {
		$query = 'INSERT INTO #__gcworkflowdeploy_pull_history VALUES (NULL, "", "", "", "", "", "", "", "" )';
		$this->db->setQuery($query);
		$this->db->query();
		return $this->db->insertid();
	}
	
	public function getAllPullHistory(){
		$query = 'SELECT * FROM #__gcworkflowdeploy_pull_history' ;
		$this->db->setQuery($query);
		$this->db->query();
		$result = $this->db->loadAssocList();
    if (is_array($result)) { return $result; }
    else { return array(); }
	}
	
	public function getPullHistoryPage($pagenum){
		$itemsperpage = $this->configs->give("items_per_page");
		$startrow = ($pagenum-1) * $itemsperpage;
		$query = 'SELECT * FROM #__gcworkflowdeploy_pull_history ORDER BY id DESC LIMIT '.$startrow.', '.$itemsperpage ;
		$this->db->setQuery($query);
		$this->db->query();
		$result = $this->db->loadAssocList();
    if (is_array($result)) { return $result; }
    else { return array(); }
	}
	
	public function getSinglePullDetails($pullid){
		$query = 'SELECT * FROM #__gcworkflowdeploy_pull_history WHERE id="'.$pullid.'"' ;
		$this->db->setQuery($query);
		$this->db->query();
		$result = $this->db->loadAssoc();
    if (is_array($result)) { return $result; }
    else { return array(); }
	}
	
	public function setUpdatePull($pullid, $field, $value) {
		$query = 'UPDATE #__gcworkflowdeploy_pull_history SET '.$field.'="'.$value.'" WHERE id="'.$pullid.'"';
		$this->db->setQuery($query);
		$this->db->query();
	}
	
	
	
	/*
	 *  Helpers
	 * 
	 */
	private function createUniqueID() {
		$length = 16;
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	
		$size = strlen( $chars );
		$str = "";
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ]; }
		return $str;
	}

}
