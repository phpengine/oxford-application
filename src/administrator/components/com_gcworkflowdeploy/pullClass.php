<?php
/**
 * GCFW Hello World
 *
 * @package    GCFW Hello World
 * @subpackage Component
 * @link http://www.gcsoftshop.co.uk/shop/joomla/components/gcfw-mvc-development-framework-for-joomla-components.html
 * @license        GNU/GPL, see LICENSE.php
 * com_GCWorkflowDeployer is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/********************************************************************************
 * AUTHOR:GOLDEN CONTACT COMPUTING *
 *******************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'configClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'modelClass.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'viewClass.php';

class GCWorkflowDeployerPullClass {

    private $configs;
    private $view;
    private $model;
    private $pullid;
    private $pulldetails;
    private $siteroot;
    private $folderroot;

    public function __construct($pullid){
        $this->pullid = $pullid;
        $this->configs = new GCWorkflowDeployerConfigClass();
        $this->view = new GCWorkflowDeployerBEViewClass();
        $this->model = new GCWorkflowDeployerModelClass();
        $this->pulldetails = $this->model->getSinglePullDetails($this->pullid);
        $this->siteroot = $this->getRootFolderToCalculate();
        if (substr($this->siteroot, "-1", "1") != "/") {
            $this->siteroot .= DS; }
        $this->folderroot = $this->configs->give("temp_pull_folder");
        if (substr($this->folderroot, "-1", "1") != "/") {
            $this->folderroot .= DS; }
        $this->foldername = $this->pulldetails["pull_profile_uniqueid"].'_'.$this->pulldetails["push_time"].DS;
    }

    public function createNewPull() {
        $model = new GCWorkflowDeployerModelClass();
        $pullid = $model->setNewPullID();
        return $pullid;
    }

    public function extractZip($zipfile) {
        $zip = new ZipArchive();
        $zip->open($zipfile);
        $this->pulldetails = $this->model->getSinglePullDetails($this->pullid);
        $this->foldername = $this->pulldetails["pull_profile_uniqueid"].'_'.$this->pulldetails["push_time"].DS;
        $zip->extractTo($this->folderroot.$this->foldername);
    }

    public function getProfileDetails() {
        $this->pulldetails = $this->model->getSinglePullDetails($this->pullid);
        $this->foldername = $this->pulldetails["pull_profile_uniqueid"].'_'.$this->pulldetails["push_time"].DS;
        $serializedProfile = file_get_contents($this->folderroot.$this->foldername.'gcworkflowprofile.profile');
        $profileDetailsArray = unserialize($serializedProfile);
        return $profileDetailsArray;
    }

    public function doLocalFileCopy() {
        $errors = array();
        $dh = opendir($this->folderroot.$this->foldername."gcworkflowfiles");
        // echo "filecopydir: ".$this->folderroot.$this->foldername."gcworkflowfiles".'</br>'.'</br>';
        // echo "first dest:".$this->siteroot.'</br>'.'</br>';
        while ($file = readdir($dh) ) {
            if (substr($file, -1, 1) != "." ) {
                // echo '<h1>copy exec</h1>';
                $this->copyrecursive($this->folderroot.$this->foldername."gcworkflowfiles".DS.$file, $this->siteroot.$file) ; } }
        if ( count($errors)>0 ) {
            return $errors; }
        else {
            return "OK"; }
    }

    private function getDbDumpUserDetails() {
        $details = array();
        $details["user"] = $this->configs->give("db_dump_user_name");
        $details["pass"] = $this->configs->give("db_dump_user_pass");
        if ($details["user"] == "novalue" || $details["pass"] == "novalue") {
            $config = JFactory::getConfig();
            $details["user"] = $config->get('user');
            $details["pass"] = $config->get('password');
        }
        return $details;
    }

    public function doTempDbTableInstall() {
        $sqlfile = $this->folderroot.$this->foldername."gcworkflowdeploy.sql" ;
        if (file_exists($sqlfile)) {
          $sqlfilestring = file_get_contents($sqlfile);
          $pull_identifier = substr($this->foldername, 0, strlen($this->foldername)-1 );
          $sqlfilestring = str_replace('jos_', 'jos_'.$pull_identifier.'_', $sqlfilestring);
          file_put_contents($this->folderroot.$this->foldername."modifiedgcworkflowdeploy.sql", $sqlfilestring);
          $dbUserDetails = $this->getDbDumpUserDetails();
          $config = JFactory::getConfig();
          $dbname = $config->get('db');
          $impcommand = 'mysql -u'.$dbUserDetails["user"].' -p'.$dbUserDetails["pass"].' '.$dbname;
          $impcommand .= ' < '.$this->folderroot.$this->foldername.'modifiedgcworkflowdeploy.sql';
          $output = array();
          exec( $impcommand, $output, $retval );
          $retvals[] = $retval;
          return "OK"; }
        else {
          return "OK - No SQL File to execute"; }
    }

    public function doNewDbTableRewrite() {
        $profiledetails = $this->getProfileDetails();
        $actiontables = $profiledetails["datatables"];
        $config = JFactory::getConfig();
        $dbprefix = $config->get('dbprefix');
        foreach ($actiontables as $stored_table) {
            $pull_identifier = substr($this->foldername, 0, strlen($this->foldername)-1 );
            $origtable = $stored_table["profile_datatable_tablename"];
            $temptable = str_replace($dbprefix, $dbprefix.$pull_identifier.'_', $stored_table["profile_datatable_tablename"]);
            include JPATH_COMPONENT_ADMINISTRATOR.DS.'actions'.DS.$stored_table["profile_datatable_action_code"].'tablescript.php'; }
        return "OK";
    }

    public function doTempDbTableDrop() {
        $profiledetails = $this->getProfileDetails();
        $actiontables = $profiledetails["datatables"];
        foreach ($actiontables as $table) {
          $pull_identifier = substr($this->foldername, 0, strlen($this->foldername)-1 );
          $temptable = str_replace('jos_', 'jos_'.$pull_identifier.'_', $table["profile_datatable_tablename"]);
          $this->model->setDropTempTable($temptable); }
        return "OK";
    }

    public function checkFileExists() {
        $doesExist = file_exists( $this->folderroot.$this->foldername );
        return $doesExist ;
    }

    public function statusUpdate($value) {
        $this->model->setUpdatePull($this->pullid, "pull_status", $value);
        return "OK";
    }

    public function cleanupFiles() {
        // if ( $this->rmdirrecursive( $this->folderroot.$this->foldername ) ) {
        return "OK" ;
        // } else {
        // 	return "NOTOK" ;
        // }
    }


    /*
     * Helpers
     */
    private function getRootFolderToCalculate() {
        $folder  = JPATH_BASE;
        if (strstr(JPATH_BASE, "administrator")) {
            // this includes the administrator folder if its present
            $fulllength = strlen(JPATH_BASE);
            $lengthminusadmin = $fulllength - 13;
            $folder = substr($folder,0,$lengthminusadmin);
        }
        return $folder;
    }

    private function copyrecursive($source, $dest) {
        /* first check that the destination directory
         * structure exists. If not, to a recursive mkdir */
        $foldertocheck = dirname($dest);
        if ( !file_exists($foldertocheck) ) { mkdir($foldertocheck, 0777, true); }
        if (substr($source, 0, 1) != "." ) {
            if (is_dir($source)) {
                $dir = dir($source);
                while (false !== $entry = $dir->read()) {
                    if ($entry == '.' || $entry == '..') {
                        continue; }
                    $this->copyrecursive("$source/$entry", "$dest/$entry"); }
                return true; }
            else if (is_file($source)) {
                if ( copy($source, $dest) ) {
                    return true;
                    echo "copied $source to $dest"; }
                else {
                    return false; } } }
        return false;
    }

    private function rmdirrecursive($dir) {
        if (!file_exists($dir)) return true;
        if (!is_dir($dir) || is_link($dir)) return unlink($dir);
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..' ) continue;
            if (!$this->rmdirrecursive($dir . '/' . $item)) {
                if (!$this->rmdirrecursive($dir . '/' . $item)) return false; }; }
        return rmdir($dir);
    }

}
